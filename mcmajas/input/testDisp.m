function testDisp()
% testing disp() function
x = 6;
disp(x);
y = [1 2 3 4];
disp(y);
z = [1; 2; 3;];
disp(z);
zz = [4 5; 6 7; 8 9;];
disp(zz);
end