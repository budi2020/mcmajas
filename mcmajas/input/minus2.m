function minus2()
% show int - double,  
% 1.9 - int16(3.6) = -2
a = 1.9;
b = int16(3.6);
c = a - b;
disp(c);
end