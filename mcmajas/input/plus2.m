function plus2()
% show int + double, the result is int = round(a) + round(b), 
% 1.9 + int16(3.6) = 6
% 1.2 + int16(3) = 4
a = 1.9;
b = int16(3.6);
c = a + b;
disp(c);
end