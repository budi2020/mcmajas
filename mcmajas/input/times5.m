function times5()
% matrix * matrix
a = [10 20 30; 40 50 60];
b = [1 2 3; 4 5 6];
c = a .* b;
disp(c);
end