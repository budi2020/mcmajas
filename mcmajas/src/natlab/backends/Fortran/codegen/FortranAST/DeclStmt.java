/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.Fortran.codegen.FortranAST;

/**
 * @production DeclStmt : {@link ASTNode} ::= <span class="component">&lt;Type:String&gt;</span> <span class="component">[{@link KeywordList}]</span> <span class="component">[{@link ShapeInfo}]</span> <span class="component">{@link VariableList}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/FortranIR.ast:10
 */
public class DeclStmt extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public DeclStmt clone() throws CloneNotSupportedException {
    DeclStmt node = (DeclStmt)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public DeclStmt copy() {
      try {
        DeclStmt node = (DeclStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public DeclStmt fullCopy() {
    try {
      DeclStmt tree = (DeclStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/PrettyPrinter.jadd:53
   */
  public void pp(StringBuffer sb) {
    	sb.append(getType());
    	if(hasKeywordList()) {
    	    sb.append(" , ");
    		getKeywordList().pp(sb);
    	}
    	if(hasShapeInfo()) {
    		sb.append(" , ");
    		getShapeInfo().pp(sb);
    	}
    	sb.append(" :: ");
    	getVariableList().pp(sb);
    }
  /**
   * @ast method 
   * 
   */
  public DeclStmt() {
    super();

    setChild(new Opt(), 0);
    setChild(new Opt(), 1);

  }
  /**
   * @ast method 
   * 
   */
  public DeclStmt(String p0, Opt<KeywordList> p1, Opt<ShapeInfo> p2, VariableList p3) {
    setType(p0);
    setChild(p1, 0);
    setChild(p2, 1);
    setChild(p3, 2);
  }
  /**
   * @ast method 
   * 
   */
  public DeclStmt(beaver.Symbol p0, Opt<KeywordList> p1, Opt<ShapeInfo> p2, VariableList p3) {
    setType(p0);
    setChild(p1, 0);
    setChild(p2, 1);
    setChild(p3, 2);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 3;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the lexeme Type.
   * @param value The new value for the lexeme Type.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setType(String value) {
    tokenString_Type = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Type;
  /**
   * @ast method 
   * 
   */
  
  public int Typestart;
  /**
   * @ast method 
   * 
   */
  
  public int Typeend;
  /**
   * JastAdd-internal setter for lexeme Type using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setType(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setType is only valid for String lexemes");
    tokenString_Type = (String)symbol.value;
    Typestart = symbol.getStart();
    Typeend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Type.
   * @return The value for the lexeme Type.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getType() {
    return tokenString_Type != null ? tokenString_Type : "";
  }
  /**
   * Replaces the optional node for the KeywordList child. This is the {@code Opt} node containing the child KeywordList, not the actual child!
   * @param opt The new node to be used as the optional node for the KeywordList child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setKeywordListOpt(Opt<KeywordList> opt) {
    setChild(opt, 0);
  }
  /**
   * Check whether the optional KeywordList child exists.
   * @return {@code true} if the optional KeywordList child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasKeywordList() {
    return getKeywordListOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) KeywordList child.
   * @return The KeywordList child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public KeywordList getKeywordList() {
    return (KeywordList)getKeywordListOpt().getChild(0);
  }
  /**
   * Replaces the (optional) KeywordList child.
   * @param node The new node to be used as the KeywordList child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setKeywordList(KeywordList node) {
    getKeywordListOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the KeywordList child. This is the {@code Opt} node containing the child KeywordList, not the actual child!
   * @return The optional node for child the KeywordList child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<KeywordList> getKeywordListOpt() {
    return (Opt<KeywordList>)getChild(0);
  }
  /**
   * Retrieves the optional node for child KeywordList. This is the {@code Opt} node containing the child KeywordList, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child KeywordList.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<KeywordList> getKeywordListOptNoTransform() {
    return (Opt<KeywordList>)getChildNoTransform(0);
  }
  /**
   * Replaces the optional node for the ShapeInfo child. This is the {@code Opt} node containing the child ShapeInfo, not the actual child!
   * @param opt The new node to be used as the optional node for the ShapeInfo child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setShapeInfoOpt(Opt<ShapeInfo> opt) {
    setChild(opt, 1);
  }
  /**
   * Check whether the optional ShapeInfo child exists.
   * @return {@code true} if the optional ShapeInfo child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasShapeInfo() {
    return getShapeInfoOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) ShapeInfo child.
   * @return The ShapeInfo child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ShapeInfo getShapeInfo() {
    return (ShapeInfo)getShapeInfoOpt().getChild(0);
  }
  /**
   * Replaces the (optional) ShapeInfo child.
   * @param node The new node to be used as the ShapeInfo child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setShapeInfo(ShapeInfo node) {
    getShapeInfoOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the ShapeInfo child. This is the {@code Opt} node containing the child ShapeInfo, not the actual child!
   * @return The optional node for child the ShapeInfo child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<ShapeInfo> getShapeInfoOpt() {
    return (Opt<ShapeInfo>)getChild(1);
  }
  /**
   * Retrieves the optional node for child ShapeInfo. This is the {@code Opt} node containing the child ShapeInfo, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child ShapeInfo.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<ShapeInfo> getShapeInfoOptNoTransform() {
    return (Opt<ShapeInfo>)getChildNoTransform(1);
  }
  /**
   * Replaces the VariableList child.
   * @param node The new node to replace the VariableList child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setVariableList(VariableList node) {
    setChild(node, 2);
  }
  /**
   * Retrieves the VariableList child.
   * @return The current node used as the VariableList child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public VariableList getVariableList() {
    return (VariableList)getChild(2);
  }
  /**
   * Retrieves the VariableList child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the VariableList child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public VariableList getVariableListNoTransform() {
    return (VariableList)getChildNoTransform(2);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
