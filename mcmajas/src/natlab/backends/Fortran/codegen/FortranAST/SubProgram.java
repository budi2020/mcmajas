/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.Fortran.codegen.FortranAST;

/**
 * @production SubProgram : {@link ASTNode} ::= <span class="component">{@link ProgramTitle}</span> <span class="component">{@link DeclarationSection}</span> <span class="component">{@link BackupVar}*</span> <span class="component">{@link StatementSection}</span> <span class="component">&lt;ProgramEnd:String&gt;</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/FortranIR.ast:2
 */
public class SubProgram extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public SubProgram clone() throws CloneNotSupportedException {
    SubProgram node = (SubProgram)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public SubProgram copy() {
      try {
        SubProgram node = (SubProgram)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public SubProgram fullCopy() {
    try {
      SubProgram tree = (SubProgram) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/PrettyPrinter.jadd:6
   */
  public void pp(StringBuffer sb) {
		getProgramTitle().pp(sb);
		getDeclarationSection().pp(sb);
		int size = getNumBackupVar();
    	for(int i=0;i<size;i++) {
    		getBackupVar(i).pp(sb);
    	}
		getStatementSection().pp(sb);
		sb.append(getProgramEnd()+"\n");
	}
  /**
   * @ast method 
   * 
   */
  public SubProgram() {
    super();

    setChild(new List(), 2);

  }
  /**
   * @ast method 
   * 
   */
  public SubProgram(ProgramTitle p0, DeclarationSection p1, List<BackupVar> p2, StatementSection p3, String p4) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
    setChild(p3, 3);
    setProgramEnd(p4);
  }
  /**
   * @ast method 
   * 
   */
  public SubProgram(ProgramTitle p0, DeclarationSection p1, List<BackupVar> p2, StatementSection p3, beaver.Symbol p4) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
    setChild(p3, 3);
    setProgramEnd(p4);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 4;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the ProgramTitle child.
   * @param node The new node to replace the ProgramTitle child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setProgramTitle(ProgramTitle node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the ProgramTitle child.
   * @return The current node used as the ProgramTitle child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public ProgramTitle getProgramTitle() {
    return (ProgramTitle)getChild(0);
  }
  /**
   * Retrieves the ProgramTitle child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the ProgramTitle child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public ProgramTitle getProgramTitleNoTransform() {
    return (ProgramTitle)getChildNoTransform(0);
  }
  /**
   * Replaces the DeclarationSection child.
   * @param node The new node to replace the DeclarationSection child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setDeclarationSection(DeclarationSection node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the DeclarationSection child.
   * @return The current node used as the DeclarationSection child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public DeclarationSection getDeclarationSection() {
    return (DeclarationSection)getChild(1);
  }
  /**
   * Retrieves the DeclarationSection child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the DeclarationSection child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public DeclarationSection getDeclarationSectionNoTransform() {
    return (DeclarationSection)getChildNoTransform(1);
  }
  /**
   * Replaces the BackupVar list.
   * @param list The new list node to be used as the BackupVar list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setBackupVarList(List<BackupVar> list) {
    setChild(list, 2);
  }
  /**
   * Retrieves the number of children in the BackupVar list.
   * @return Number of children in the BackupVar list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumBackupVar() {
    return getBackupVarList().getNumChild();
  }
  /**
   * Retrieves the number of children in the BackupVar list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the BackupVar list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumBackupVarNoTransform() {
    return getBackupVarListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the BackupVar list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the BackupVar list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public BackupVar getBackupVar(int i) {
    return (BackupVar)getBackupVarList().getChild(i);
  }
  /**
   * Append an element to the BackupVar list.
   * @param node The element to append to the BackupVar list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addBackupVar(BackupVar node) {
    List<BackupVar> list = (parent == null || state == null) ? getBackupVarListNoTransform() : getBackupVarList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addBackupVarNoTransform(BackupVar node) {
    List<BackupVar> list = getBackupVarListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the BackupVar list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setBackupVar(BackupVar node, int i) {
    List<BackupVar> list = getBackupVarList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the BackupVar list.
   * @return The node representing the BackupVar list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<BackupVar> getBackupVars() {
    return getBackupVarList();
  }
  /**
   * Retrieves the BackupVar list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the BackupVar list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<BackupVar> getBackupVarsNoTransform() {
    return getBackupVarListNoTransform();
  }
  /**
   * Retrieves the BackupVar list.
   * @return The node representing the BackupVar list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<BackupVar> getBackupVarList() {
    List<BackupVar> list = (List<BackupVar>)getChild(2);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the BackupVar list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the BackupVar list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<BackupVar> getBackupVarListNoTransform() {
    return (List<BackupVar>)getChildNoTransform(2);
  }
  /**
   * Replaces the StatementSection child.
   * @param node The new node to replace the StatementSection child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setStatementSection(StatementSection node) {
    setChild(node, 3);
  }
  /**
   * Retrieves the StatementSection child.
   * @return The current node used as the StatementSection child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public StatementSection getStatementSection() {
    return (StatementSection)getChild(3);
  }
  /**
   * Retrieves the StatementSection child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the StatementSection child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public StatementSection getStatementSectionNoTransform() {
    return (StatementSection)getChildNoTransform(3);
  }
  /**
   * Replaces the lexeme ProgramEnd.
   * @param value The new value for the lexeme ProgramEnd.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setProgramEnd(String value) {
    tokenString_ProgramEnd = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_ProgramEnd;
  /**
   * @ast method 
   * 
   */
  
  public int ProgramEndstart;
  /**
   * @ast method 
   * 
   */
  
  public int ProgramEndend;
  /**
   * JastAdd-internal setter for lexeme ProgramEnd using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setProgramEnd(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setProgramEnd is only valid for String lexemes");
    tokenString_ProgramEnd = (String)symbol.value;
    ProgramEndstart = symbol.getStart();
    ProgramEndend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ProgramEnd.
   * @return The value for the lexeme ProgramEnd.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getProgramEnd() {
    return tokenString_ProgramEnd != null ? tokenString_ProgramEnd : "";
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
