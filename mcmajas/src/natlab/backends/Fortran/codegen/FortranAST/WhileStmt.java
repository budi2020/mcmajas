/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.Fortran.codegen.FortranAST;

/**
 * @production WhileStmt : {@link Statement} ::= <span class="component">&lt;Indent:String&gt;</span> <span class="component">&lt;Condition:String&gt;</span> <span class="component">WhileBlock:{@link StatementSection}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/FortranIR.ast:40
 */
public class WhileStmt extends Statement implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public WhileStmt clone() throws CloneNotSupportedException {
    WhileStmt node = (WhileStmt)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public WhileStmt copy() {
      try {
        WhileStmt node = (WhileStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public WhileStmt fullCopy() {
    try {
      WhileStmt tree = (WhileStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/PrettyPrinter.jadd:253
   */
  public void pp(StringBuffer sb) {
   	 	sb.append(getIndent());
    	sb.append("do while ("+getCondition()+")");
    	sb.append("\n");
    	getWhileBlock().pp(sb);
    	sb.append(getIndent()+"enddo");   	
    }
  /**
   * @ast method 
   * 
   */
  public WhileStmt() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public WhileStmt(String p0, String p1, StatementSection p2) {
    setIndent(p0);
    setCondition(p1);
    setChild(p2, 0);
  }
  /**
   * @ast method 
   * 
   */
  public WhileStmt(beaver.Symbol p0, beaver.Symbol p1, StatementSection p2) {
    setIndent(p0);
    setCondition(p1);
    setChild(p2, 0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the lexeme Indent.
   * @param value The new value for the lexeme Indent.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIndent(String value) {
    tokenString_Indent = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Indent;
  /**
   * @ast method 
   * 
   */
  
  public int Indentstart;
  /**
   * @ast method 
   * 
   */
  
  public int Indentend;
  /**
   * JastAdd-internal setter for lexeme Indent using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setIndent(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setIndent is only valid for String lexemes");
    tokenString_Indent = (String)symbol.value;
    Indentstart = symbol.getStart();
    Indentend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Indent.
   * @return The value for the lexeme Indent.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getIndent() {
    return tokenString_Indent != null ? tokenString_Indent : "";
  }
  /**
   * Replaces the lexeme Condition.
   * @param value The new value for the lexeme Condition.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setCondition(String value) {
    tokenString_Condition = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Condition;
  /**
   * @ast method 
   * 
   */
  
  public int Conditionstart;
  /**
   * @ast method 
   * 
   */
  
  public int Conditionend;
  /**
   * JastAdd-internal setter for lexeme Condition using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setCondition(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setCondition is only valid for String lexemes");
    tokenString_Condition = (String)symbol.value;
    Conditionstart = symbol.getStart();
    Conditionend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Condition.
   * @return The value for the lexeme Condition.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getCondition() {
    return tokenString_Condition != null ? tokenString_Condition : "";
  }
  /**
   * Replaces the WhileBlock child.
   * @param node The new node to replace the WhileBlock child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setWhileBlock(StatementSection node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the WhileBlock child.
   * @return The current node used as the WhileBlock child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public StatementSection getWhileBlock() {
    return (StatementSection)getChild(0);
  }
  /**
   * Retrieves the WhileBlock child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the WhileBlock child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public StatementSection getWhileBlockNoTransform() {
    return (StatementSection)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
