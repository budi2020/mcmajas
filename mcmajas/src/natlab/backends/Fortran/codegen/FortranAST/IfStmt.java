/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.Fortran.codegen.FortranAST;

/**
 * @production IfStmt : {@link Statement} ::= <span class="component">&lt;Indent:String&gt;</span> <span class="component">&lt;Condition:String&gt;</span> <span class="component">IfBlock:{@link StatementSection}</span> <span class="component">[ElseBlock:{@link StatementSection}]</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/FortranIR.ast:38
 */
public class IfStmt extends Statement implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IfStmt clone() throws CloneNotSupportedException {
    IfStmt node = (IfStmt)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IfStmt copy() {
      try {
        IfStmt node = (IfStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IfStmt fullCopy() {
    try {
      IfStmt tree = (IfStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/PrettyPrinter.jadd:241
   */
  public void pp(StringBuffer sb) {
    	sb.append(getIndent());
    	sb.append("if ("+getCondition()+") then\n");
    	getIfBlock().pp(sb);
    	if(hasElseBlock()) {
    		sb.append(getIndent());
    		sb.append("else\n");
    		getElseBlock().pp(sb);
    	}
    	sb.append(getIndent()+"endif");
    }
  /**
   * @ast method 
   * 
   */
  public IfStmt() {
    super();

    setChild(new Opt(), 1);

  }
  /**
   * @ast method 
   * 
   */
  public IfStmt(String p0, String p1, StatementSection p2, Opt<StatementSection> p3) {
    setIndent(p0);
    setCondition(p1);
    setChild(p2, 0);
    setChild(p3, 1);
  }
  /**
   * @ast method 
   * 
   */
  public IfStmt(beaver.Symbol p0, beaver.Symbol p1, StatementSection p2, Opt<StatementSection> p3) {
    setIndent(p0);
    setCondition(p1);
    setChild(p2, 0);
    setChild(p3, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the lexeme Indent.
   * @param value The new value for the lexeme Indent.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIndent(String value) {
    tokenString_Indent = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Indent;
  /**
   * @ast method 
   * 
   */
  
  public int Indentstart;
  /**
   * @ast method 
   * 
   */
  
  public int Indentend;
  /**
   * JastAdd-internal setter for lexeme Indent using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setIndent(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setIndent is only valid for String lexemes");
    tokenString_Indent = (String)symbol.value;
    Indentstart = symbol.getStart();
    Indentend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Indent.
   * @return The value for the lexeme Indent.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getIndent() {
    return tokenString_Indent != null ? tokenString_Indent : "";
  }
  /**
   * Replaces the lexeme Condition.
   * @param value The new value for the lexeme Condition.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setCondition(String value) {
    tokenString_Condition = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Condition;
  /**
   * @ast method 
   * 
   */
  
  public int Conditionstart;
  /**
   * @ast method 
   * 
   */
  
  public int Conditionend;
  /**
   * JastAdd-internal setter for lexeme Condition using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setCondition(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setCondition is only valid for String lexemes");
    tokenString_Condition = (String)symbol.value;
    Conditionstart = symbol.getStart();
    Conditionend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Condition.
   * @return The value for the lexeme Condition.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getCondition() {
    return tokenString_Condition != null ? tokenString_Condition : "";
  }
  /**
   * Replaces the IfBlock child.
   * @param node The new node to replace the IfBlock child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIfBlock(StatementSection node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the IfBlock child.
   * @return The current node used as the IfBlock child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public StatementSection getIfBlock() {
    return (StatementSection)getChild(0);
  }
  /**
   * Retrieves the IfBlock child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the IfBlock child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public StatementSection getIfBlockNoTransform() {
    return (StatementSection)getChildNoTransform(0);
  }
  /**
   * Replaces the optional node for the ElseBlock child. This is the {@code Opt} node containing the child ElseBlock, not the actual child!
   * @param opt The new node to be used as the optional node for the ElseBlock child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setElseBlockOpt(Opt<StatementSection> opt) {
    setChild(opt, 1);
  }
  /**
   * Check whether the optional ElseBlock child exists.
   * @return {@code true} if the optional ElseBlock child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasElseBlock() {
    return getElseBlockOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) ElseBlock child.
   * @return The ElseBlock child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public StatementSection getElseBlock() {
    return (StatementSection)getElseBlockOpt().getChild(0);
  }
  /**
   * Replaces the (optional) ElseBlock child.
   * @param node The new node to be used as the ElseBlock child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setElseBlock(StatementSection node) {
    getElseBlockOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the ElseBlock child. This is the {@code Opt} node containing the child ElseBlock, not the actual child!
   * @return The optional node for child the ElseBlock child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<StatementSection> getElseBlockOpt() {
    return (Opt<StatementSection>)getChild(1);
  }
  /**
   * Retrieves the optional node for child ElseBlock. This is the {@code Opt} node containing the child ElseBlock, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child ElseBlock.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<StatementSection> getElseBlockOptNoTransform() {
    return (Opt<StatementSection>)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
