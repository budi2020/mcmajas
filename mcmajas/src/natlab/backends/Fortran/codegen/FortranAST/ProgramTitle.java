/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.Fortran.codegen.FortranAST;

/**
 * @production ProgramTitle : {@link ASTNode} ::= <span class="component">&lt;ProgramType:String&gt;</span> <span class="component">&lt;ProgramName:String&gt;</span> <span class="component">[{@link ProgramParameterList}]</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/FortranIR.ast:5
 */
public class ProgramTitle extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ProgramTitle clone() throws CloneNotSupportedException {
    ProgramTitle node = (ProgramTitle)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ProgramTitle copy() {
      try {
        ProgramTitle node = (ProgramTitle)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ProgramTitle fullCopy() {
    try {
      ProgramTitle tree = (ProgramTitle) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/PrettyPrinter.jadd:21
   */
  public void pp(StringBuffer sb) {
	    sb.append(getProgramType()+" "+getProgramName());
	    if(hasProgramParameterList()) {
	    	sb.append("(");
	    	getProgramParameterList().pp(sb);
	    	sb.append(")");
	    }
	    sb.append("\nimplicit none\n");
	}
  /**
   * @ast method 
   * 
   */
  public ProgramTitle() {
    super();

    setChild(new Opt(), 0);

  }
  /**
   * @ast method 
   * 
   */
  public ProgramTitle(String p0, String p1, Opt<ProgramParameterList> p2) {
    setProgramType(p0);
    setProgramName(p1);
    setChild(p2, 0);
  }
  /**
   * @ast method 
   * 
   */
  public ProgramTitle(beaver.Symbol p0, beaver.Symbol p1, Opt<ProgramParameterList> p2) {
    setProgramType(p0);
    setProgramName(p1);
    setChild(p2, 0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the lexeme ProgramType.
   * @param value The new value for the lexeme ProgramType.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setProgramType(String value) {
    tokenString_ProgramType = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_ProgramType;
  /**
   * @ast method 
   * 
   */
  
  public int ProgramTypestart;
  /**
   * @ast method 
   * 
   */
  
  public int ProgramTypeend;
  /**
   * JastAdd-internal setter for lexeme ProgramType using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setProgramType(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setProgramType is only valid for String lexemes");
    tokenString_ProgramType = (String)symbol.value;
    ProgramTypestart = symbol.getStart();
    ProgramTypeend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ProgramType.
   * @return The value for the lexeme ProgramType.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getProgramType() {
    return tokenString_ProgramType != null ? tokenString_ProgramType : "";
  }
  /**
   * Replaces the lexeme ProgramName.
   * @param value The new value for the lexeme ProgramName.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setProgramName(String value) {
    tokenString_ProgramName = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_ProgramName;
  /**
   * @ast method 
   * 
   */
  
  public int ProgramNamestart;
  /**
   * @ast method 
   * 
   */
  
  public int ProgramNameend;
  /**
   * JastAdd-internal setter for lexeme ProgramName using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setProgramName(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setProgramName is only valid for String lexemes");
    tokenString_ProgramName = (String)symbol.value;
    ProgramNamestart = symbol.getStart();
    ProgramNameend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ProgramName.
   * @return The value for the lexeme ProgramName.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getProgramName() {
    return tokenString_ProgramName != null ? tokenString_ProgramName : "";
  }
  /**
   * Replaces the optional node for the ProgramParameterList child. This is the {@code Opt} node containing the child ProgramParameterList, not the actual child!
   * @param opt The new node to be used as the optional node for the ProgramParameterList child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setProgramParameterListOpt(Opt<ProgramParameterList> opt) {
    setChild(opt, 0);
  }
  /**
   * Check whether the optional ProgramParameterList child exists.
   * @return {@code true} if the optional ProgramParameterList child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasProgramParameterList() {
    return getProgramParameterListOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) ProgramParameterList child.
   * @return The ProgramParameterList child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ProgramParameterList getProgramParameterList() {
    return (ProgramParameterList)getProgramParameterListOpt().getChild(0);
  }
  /**
   * Replaces the (optional) ProgramParameterList child.
   * @param node The new node to be used as the ProgramParameterList child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setProgramParameterList(ProgramParameterList node) {
    getProgramParameterListOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the ProgramParameterList child. This is the {@code Opt} node containing the child ProgramParameterList, not the actual child!
   * @return The optional node for child the ProgramParameterList child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<ProgramParameterList> getProgramParameterListOpt() {
    return (Opt<ProgramParameterList>)getChild(0);
  }
  /**
   * Retrieves the optional node for child ProgramParameterList. This is the {@code Opt} node containing the child ProgramParameterList, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child ProgramParameterList.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<ProgramParameterList> getProgramParameterListOptNoTransform() {
    return (Opt<ProgramParameterList>)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
