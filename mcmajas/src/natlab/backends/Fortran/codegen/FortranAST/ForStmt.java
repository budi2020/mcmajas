/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.Fortran.codegen.FortranAST;

/**
 * @production ForStmt : {@link Statement} ::= <span class="component">&lt;Indent:String&gt;</span> <span class="component">&lt;LoopVar:String&gt;</span> <span class="component">&lt;LowBoundary:String&gt;</span> <span class="component">[{@link Inc}]</span> <span class="component">&lt;UpperBoundary:String&gt;</span> <span class="component">ForBlock:{@link StatementSection}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/FortranIR.ast:42
 */
public class ForStmt extends Statement implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ForStmt clone() throws CloneNotSupportedException {
    ForStmt node = (ForStmt)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ForStmt copy() {
      try {
        ForStmt node = (ForStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ForStmt fullCopy() {
    try {
      ForStmt tree = (ForStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/PrettyPrinter.jadd:261
   */
  public void pp(StringBuffer sb) {
    	sb.append(getIndent());
    	sb.append("do "+getLoopVar()+" = "+getLowBoundary()+" , ");
    	if(hasInc()) {
    		getInc().pp(sb);
    		sb.append(" , ");
    	}
    	sb.append(getUpperBoundary());
    	sb.append("\n");
    	getForBlock().pp(sb);
    	sb.append(getIndent()+"enddo");    	
    }
  /**
   * @ast method 
   * 
   */
  public ForStmt() {
    super();

    setChild(new Opt(), 0);

  }
  /**
   * @ast method 
   * 
   */
  public ForStmt(String p0, String p1, String p2, Opt<Inc> p3, String p4, StatementSection p5) {
    setIndent(p0);
    setLoopVar(p1);
    setLowBoundary(p2);
    setChild(p3, 0);
    setUpperBoundary(p4);
    setChild(p5, 1);
  }
  /**
   * @ast method 
   * 
   */
  public ForStmt(beaver.Symbol p0, beaver.Symbol p1, beaver.Symbol p2, Opt<Inc> p3, beaver.Symbol p4, StatementSection p5) {
    setIndent(p0);
    setLoopVar(p1);
    setLowBoundary(p2);
    setChild(p3, 0);
    setUpperBoundary(p4);
    setChild(p5, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the lexeme Indent.
   * @param value The new value for the lexeme Indent.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIndent(String value) {
    tokenString_Indent = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Indent;
  /**
   * @ast method 
   * 
   */
  
  public int Indentstart;
  /**
   * @ast method 
   * 
   */
  
  public int Indentend;
  /**
   * JastAdd-internal setter for lexeme Indent using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setIndent(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setIndent is only valid for String lexemes");
    tokenString_Indent = (String)symbol.value;
    Indentstart = symbol.getStart();
    Indentend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Indent.
   * @return The value for the lexeme Indent.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getIndent() {
    return tokenString_Indent != null ? tokenString_Indent : "";
  }
  /**
   * Replaces the lexeme LoopVar.
   * @param value The new value for the lexeme LoopVar.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setLoopVar(String value) {
    tokenString_LoopVar = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_LoopVar;
  /**
   * @ast method 
   * 
   */
  
  public int LoopVarstart;
  /**
   * @ast method 
   * 
   */
  
  public int LoopVarend;
  /**
   * JastAdd-internal setter for lexeme LoopVar using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setLoopVar(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setLoopVar is only valid for String lexemes");
    tokenString_LoopVar = (String)symbol.value;
    LoopVarstart = symbol.getStart();
    LoopVarend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme LoopVar.
   * @return The value for the lexeme LoopVar.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getLoopVar() {
    return tokenString_LoopVar != null ? tokenString_LoopVar : "";
  }
  /**
   * Replaces the lexeme LowBoundary.
   * @param value The new value for the lexeme LowBoundary.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setLowBoundary(String value) {
    tokenString_LowBoundary = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_LowBoundary;
  /**
   * @ast method 
   * 
   */
  
  public int LowBoundarystart;
  /**
   * @ast method 
   * 
   */
  
  public int LowBoundaryend;
  /**
   * JastAdd-internal setter for lexeme LowBoundary using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setLowBoundary(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setLowBoundary is only valid for String lexemes");
    tokenString_LowBoundary = (String)symbol.value;
    LowBoundarystart = symbol.getStart();
    LowBoundaryend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme LowBoundary.
   * @return The value for the lexeme LowBoundary.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getLowBoundary() {
    return tokenString_LowBoundary != null ? tokenString_LowBoundary : "";
  }
  /**
   * Replaces the optional node for the Inc child. This is the {@code Opt} node containing the child Inc, not the actual child!
   * @param opt The new node to be used as the optional node for the Inc child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setIncOpt(Opt<Inc> opt) {
    setChild(opt, 0);
  }
  /**
   * Check whether the optional Inc child exists.
   * @return {@code true} if the optional Inc child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasInc() {
    return getIncOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) Inc child.
   * @return The Inc child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Inc getInc() {
    return (Inc)getIncOpt().getChild(0);
  }
  /**
   * Replaces the (optional) Inc child.
   * @param node The new node to be used as the Inc child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setInc(Inc node) {
    getIncOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the Inc child. This is the {@code Opt} node containing the child Inc, not the actual child!
   * @return The optional node for child the Inc child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<Inc> getIncOpt() {
    return (Opt<Inc>)getChild(0);
  }
  /**
   * Retrieves the optional node for child Inc. This is the {@code Opt} node containing the child Inc, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child Inc.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<Inc> getIncOptNoTransform() {
    return (Opt<Inc>)getChildNoTransform(0);
  }
  /**
   * Replaces the lexeme UpperBoundary.
   * @param value The new value for the lexeme UpperBoundary.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setUpperBoundary(String value) {
    tokenString_UpperBoundary = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_UpperBoundary;
  /**
   * @ast method 
   * 
   */
  
  public int UpperBoundarystart;
  /**
   * @ast method 
   * 
   */
  
  public int UpperBoundaryend;
  /**
   * JastAdd-internal setter for lexeme UpperBoundary using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setUpperBoundary(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setUpperBoundary is only valid for String lexemes");
    tokenString_UpperBoundary = (String)symbol.value;
    UpperBoundarystart = symbol.getStart();
    UpperBoundaryend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme UpperBoundary.
   * @return The value for the lexeme UpperBoundary.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getUpperBoundary() {
    return tokenString_UpperBoundary != null ? tokenString_UpperBoundary : "";
  }
  /**
   * Replaces the ForBlock child.
   * @param node The new node to replace the ForBlock child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setForBlock(StatementSection node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the ForBlock child.
   * @return The current node used as the ForBlock child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public StatementSection getForBlock() {
    return (StatementSection)getChild(1);
  }
  /**
   * Retrieves the ForBlock child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the ForBlock child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public StatementSection getForBlockNoTransform() {
    return (StatementSection)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
