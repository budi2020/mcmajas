/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.Fortran.codegen.FortranAST;

/**
 * @production ArrayGetStmt : {@link Statement} ::= <span class="component">&lt;Indent:String&gt;</span> <span class="component">[{@link RuntimeCheck}]</span> <span class="component">[{@link ArrayConvert}]</span> <span class="component">&lt;lhsVariable:String&gt;</span> <span class="component">[{@link lhsIndex}]</span> <span class="component">&lt;rhsVariable:String&gt;</span> <span class="component">&lt;rhsIndex:String&gt;</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/FortranIR.ast:45
 */
public class ArrayGetStmt extends Statement implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArrayGetStmt clone() throws CloneNotSupportedException {
    ArrayGetStmt node = (ArrayGetStmt)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArrayGetStmt copy() {
      try {
        ArrayGetStmt node = (ArrayGetStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArrayGetStmt fullCopy() {
    try {
      ArrayGetStmt tree = (ArrayGetStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/PrettyPrinter.jadd:278
   */
  public void pp(StringBuffer sb) {
    	if(hasRuntimeCheck()) {
    		sb.append(getRuntimeCheck()+"\n");
    	}
    	if(hasArrayConvert()) {
    		getArrayConvert().pp(sb);
    	}
    	sb.append(getIndent());
    	sb.append(getlhsVariable());
    	if(haslhsIndex()) {
    		sb.append("(");
    		getlhsIndex().pp(sb);
    		sb.append(")");
    	}
    	sb.append(" = "+getrhsVariable()+"("+getrhsIndex()+");");
    }
  /**
   * @ast method 
   * 
   */
  public ArrayGetStmt() {
    super();

    setChild(new Opt(), 0);
    setChild(new Opt(), 1);
    setChild(new Opt(), 2);

  }
  /**
   * @ast method 
   * 
   */
  public ArrayGetStmt(String p0, Opt<RuntimeCheck> p1, Opt<ArrayConvert> p2, String p3, Opt<lhsIndex> p4, String p5, String p6) {
    setIndent(p0);
    setChild(p1, 0);
    setChild(p2, 1);
    setlhsVariable(p3);
    setChild(p4, 2);
    setrhsVariable(p5);
    setrhsIndex(p6);
  }
  /**
   * @ast method 
   * 
   */
  public ArrayGetStmt(beaver.Symbol p0, Opt<RuntimeCheck> p1, Opt<ArrayConvert> p2, beaver.Symbol p3, Opt<lhsIndex> p4, beaver.Symbol p5, beaver.Symbol p6) {
    setIndent(p0);
    setChild(p1, 0);
    setChild(p2, 1);
    setlhsVariable(p3);
    setChild(p4, 2);
    setrhsVariable(p5);
    setrhsIndex(p6);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 3;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the lexeme Indent.
   * @param value The new value for the lexeme Indent.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIndent(String value) {
    tokenString_Indent = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Indent;
  /**
   * @ast method 
   * 
   */
  
  public int Indentstart;
  /**
   * @ast method 
   * 
   */
  
  public int Indentend;
  /**
   * JastAdd-internal setter for lexeme Indent using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setIndent(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setIndent is only valid for String lexemes");
    tokenString_Indent = (String)symbol.value;
    Indentstart = symbol.getStart();
    Indentend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Indent.
   * @return The value for the lexeme Indent.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getIndent() {
    return tokenString_Indent != null ? tokenString_Indent : "";
  }
  /**
   * Replaces the optional node for the RuntimeCheck child. This is the {@code Opt} node containing the child RuntimeCheck, not the actual child!
   * @param opt The new node to be used as the optional node for the RuntimeCheck child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setRuntimeCheckOpt(Opt<RuntimeCheck> opt) {
    setChild(opt, 0);
  }
  /**
   * Check whether the optional RuntimeCheck child exists.
   * @return {@code true} if the optional RuntimeCheck child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasRuntimeCheck() {
    return getRuntimeCheckOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) RuntimeCheck child.
   * @return The RuntimeCheck child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public RuntimeCheck getRuntimeCheck() {
    return (RuntimeCheck)getRuntimeCheckOpt().getChild(0);
  }
  /**
   * Replaces the (optional) RuntimeCheck child.
   * @param node The new node to be used as the RuntimeCheck child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setRuntimeCheck(RuntimeCheck node) {
    getRuntimeCheckOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the RuntimeCheck child. This is the {@code Opt} node containing the child RuntimeCheck, not the actual child!
   * @return The optional node for child the RuntimeCheck child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<RuntimeCheck> getRuntimeCheckOpt() {
    return (Opt<RuntimeCheck>)getChild(0);
  }
  /**
   * Retrieves the optional node for child RuntimeCheck. This is the {@code Opt} node containing the child RuntimeCheck, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child RuntimeCheck.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<RuntimeCheck> getRuntimeCheckOptNoTransform() {
    return (Opt<RuntimeCheck>)getChildNoTransform(0);
  }
  /**
   * Replaces the optional node for the ArrayConvert child. This is the {@code Opt} node containing the child ArrayConvert, not the actual child!
   * @param opt The new node to be used as the optional node for the ArrayConvert child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setArrayConvertOpt(Opt<ArrayConvert> opt) {
    setChild(opt, 1);
  }
  /**
   * Check whether the optional ArrayConvert child exists.
   * @return {@code true} if the optional ArrayConvert child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasArrayConvert() {
    return getArrayConvertOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) ArrayConvert child.
   * @return The ArrayConvert child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArrayConvert getArrayConvert() {
    return (ArrayConvert)getArrayConvertOpt().getChild(0);
  }
  /**
   * Replaces the (optional) ArrayConvert child.
   * @param node The new node to be used as the ArrayConvert child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setArrayConvert(ArrayConvert node) {
    getArrayConvertOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the ArrayConvert child. This is the {@code Opt} node containing the child ArrayConvert, not the actual child!
   * @return The optional node for child the ArrayConvert child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<ArrayConvert> getArrayConvertOpt() {
    return (Opt<ArrayConvert>)getChild(1);
  }
  /**
   * Retrieves the optional node for child ArrayConvert. This is the {@code Opt} node containing the child ArrayConvert, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child ArrayConvert.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<ArrayConvert> getArrayConvertOptNoTransform() {
    return (Opt<ArrayConvert>)getChildNoTransform(1);
  }
  /**
   * Replaces the lexeme lhsVariable.
   * @param value The new value for the lexeme lhsVariable.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setlhsVariable(String value) {
    tokenString_lhsVariable = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_lhsVariable;
  /**
   * @ast method 
   * 
   */
  
  public int lhsVariablestart;
  /**
   * @ast method 
   * 
   */
  
  public int lhsVariableend;
  /**
   * JastAdd-internal setter for lexeme lhsVariable using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setlhsVariable(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setlhsVariable is only valid for String lexemes");
    tokenString_lhsVariable = (String)symbol.value;
    lhsVariablestart = symbol.getStart();
    lhsVariableend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme lhsVariable.
   * @return The value for the lexeme lhsVariable.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getlhsVariable() {
    return tokenString_lhsVariable != null ? tokenString_lhsVariable : "";
  }
  /**
   * Replaces the optional node for the lhsIndex child. This is the {@code Opt} node containing the child lhsIndex, not the actual child!
   * @param opt The new node to be used as the optional node for the lhsIndex child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setlhsIndexOpt(Opt<lhsIndex> opt) {
    setChild(opt, 2);
  }
  /**
   * Check whether the optional lhsIndex child exists.
   * @return {@code true} if the optional lhsIndex child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean haslhsIndex() {
    return getlhsIndexOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) lhsIndex child.
   * @return The lhsIndex child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public lhsIndex getlhsIndex() {
    return (lhsIndex)getlhsIndexOpt().getChild(0);
  }
  /**
   * Replaces the (optional) lhsIndex child.
   * @param node The new node to be used as the lhsIndex child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setlhsIndex(lhsIndex node) {
    getlhsIndexOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the lhsIndex child. This is the {@code Opt} node containing the child lhsIndex, not the actual child!
   * @return The optional node for child the lhsIndex child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<lhsIndex> getlhsIndexOpt() {
    return (Opt<lhsIndex>)getChild(2);
  }
  /**
   * Retrieves the optional node for child lhsIndex. This is the {@code Opt} node containing the child lhsIndex, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child lhsIndex.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<lhsIndex> getlhsIndexOptNoTransform() {
    return (Opt<lhsIndex>)getChildNoTransform(2);
  }
  /**
   * Replaces the lexeme rhsVariable.
   * @param value The new value for the lexeme rhsVariable.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setrhsVariable(String value) {
    tokenString_rhsVariable = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_rhsVariable;
  /**
   * @ast method 
   * 
   */
  
  public int rhsVariablestart;
  /**
   * @ast method 
   * 
   */
  
  public int rhsVariableend;
  /**
   * JastAdd-internal setter for lexeme rhsVariable using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setrhsVariable(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setrhsVariable is only valid for String lexemes");
    tokenString_rhsVariable = (String)symbol.value;
    rhsVariablestart = symbol.getStart();
    rhsVariableend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme rhsVariable.
   * @return The value for the lexeme rhsVariable.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getrhsVariable() {
    return tokenString_rhsVariable != null ? tokenString_rhsVariable : "";
  }
  /**
   * Replaces the lexeme rhsIndex.
   * @param value The new value for the lexeme rhsIndex.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setrhsIndex(String value) {
    tokenString_rhsIndex = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_rhsIndex;
  /**
   * @ast method 
   * 
   */
  
  public int rhsIndexstart;
  /**
   * @ast method 
   * 
   */
  
  public int rhsIndexend;
  /**
   * JastAdd-internal setter for lexeme rhsIndex using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setrhsIndex(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setrhsIndex is only valid for String lexemes");
    tokenString_rhsIndex = (String)symbol.value;
    rhsIndexstart = symbol.getStart();
    rhsIndexend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme rhsIndex.
   * @return The value for the lexeme rhsIndex.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getrhsIndex() {
    return tokenString_rhsIndex != null ? tokenString_rhsIndex : "";
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
