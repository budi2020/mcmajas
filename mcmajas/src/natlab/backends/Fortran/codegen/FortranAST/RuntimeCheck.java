/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.Fortran.codegen.FortranAST;

/**
 * @production RuntimeCheck : {@link ASTNode} ::= <span class="component">&lt;Block:String&gt;</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/FortranIR.ast:23
 */
public class RuntimeCheck extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public RuntimeCheck clone() throws CloneNotSupportedException {
    RuntimeCheck node = (RuntimeCheck)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public RuntimeCheck copy() {
      try {
        RuntimeCheck node = (RuntimeCheck)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public RuntimeCheck fullCopy() {
    try {
      RuntimeCheck tree = (RuntimeCheck) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * 
   */
  public RuntimeCheck() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public RuntimeCheck(String p0) {
    setBlock(p0);
  }
  /**
   * @ast method 
   * 
   */
  public RuntimeCheck(beaver.Symbol p0) {
    setBlock(p0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the lexeme Block.
   * @param value The new value for the lexeme Block.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setBlock(String value) {
    tokenString_Block = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Block;
  /**
   * @ast method 
   * 
   */
  
  public int Blockstart;
  /**
   * @ast method 
   * 
   */
  
  public int Blockend;
  /**
   * JastAdd-internal setter for lexeme Block using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setBlock(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setBlock is only valid for String lexemes");
    tokenString_Block = (String)symbol.value;
    Blockstart = symbol.getStart();
    Blockend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Block.
   * @return The value for the lexeme Block.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getBlock() {
    return tokenString_Block != null ? tokenString_Block : "";
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
