/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.Fortran.codegen.FortranAST;

/**
 * @production UnaryExpr : {@link AbstractAssignToListStmt} ::= <span class="component">&lt;Indent:String&gt;</span> <span class="component">[{@link RuntimeCheck}]</span> <span class="component">{@link Variable}*</span> <span class="component">&lt;Operator:String&gt;</span> <span class="component">&lt;Operand:String&gt;</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/FortranIR.ast:29
 */
public class UnaryExpr extends AbstractAssignToListStmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public UnaryExpr clone() throws CloneNotSupportedException {
    UnaryExpr node = (UnaryExpr)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public UnaryExpr copy() {
      try {
        UnaryExpr node = (UnaryExpr)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public UnaryExpr fullCopy() {
    try {
      UnaryExpr tree = (UnaryExpr) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/PrettyPrinter.jadd:155
   */
  public void pp(StringBuffer sb) {
    	if(hasRuntimeCheck()) {
    		sb.append(getRuntimeCheck()+"\n");
    	}
    	sb.append(getIndent());
    	int size = getNumVariable();
    	for(int i=0;i<size;i++) {
    		getVariable(i).pp(sb);
    		if(i<size-1) {
        		sb.append(",");
        	}
    	}
    	sb.append(" = "+getOperator()+" "+getOperand()+";");
    }
  /**
   * @ast method 
   * 
   */
  public UnaryExpr() {
    super();

    setChild(new Opt(), 0);
    setChild(new List(), 1);

  }
  /**
   * @ast method 
   * 
   */
  public UnaryExpr(String p0, Opt<RuntimeCheck> p1, List<Variable> p2, String p3, String p4) {
    setIndent(p0);
    setChild(p1, 0);
    setChild(p2, 1);
    setOperator(p3);
    setOperand(p4);
  }
  /**
   * @ast method 
   * 
   */
  public UnaryExpr(beaver.Symbol p0, Opt<RuntimeCheck> p1, List<Variable> p2, beaver.Symbol p3, beaver.Symbol p4) {
    setIndent(p0);
    setChild(p1, 0);
    setChild(p2, 1);
    setOperator(p3);
    setOperand(p4);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the lexeme Indent.
   * @param value The new value for the lexeme Indent.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIndent(String value) {
    tokenString_Indent = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Indent;
  /**
   * @ast method 
   * 
   */
  
  public int Indentstart;
  /**
   * @ast method 
   * 
   */
  
  public int Indentend;
  /**
   * JastAdd-internal setter for lexeme Indent using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setIndent(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setIndent is only valid for String lexemes");
    tokenString_Indent = (String)symbol.value;
    Indentstart = symbol.getStart();
    Indentend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Indent.
   * @return The value for the lexeme Indent.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getIndent() {
    return tokenString_Indent != null ? tokenString_Indent : "";
  }
  /**
   * Replaces the optional node for the RuntimeCheck child. This is the {@code Opt} node containing the child RuntimeCheck, not the actual child!
   * @param opt The new node to be used as the optional node for the RuntimeCheck child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setRuntimeCheckOpt(Opt<RuntimeCheck> opt) {
    setChild(opt, 0);
  }
  /**
   * Check whether the optional RuntimeCheck child exists.
   * @return {@code true} if the optional RuntimeCheck child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasRuntimeCheck() {
    return getRuntimeCheckOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) RuntimeCheck child.
   * @return The RuntimeCheck child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public RuntimeCheck getRuntimeCheck() {
    return (RuntimeCheck)getRuntimeCheckOpt().getChild(0);
  }
  /**
   * Replaces the (optional) RuntimeCheck child.
   * @param node The new node to be used as the RuntimeCheck child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setRuntimeCheck(RuntimeCheck node) {
    getRuntimeCheckOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the RuntimeCheck child. This is the {@code Opt} node containing the child RuntimeCheck, not the actual child!
   * @return The optional node for child the RuntimeCheck child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<RuntimeCheck> getRuntimeCheckOpt() {
    return (Opt<RuntimeCheck>)getChild(0);
  }
  /**
   * Retrieves the optional node for child RuntimeCheck. This is the {@code Opt} node containing the child RuntimeCheck, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child RuntimeCheck.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<RuntimeCheck> getRuntimeCheckOptNoTransform() {
    return (Opt<RuntimeCheck>)getChildNoTransform(0);
  }
  /**
   * Replaces the Variable list.
   * @param list The new list node to be used as the Variable list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setVariableList(List<Variable> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Variable list.
   * @return Number of children in the Variable list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumVariable() {
    return getVariableList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Variable list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Variable list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumVariableNoTransform() {
    return getVariableListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Variable list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Variable list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Variable getVariable(int i) {
    return (Variable)getVariableList().getChild(i);
  }
  /**
   * Append an element to the Variable list.
   * @param node The element to append to the Variable list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addVariable(Variable node) {
    List<Variable> list = (parent == null || state == null) ? getVariableListNoTransform() : getVariableList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addVariableNoTransform(Variable node) {
    List<Variable> list = getVariableListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Variable list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setVariable(Variable node, int i) {
    List<Variable> list = getVariableList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Variable list.
   * @return The node representing the Variable list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Variable> getVariables() {
    return getVariableList();
  }
  /**
   * Retrieves the Variable list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Variable list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Variable> getVariablesNoTransform() {
    return getVariableListNoTransform();
  }
  /**
   * Retrieves the Variable list.
   * @return The node representing the Variable list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Variable> getVariableList() {
    List<Variable> list = (List<Variable>)getChild(1);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Variable list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Variable list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Variable> getVariableListNoTransform() {
    return (List<Variable>)getChildNoTransform(1);
  }
  /**
   * Replaces the lexeme Operator.
   * @param value The new value for the lexeme Operator.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setOperator(String value) {
    tokenString_Operator = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Operator;
  /**
   * @ast method 
   * 
   */
  
  public int Operatorstart;
  /**
   * @ast method 
   * 
   */
  
  public int Operatorend;
  /**
   * JastAdd-internal setter for lexeme Operator using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setOperator(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setOperator is only valid for String lexemes");
    tokenString_Operator = (String)symbol.value;
    Operatorstart = symbol.getStart();
    Operatorend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Operator.
   * @return The value for the lexeme Operator.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getOperator() {
    return tokenString_Operator != null ? tokenString_Operator : "";
  }
  /**
   * Replaces the lexeme Operand.
   * @param value The new value for the lexeme Operand.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setOperand(String value) {
    tokenString_Operand = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Operand;
  /**
   * @ast method 
   * 
   */
  
  public int Operandstart;
  /**
   * @ast method 
   * 
   */
  
  public int Operandend;
  /**
   * JastAdd-internal setter for lexeme Operand using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setOperand(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setOperand is only valid for String lexemes");
    tokenString_Operand = (String)symbol.value;
    Operandstart = symbol.getStart();
    Operandend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Operand.
   * @return The value for the lexeme Operand.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getOperand() {
    return tokenString_Operand != null ? tokenString_Operand : "";
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
