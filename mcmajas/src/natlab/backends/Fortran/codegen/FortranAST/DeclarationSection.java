/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.Fortran.codegen.FortranAST;

/**
 * @production DeclarationSection : {@link ASTNode} ::= <span class="component">{@link DeclStmt}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/FortranIR.ast:9
 */
public class DeclarationSection extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public DeclarationSection clone() throws CloneNotSupportedException {
    DeclarationSection node = (DeclarationSection)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public DeclarationSection copy() {
      try {
        DeclarationSection node = (DeclarationSection)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public DeclarationSection fullCopy() {
    try {
      DeclarationSection tree = (DeclarationSection) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/PrettyPrinter.jadd:45
   */
  public void pp(StringBuffer sb) {
    	int size = getNumDeclStmt();
    	for(int i=0;i<size;i++) {
    		getDeclStmt(i).pp(sb);
    		sb.append("\n");
    	}    
    }
  /**
   * @ast method 
   * 
   */
  public DeclarationSection() {
    super();

    setChild(new List(), 0);

  }
  /**
   * @ast method 
   * 
   */
  public DeclarationSection(List<DeclStmt> p0) {
    setChild(p0, 0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the DeclStmt list.
   * @param list The new list node to be used as the DeclStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setDeclStmtList(List<DeclStmt> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the DeclStmt list.
   * @return Number of children in the DeclStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumDeclStmt() {
    return getDeclStmtList().getNumChild();
  }
  /**
   * Retrieves the number of children in the DeclStmt list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the DeclStmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumDeclStmtNoTransform() {
    return getDeclStmtListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the DeclStmt list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the DeclStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public DeclStmt getDeclStmt(int i) {
    return (DeclStmt)getDeclStmtList().getChild(i);
  }
  /**
   * Append an element to the DeclStmt list.
   * @param node The element to append to the DeclStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addDeclStmt(DeclStmt node) {
    List<DeclStmt> list = (parent == null || state == null) ? getDeclStmtListNoTransform() : getDeclStmtList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addDeclStmtNoTransform(DeclStmt node) {
    List<DeclStmt> list = getDeclStmtListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the DeclStmt list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setDeclStmt(DeclStmt node, int i) {
    List<DeclStmt> list = getDeclStmtList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the DeclStmt list.
   * @return The node representing the DeclStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<DeclStmt> getDeclStmts() {
    return getDeclStmtList();
  }
  /**
   * Retrieves the DeclStmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the DeclStmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<DeclStmt> getDeclStmtsNoTransform() {
    return getDeclStmtListNoTransform();
  }
  /**
   * Retrieves the DeclStmt list.
   * @return The node representing the DeclStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<DeclStmt> getDeclStmtList() {
    List<DeclStmt> list = (List<DeclStmt>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the DeclStmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the DeclStmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<DeclStmt> getDeclStmtListNoTransform() {
    return (List<DeclStmt>)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
