/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.Fortran.codegen.FortranAST;

/**
 * @production IOOperationExpr : {@link AbstractAssignToListStmt} ::= <span class="component">&lt;Indent:String&gt;</span> <span class="component">[{@link RuntimeCheck}]</span> <span class="component">&lt;IOOperator:String&gt;</span> <span class="component">&lt;ArgsList:String&gt;</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/FortranIR.ast:33
 */
public class IOOperationExpr extends AbstractAssignToListStmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IOOperationExpr clone() throws CloneNotSupportedException {
    IOOperationExpr node = (IOOperationExpr)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IOOperationExpr copy() {
      try {
        IOOperationExpr node = (IOOperationExpr)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IOOperationExpr fullCopy() {
    try {
      IOOperationExpr tree = (IOOperationExpr) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/PrettyPrinter.jadd:210
   */
  public void pp(StringBuffer sb) {
    	if(hasRuntimeCheck()) {
    		sb.append(getRuntimeCheck()+"\n");
    	}
    	sb.append(getIndent());
    	sb.append(getIOOperator()+getArgsList()+";");
    }
  /**
   * @ast method 
   * 
   */
  public IOOperationExpr() {
    super();

    setChild(new Opt(), 0);

  }
  /**
   * @ast method 
   * 
   */
  public IOOperationExpr(String p0, Opt<RuntimeCheck> p1, String p2, String p3) {
    setIndent(p0);
    setChild(p1, 0);
    setIOOperator(p2);
    setArgsList(p3);
  }
  /**
   * @ast method 
   * 
   */
  public IOOperationExpr(beaver.Symbol p0, Opt<RuntimeCheck> p1, beaver.Symbol p2, beaver.Symbol p3) {
    setIndent(p0);
    setChild(p1, 0);
    setIOOperator(p2);
    setArgsList(p3);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the lexeme Indent.
   * @param value The new value for the lexeme Indent.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIndent(String value) {
    tokenString_Indent = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Indent;
  /**
   * @ast method 
   * 
   */
  
  public int Indentstart;
  /**
   * @ast method 
   * 
   */
  
  public int Indentend;
  /**
   * JastAdd-internal setter for lexeme Indent using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setIndent(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setIndent is only valid for String lexemes");
    tokenString_Indent = (String)symbol.value;
    Indentstart = symbol.getStart();
    Indentend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Indent.
   * @return The value for the lexeme Indent.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getIndent() {
    return tokenString_Indent != null ? tokenString_Indent : "";
  }
  /**
   * Replaces the optional node for the RuntimeCheck child. This is the {@code Opt} node containing the child RuntimeCheck, not the actual child!
   * @param opt The new node to be used as the optional node for the RuntimeCheck child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setRuntimeCheckOpt(Opt<RuntimeCheck> opt) {
    setChild(opt, 0);
  }
  /**
   * Check whether the optional RuntimeCheck child exists.
   * @return {@code true} if the optional RuntimeCheck child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasRuntimeCheck() {
    return getRuntimeCheckOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) RuntimeCheck child.
   * @return The RuntimeCheck child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public RuntimeCheck getRuntimeCheck() {
    return (RuntimeCheck)getRuntimeCheckOpt().getChild(0);
  }
  /**
   * Replaces the (optional) RuntimeCheck child.
   * @param node The new node to be used as the RuntimeCheck child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setRuntimeCheck(RuntimeCheck node) {
    getRuntimeCheckOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the RuntimeCheck child. This is the {@code Opt} node containing the child RuntimeCheck, not the actual child!
   * @return The optional node for child the RuntimeCheck child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<RuntimeCheck> getRuntimeCheckOpt() {
    return (Opt<RuntimeCheck>)getChild(0);
  }
  /**
   * Retrieves the optional node for child RuntimeCheck. This is the {@code Opt} node containing the child RuntimeCheck, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child RuntimeCheck.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<RuntimeCheck> getRuntimeCheckOptNoTransform() {
    return (Opt<RuntimeCheck>)getChildNoTransform(0);
  }
  /**
   * Replaces the lexeme IOOperator.
   * @param value The new value for the lexeme IOOperator.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIOOperator(String value) {
    tokenString_IOOperator = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_IOOperator;
  /**
   * @ast method 
   * 
   */
  
  public int IOOperatorstart;
  /**
   * @ast method 
   * 
   */
  
  public int IOOperatorend;
  /**
   * JastAdd-internal setter for lexeme IOOperator using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setIOOperator(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setIOOperator is only valid for String lexemes");
    tokenString_IOOperator = (String)symbol.value;
    IOOperatorstart = symbol.getStart();
    IOOperatorend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme IOOperator.
   * @return The value for the lexeme IOOperator.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getIOOperator() {
    return tokenString_IOOperator != null ? tokenString_IOOperator : "";
  }
  /**
   * Replaces the lexeme ArgsList.
   * @param value The new value for the lexeme ArgsList.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setArgsList(String value) {
    tokenString_ArgsList = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_ArgsList;
  /**
   * @ast method 
   * 
   */
  
  public int ArgsListstart;
  /**
   * @ast method 
   * 
   */
  
  public int ArgsListend;
  /**
   * JastAdd-internal setter for lexeme ArgsList using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setArgsList(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setArgsList is only valid for String lexemes");
    tokenString_ArgsList = (String)symbol.value;
    ArgsListstart = symbol.getStart();
    ArgsListend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ArgsList.
   * @return The value for the lexeme ArgsList.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getArgsList() {
    return tokenString_ArgsList != null ? tokenString_ArgsList : "";
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
