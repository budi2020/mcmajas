/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.Fortran.codegen.FortranAST;

/**
 * @production BackupVar : {@link ASTNode} ::= <span class="component">&lt;Stmt:String&gt;</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/FortranIR.ast:3
 */
public class BackupVar extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public BackupVar clone() throws CloneNotSupportedException {
    BackupVar node = (BackupVar)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public BackupVar copy() {
      try {
        BackupVar node = (BackupVar)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public BackupVar fullCopy() {
    try {
      BackupVar tree = (BackupVar) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/PrettyPrinter.jadd:17
   */
  public void pp(StringBuffer sb) {
    	sb.append(getStmt()+"\n");
    }
  /**
   * @ast method 
   * 
   */
  public BackupVar() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public BackupVar(String p0) {
    setStmt(p0);
  }
  /**
   * @ast method 
   * 
   */
  public BackupVar(beaver.Symbol p0) {
    setStmt(p0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the lexeme Stmt.
   * @param value The new value for the lexeme Stmt.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setStmt(String value) {
    tokenString_Stmt = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Stmt;
  /**
   * @ast method 
   * 
   */
  
  public int Stmtstart;
  /**
   * @ast method 
   * 
   */
  
  public int Stmtend;
  /**
   * JastAdd-internal setter for lexeme Stmt using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setStmt(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setStmt is only valid for String lexemes");
    tokenString_Stmt = (String)symbol.value;
    Stmtstart = symbol.getStart();
    Stmtend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Stmt.
   * @return The value for the lexeme Stmt.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getStmt() {
    return tokenString_Stmt != null ? tokenString_Stmt : "";
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
