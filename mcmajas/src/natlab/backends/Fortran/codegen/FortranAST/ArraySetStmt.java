/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.Fortran.codegen.FortranAST;

/**
 * @production ArraySetStmt : {@link Statement} ::= <span class="component">&lt;Indent:String&gt;</span> <span class="component">[{@link RuntimeCheck}]</span> <span class="component">&lt;lhsVariable:String&gt;</span> <span class="component">&lt;lhsIndex:String&gt;</span> <span class="component">&lt;rhsVariable:String&gt;</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/FortranIR.ast:49
 */
public class ArraySetStmt extends Statement implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArraySetStmt clone() throws CloneNotSupportedException {
    ArraySetStmt node = (ArraySetStmt)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArraySetStmt copy() {
      try {
        ArraySetStmt node = (ArraySetStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArraySetStmt fullCopy() {
    try {
      ArraySetStmt tree = (ArraySetStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/PrettyPrinter.jadd:303
   */
  public void pp(StringBuffer sb) {
    	if(hasRuntimeCheck()) {
    	    sb.append(getRuntimeCheck()+"\n");
    	}
    	sb.append(getIndent());
    	sb.append(getlhsVariable()+"("+getlhsIndex()+") = "+getrhsVariable()+";");
    }
  /**
   * @ast method 
   * 
   */
  public ArraySetStmt() {
    super();

    setChild(new Opt(), 0);

  }
  /**
   * @ast method 
   * 
   */
  public ArraySetStmt(String p0, Opt<RuntimeCheck> p1, String p2, String p3, String p4) {
    setIndent(p0);
    setChild(p1, 0);
    setlhsVariable(p2);
    setlhsIndex(p3);
    setrhsVariable(p4);
  }
  /**
   * @ast method 
   * 
   */
  public ArraySetStmt(beaver.Symbol p0, Opt<RuntimeCheck> p1, beaver.Symbol p2, beaver.Symbol p3, beaver.Symbol p4) {
    setIndent(p0);
    setChild(p1, 0);
    setlhsVariable(p2);
    setlhsIndex(p3);
    setrhsVariable(p4);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the lexeme Indent.
   * @param value The new value for the lexeme Indent.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIndent(String value) {
    tokenString_Indent = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Indent;
  /**
   * @ast method 
   * 
   */
  
  public int Indentstart;
  /**
   * @ast method 
   * 
   */
  
  public int Indentend;
  /**
   * JastAdd-internal setter for lexeme Indent using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setIndent(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setIndent is only valid for String lexemes");
    tokenString_Indent = (String)symbol.value;
    Indentstart = symbol.getStart();
    Indentend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Indent.
   * @return The value for the lexeme Indent.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getIndent() {
    return tokenString_Indent != null ? tokenString_Indent : "";
  }
  /**
   * Replaces the optional node for the RuntimeCheck child. This is the {@code Opt} node containing the child RuntimeCheck, not the actual child!
   * @param opt The new node to be used as the optional node for the RuntimeCheck child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setRuntimeCheckOpt(Opt<RuntimeCheck> opt) {
    setChild(opt, 0);
  }
  /**
   * Check whether the optional RuntimeCheck child exists.
   * @return {@code true} if the optional RuntimeCheck child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasRuntimeCheck() {
    return getRuntimeCheckOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) RuntimeCheck child.
   * @return The RuntimeCheck child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public RuntimeCheck getRuntimeCheck() {
    return (RuntimeCheck)getRuntimeCheckOpt().getChild(0);
  }
  /**
   * Replaces the (optional) RuntimeCheck child.
   * @param node The new node to be used as the RuntimeCheck child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setRuntimeCheck(RuntimeCheck node) {
    getRuntimeCheckOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the RuntimeCheck child. This is the {@code Opt} node containing the child RuntimeCheck, not the actual child!
   * @return The optional node for child the RuntimeCheck child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<RuntimeCheck> getRuntimeCheckOpt() {
    return (Opt<RuntimeCheck>)getChild(0);
  }
  /**
   * Retrieves the optional node for child RuntimeCheck. This is the {@code Opt} node containing the child RuntimeCheck, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child RuntimeCheck.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<RuntimeCheck> getRuntimeCheckOptNoTransform() {
    return (Opt<RuntimeCheck>)getChildNoTransform(0);
  }
  /**
   * Replaces the lexeme lhsVariable.
   * @param value The new value for the lexeme lhsVariable.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setlhsVariable(String value) {
    tokenString_lhsVariable = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_lhsVariable;
  /**
   * @ast method 
   * 
   */
  
  public int lhsVariablestart;
  /**
   * @ast method 
   * 
   */
  
  public int lhsVariableend;
  /**
   * JastAdd-internal setter for lexeme lhsVariable using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setlhsVariable(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setlhsVariable is only valid for String lexemes");
    tokenString_lhsVariable = (String)symbol.value;
    lhsVariablestart = symbol.getStart();
    lhsVariableend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme lhsVariable.
   * @return The value for the lexeme lhsVariable.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getlhsVariable() {
    return tokenString_lhsVariable != null ? tokenString_lhsVariable : "";
  }
  /**
   * Replaces the lexeme lhsIndex.
   * @param value The new value for the lexeme lhsIndex.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setlhsIndex(String value) {
    tokenString_lhsIndex = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_lhsIndex;
  /**
   * @ast method 
   * 
   */
  
  public int lhsIndexstart;
  /**
   * @ast method 
   * 
   */
  
  public int lhsIndexend;
  /**
   * JastAdd-internal setter for lexeme lhsIndex using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setlhsIndex(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setlhsIndex is only valid for String lexemes");
    tokenString_lhsIndex = (String)symbol.value;
    lhsIndexstart = symbol.getStart();
    lhsIndexend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme lhsIndex.
   * @return The value for the lexeme lhsIndex.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getlhsIndex() {
    return tokenString_lhsIndex != null ? tokenString_lhsIndex : "";
  }
  /**
   * Replaces the lexeme rhsVariable.
   * @param value The new value for the lexeme rhsVariable.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setrhsVariable(String value) {
    tokenString_rhsVariable = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_rhsVariable;
  /**
   * @ast method 
   * 
   */
  
  public int rhsVariablestart;
  /**
   * @ast method 
   * 
   */
  
  public int rhsVariableend;
  /**
   * JastAdd-internal setter for lexeme rhsVariable using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setrhsVariable(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setrhsVariable is only valid for String lexemes");
    tokenString_rhsVariable = (String)symbol.value;
    rhsVariablestart = symbol.getStart();
    rhsVariableend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme rhsVariable.
   * @return The value for the lexeme rhsVariable.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getrhsVariable() {
    return tokenString_rhsVariable != null ? tokenString_rhsVariable : "";
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
