/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.Fortran.codegen.FortranAST;

/**
 * @production Program : {@link ASTNode} ::= <span class="component">{@link SubProgram}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/FortranIR.ast:1
 */
public class Program extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Program clone() throws CloneNotSupportedException {
    Program node = (Program)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Program copy() {
      try {
        Program node = (Program)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Program fullCopy() {
    try {
      Program tree = (Program) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/PrettyPrinter.jadd:4
   */
  public void pp(StringBuffer sb) {}
  /**
   * @ast method 
   * 
   */
  public Program() {
    super();

    setChild(new List(), 0);
    is$Final(true);

  }
  /**
   * @ast method 
   * 
   */
  public Program(List<SubProgram> p0) {
    setChild(p0, 0);
    is$Final(true);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the SubProgram list.
   * @param list The new list node to be used as the SubProgram list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setSubProgramList(List<SubProgram> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the SubProgram list.
   * @return Number of children in the SubProgram list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumSubProgram() {
    return getSubProgramList().getNumChild();
  }
  /**
   * Retrieves the number of children in the SubProgram list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the SubProgram list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumSubProgramNoTransform() {
    return getSubProgramListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the SubProgram list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the SubProgram list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public SubProgram getSubProgram(int i) {
    return (SubProgram)getSubProgramList().getChild(i);
  }
  /**
   * Append an element to the SubProgram list.
   * @param node The element to append to the SubProgram list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addSubProgram(SubProgram node) {
    List<SubProgram> list = (parent == null || state == null) ? getSubProgramListNoTransform() : getSubProgramList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addSubProgramNoTransform(SubProgram node) {
    List<SubProgram> list = getSubProgramListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the SubProgram list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setSubProgram(SubProgram node, int i) {
    List<SubProgram> list = getSubProgramList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the SubProgram list.
   * @return The node representing the SubProgram list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<SubProgram> getSubPrograms() {
    return getSubProgramList();
  }
  /**
   * Retrieves the SubProgram list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SubProgram list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<SubProgram> getSubProgramsNoTransform() {
    return getSubProgramListNoTransform();
  }
  /**
   * Retrieves the SubProgram list.
   * @return The node representing the SubProgram list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<SubProgram> getSubProgramList() {
    List<SubProgram> list = (List<SubProgram>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the SubProgram list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SubProgram list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<SubProgram> getSubProgramListNoTransform() {
    return (List<SubProgram>)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
