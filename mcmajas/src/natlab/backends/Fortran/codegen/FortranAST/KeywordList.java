/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.Fortran.codegen.FortranAST;

/**
 * @production KeywordList : {@link ASTNode} ::= <span class="component">{@link Keyword}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/FortranIR.ast:11
 */
public class KeywordList extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public KeywordList clone() throws CloneNotSupportedException {
    KeywordList node = (KeywordList)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public KeywordList copy() {
      try {
        KeywordList node = (KeywordList)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public KeywordList fullCopy() {
    try {
      KeywordList tree = (KeywordList) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/Fortran/codegen/PrettyPrinter.jadd:67
   */
  public void pp(StringBuffer sb) {
    	int size = getNumKeyword();
        for(int i=0;i<size;i++) {
        	getKeyword(i).pp(sb);
        	if(i<size-1) {
        		sb.append(" , ");
        	}
        }
    }
  /**
   * @ast method 
   * 
   */
  public KeywordList() {
    super();

    setChild(new List(), 0);

  }
  /**
   * @ast method 
   * 
   */
  public KeywordList(List<Keyword> p0) {
    setChild(p0, 0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Keyword list.
   * @param list The new list node to be used as the Keyword list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setKeywordList(List<Keyword> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Keyword list.
   * @return Number of children in the Keyword list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumKeyword() {
    return getKeywordList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Keyword list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Keyword list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumKeywordNoTransform() {
    return getKeywordListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Keyword list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Keyword list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Keyword getKeyword(int i) {
    return (Keyword)getKeywordList().getChild(i);
  }
  /**
   * Append an element to the Keyword list.
   * @param node The element to append to the Keyword list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addKeyword(Keyword node) {
    List<Keyword> list = (parent == null || state == null) ? getKeywordListNoTransform() : getKeywordList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addKeywordNoTransform(Keyword node) {
    List<Keyword> list = getKeywordListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Keyword list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setKeyword(Keyword node, int i) {
    List<Keyword> list = getKeywordList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Keyword list.
   * @return The node representing the Keyword list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Keyword> getKeywords() {
    return getKeywordList();
  }
  /**
   * Retrieves the Keyword list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Keyword list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Keyword> getKeywordsNoTransform() {
    return getKeywordListNoTransform();
  }
  /**
   * Retrieves the Keyword list.
   * @return The node representing the Keyword list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Keyword> getKeywordList() {
    List<Keyword> list = (List<Keyword>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Keyword list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Keyword list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Keyword> getKeywordListNoTransform() {
    return (List<Keyword>)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
