/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production AssignStmt : {@link ExpStmt} ::= <span class="component">[{@link MultiAssignLHS}]</span> <span class="component">LHS:{@link IDInfo}</span> <span class="component">RHS:{@link Exp}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:12
 */
public class AssignStmt extends ExpStmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public AssignStmt clone() throws CloneNotSupportedException {
    AssignStmt node = (AssignStmt)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public AssignStmt copy() {
      try {
        AssignStmt node = (AssignStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public AssignStmt fullCopy() {
    try {
      AssignStmt tree = (AssignStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/pretty.jadd:157
   */
  String pp(String indent){
	StringBuffer x = new StringBuffer();
	if (null != getLHS()) {
		/**
		 * It is not a list assignment
		 */

		if (null == getLHS().getShape()) {
			/* Scalar variable */
			x.append(getLHS().getName());
			x.append(" = ");
			x.append(getRHS().pp("") + ";\n");

		}

		else if (null != getLHS().getShape()) {
			boolean tf = true;
			for (int i = 0; i < getLHS().getShape().size(); i++) {
				if(null != getLHS().getShape().get(i))
				tf &= ("1").equals(getLHS().getShape().get(i).toString());
			}
			if (tf) {

				/* Scalar variable */
				x.append(getLHS().getName());
				x.append(" = ");
				x.append(getRHS().pp("") + ";\n");

			}

		}
		else //array
		{
			/**TODO
			 * Make sure if arrays can be aSSIGNED DIRECTLY LIKE VARIABLES. wHAT ARE THE RESTRICTIONS ?
			 */
			x.append(getLHS().getName());
			x.append(" = ");
			x.append(getRHS().pp("") + ";\n");
//			
//			x.append("val "+getLHS().getName()+" = "+
//			"new Array["+getLHS().getType().getName()+"]"+
//			"("+PPHelper.makeRange(getLHS().getShape())+
//			", "+getRHS().pp("")+");\n");
		}

	}
	else
	{
		
		System.out.println("list assign:"+getRHS().pp(indent));
	  /**TODO
	   * Case when assigned to a list
	   */
		if(! hasMultiAssignLHS() || 0==getMultiAssignLHS().getIDInfoList().getNumChild()){ //|| getMultiAssignLHS().getNumChild() ==0
			x.append(getRHS().pp(indent)+" ;\n");
		}
		else {
			x.append("val "+"generate_a_name"+" = "+
		"new Array[Any]"+"(0.."+(getMultiAssignLHS().getIDInfoList().getNumChild()-1)+", "+getRHS().pp("")+");\n");
		}
		
	}
	return x.toString();
}
  /**
   * @ast method 
   * 
   */
  public AssignStmt() {
    super();

    setChild(new Opt(), 1);

  }
  /**
   * @ast method 
   * 
   */
  public AssignStmt(Exp p0, Opt<MultiAssignLHS> p1, IDInfo p2, Exp p3) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
    setChild(p3, 3);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 4;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Exp child.
   * @param node The new node to replace the Exp child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setExp(Exp node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Exp child.
   * @return The current node used as the Exp child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Exp getExp() {
    return (Exp)getChild(0);
  }
  /**
   * Retrieves the Exp child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Exp child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Exp getExpNoTransform() {
    return (Exp)getChildNoTransform(0);
  }
  /**
   * Replaces the optional node for the MultiAssignLHS child. This is the {@code Opt} node containing the child MultiAssignLHS, not the actual child!
   * @param opt The new node to be used as the optional node for the MultiAssignLHS child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setMultiAssignLHSOpt(Opt<MultiAssignLHS> opt) {
    setChild(opt, 1);
  }
  /**
   * Check whether the optional MultiAssignLHS child exists.
   * @return {@code true} if the optional MultiAssignLHS child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasMultiAssignLHS() {
    return getMultiAssignLHSOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) MultiAssignLHS child.
   * @return The MultiAssignLHS child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public MultiAssignLHS getMultiAssignLHS() {
    return (MultiAssignLHS)getMultiAssignLHSOpt().getChild(0);
  }
  /**
   * Replaces the (optional) MultiAssignLHS child.
   * @param node The new node to be used as the MultiAssignLHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setMultiAssignLHS(MultiAssignLHS node) {
    getMultiAssignLHSOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the MultiAssignLHS child. This is the {@code Opt} node containing the child MultiAssignLHS, not the actual child!
   * @return The optional node for child the MultiAssignLHS child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<MultiAssignLHS> getMultiAssignLHSOpt() {
    return (Opt<MultiAssignLHS>)getChild(1);
  }
  /**
   * Retrieves the optional node for child MultiAssignLHS. This is the {@code Opt} node containing the child MultiAssignLHS, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child MultiAssignLHS.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<MultiAssignLHS> getMultiAssignLHSOptNoTransform() {
    return (Opt<MultiAssignLHS>)getChildNoTransform(1);
  }
  /**
   * Replaces the LHS child.
   * @param node The new node to replace the LHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setLHS(IDInfo node) {
    setChild(node, 2);
  }
  /**
   * Retrieves the LHS child.
   * @return The current node used as the LHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public IDInfo getLHS() {
    return (IDInfo)getChild(2);
  }
  /**
   * Retrieves the LHS child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the LHS child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public IDInfo getLHSNoTransform() {
    return (IDInfo)getChildNoTransform(2);
  }
  /**
   * Replaces the RHS child.
   * @param node The new node to replace the RHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setRHS(Exp node) {
    setChild(node, 3);
  }
  /**
   * Retrieves the RHS child.
   * @return The current node used as the RHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Exp getRHS() {
    return (Exp)getChild(3);
  }
  /**
   * Retrieves the RHS child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the RHS child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Exp getRHSNoTransform() {
    return (Exp)getChildNoTransform(3);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
