/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production Method : {@link ASTNode} ::= <span class="component">{@link MethodHeader}</span> <span class="component">{@link MethodBlock}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:7
 */
public class Method extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Method clone() throws CloneNotSupportedException {
    Method node = (Method)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Method copy() {
      try {
        Method node = (Method)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Method fullCopy() {
    try {
      Method tree = (Method) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/pretty.jadd:114
   */
  String pp(String indent)
{
	  StringBuffer x = new StringBuffer();
	  x.append(indent+ getMethodHeader().pp("")+"\n"+getMethodBlock().pp(indent+"    "));
	  
	  return x.toString();
}
  /**
   * @ast method 
   * 
   */
  public Method() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public Method(MethodHeader p0, MethodBlock p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the MethodHeader child.
   * @param node The new node to replace the MethodHeader child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setMethodHeader(MethodHeader node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the MethodHeader child.
   * @return The current node used as the MethodHeader child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public MethodHeader getMethodHeader() {
    return (MethodHeader)getChild(0);
  }
  /**
   * Retrieves the MethodHeader child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the MethodHeader child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public MethodHeader getMethodHeaderNoTransform() {
    return (MethodHeader)getChildNoTransform(0);
  }
  /**
   * Replaces the MethodBlock child.
   * @param node The new node to replace the MethodBlock child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setMethodBlock(MethodBlock node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the MethodBlock child.
   * @return The current node used as the MethodBlock child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public MethodBlock getMethodBlock() {
    return (MethodBlock)getChild(1);
  }
  /**
   * Retrieves the MethodBlock child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the MethodBlock child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public MethodBlock getMethodBlockNoTransform() {
    return (MethodBlock)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
