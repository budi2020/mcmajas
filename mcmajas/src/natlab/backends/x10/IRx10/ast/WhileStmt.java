/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production WhileStmt : {@link Stmt} ::= <span class="component">Condition:{@link Exp}</span> <span class="component">{@link LoopBody}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:77
 */
public class WhileStmt extends Stmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public WhileStmt clone() throws CloneNotSupportedException {
    WhileStmt node = (WhileStmt)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public WhileStmt copy() {
      try {
        WhileStmt node = (WhileStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public WhileStmt fullCopy() {
    try {
      WhileStmt tree = (WhileStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/pretty.jadd:337
   */
  String pp(String indent)
{
	  StringBuffer x = new StringBuffer();
	  x.append(indent);
	  x.append("while ("+getCondition().pp("")+")\n"+getLoopBody().pp(indent+"   "));
	  return x.toString();
}
  /**
   * @ast method 
   * 
   */
  public WhileStmt() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public WhileStmt(Exp p0, LoopBody p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Condition child.
   * @param node The new node to replace the Condition child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setCondition(Exp node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Condition child.
   * @return The current node used as the Condition child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Exp getCondition() {
    return (Exp)getChild(0);
  }
  /**
   * Retrieves the Condition child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Condition child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Exp getConditionNoTransform() {
    return (Exp)getChildNoTransform(0);
  }
  /**
   * Replaces the LoopBody child.
   * @param node The new node to replace the LoopBody child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setLoopBody(LoopBody node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the LoopBody child.
   * @return The current node used as the LoopBody child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public LoopBody getLoopBody() {
    return (LoopBody)getChild(1);
  }
  /**
   * Retrieves the LoopBody child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the LoopBody child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public LoopBody getLoopBodyNoTransform() {
    return (LoopBody)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
