/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production CharLiteral : {@link Literal};
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:38
 */
public class CharLiteral extends Literal implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public CharLiteral clone() throws CloneNotSupportedException {
    CharLiteral node = (CharLiteral)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public CharLiteral copy() {
      try {
        CharLiteral node = (CharLiteral)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public CharLiteral fullCopy() {
    try {
      CharLiteral tree = (CharLiteral) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * 
   */
  public CharLiteral() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public CharLiteral(String p0) {
    setLiteral(p0);
  }
  /**
   * @ast method 
   * 
   */
  public CharLiteral(beaver.Symbol p0) {
    setLiteral(p0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the lexeme Literal.
   * @param value The new value for the lexeme Literal.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setLiteral(String value) {
    tokenString_Literal = value;
  }
  /**
   * JastAdd-internal setter for lexeme Literal using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setLiteral(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setLiteral is only valid for String lexemes");
    tokenString_Literal = (String)symbol.value;
    Literalstart = symbol.getStart();
    Literalend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Literal.
   * @return The value for the lexeme Literal.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getLiteral() {
    return tokenString_Literal != null ? tokenString_Literal : "";
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
