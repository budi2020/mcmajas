/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production ClassBlock : {@link ASTNode} ::= <span class="component">DeclStmt:{@link Stmt}*</span> <span class="component">{@link Method}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:3
 */
public class ClassBlock extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ClassBlock clone() throws CloneNotSupportedException {
    ClassBlock node = (ClassBlock)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ClassBlock copy() {
      try {
        ClassBlock node = (ClassBlock)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ClassBlock fullCopy() {
    try {
      ClassBlock tree = (ClassBlock) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/pretty.jadd:14
   */
  String pp(String indent,String className){
	StringBuffer x = new StringBuffer();
	x.append("class "+className+"{\n");
	for(Stmt decl_stmt : getDeclStmtList()){
		x.append(decl_stmt.pp(indent+"	"));
	}
	for (Method method : getMethodList()){
		x.append(method.pp(indent+"	"));
	x.append("\n}");

	}
	return x.toString();
}
  /**
   * @ast method 
   * 
   */
  public ClassBlock() {
    super();

    setChild(new List(), 0);
    setChild(new List(), 1);

  }
  /**
   * @ast method 
   * 
   */
  public ClassBlock(List<Stmt> p0, List<Method> p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the DeclStmt list.
   * @param list The new list node to be used as the DeclStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setDeclStmtList(List<Stmt> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the DeclStmt list.
   * @return Number of children in the DeclStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumDeclStmt() {
    return getDeclStmtList().getNumChild();
  }
  /**
   * Retrieves the number of children in the DeclStmt list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the DeclStmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumDeclStmtNoTransform() {
    return getDeclStmtListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the DeclStmt list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the DeclStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Stmt getDeclStmt(int i) {
    return (Stmt)getDeclStmtList().getChild(i);
  }
  /**
   * Append an element to the DeclStmt list.
   * @param node The element to append to the DeclStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addDeclStmt(Stmt node) {
    List<Stmt> list = (parent == null || state == null) ? getDeclStmtListNoTransform() : getDeclStmtList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addDeclStmtNoTransform(Stmt node) {
    List<Stmt> list = getDeclStmtListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the DeclStmt list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setDeclStmt(Stmt node, int i) {
    List<Stmt> list = getDeclStmtList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the DeclStmt list.
   * @return The node representing the DeclStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Stmt> getDeclStmts() {
    return getDeclStmtList();
  }
  /**
   * Retrieves the DeclStmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the DeclStmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Stmt> getDeclStmtsNoTransform() {
    return getDeclStmtListNoTransform();
  }
  /**
   * Retrieves the DeclStmt list.
   * @return The node representing the DeclStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Stmt> getDeclStmtList() {
    List<Stmt> list = (List<Stmt>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the DeclStmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the DeclStmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Stmt> getDeclStmtListNoTransform() {
    return (List<Stmt>)getChildNoTransform(0);
  }
  /**
   * Replaces the Method list.
   * @param list The new list node to be used as the Method list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setMethodList(List<Method> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Method list.
   * @return Number of children in the Method list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumMethod() {
    return getMethodList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Method list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Method list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumMethodNoTransform() {
    return getMethodListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Method list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Method list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Method getMethod(int i) {
    return (Method)getMethodList().getChild(i);
  }
  /**
   * Append an element to the Method list.
   * @param node The element to append to the Method list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addMethod(Method node) {
    List<Method> list = (parent == null || state == null) ? getMethodListNoTransform() : getMethodList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addMethodNoTransform(Method node) {
    List<Method> list = getMethodListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Method list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setMethod(Method node, int i) {
    List<Method> list = getMethodList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Method list.
   * @return The node representing the Method list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Method> getMethods() {
    return getMethodList();
  }
  /**
   * Retrieves the Method list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Method list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Method> getMethodsNoTransform() {
    return getMethodListNoTransform();
  }
  /**
   * Retrieves the Method list.
   * @return The node representing the Method list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Method> getMethodList() {
    List<Method> list = (List<Method>)getChild(1);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Method list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Method list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Method> getMethodListNoTransform() {
    return (List<Method>)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
