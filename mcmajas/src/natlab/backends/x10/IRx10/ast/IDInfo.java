/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production IDInfo : {@link ASTNode} ::= <span class="component">{@link Type}</span> <span class="component">&lt;Name:String&gt;</span> <span class="component">&lt;Shape:ArrayList&gt;</span> <span class="component">&lt;isComplex:String&gt;</span> <span class="component">Value:{@link Exp}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:16
 */
public class IDInfo extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IDInfo clone() throws CloneNotSupportedException {
    IDInfo node = (IDInfo)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IDInfo copy() {
      try {
        IDInfo node = (IDInfo)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IDInfo fullCopy() {
    try {
      IDInfo tree = (IDInfo) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * 
   */
  public IDInfo() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public IDInfo(Type p0, String p1, ArrayList p2, String p3, Exp p4) {
    setChild(p0, 0);
    setName(p1);
    setShape(p2);
    setisComplex(p3);
    setChild(p4, 1);
  }
  /**
   * @ast method 
   * 
   */
  public IDInfo(Type p0, beaver.Symbol p1, ArrayList p2, beaver.Symbol p3, Exp p4) {
    setChild(p0, 0);
    setName(p1);
    setShape(p2);
    setisComplex(p3);
    setChild(p4, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Type child.
   * @param node The new node to replace the Type child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setType(Type node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Type child.
   * @return The current node used as the Type child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Type getType() {
    return (Type)getChild(0);
  }
  /**
   * Retrieves the Type child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Type child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Type getTypeNoTransform() {
    return (Type)getChildNoTransform(0);
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setName(String value) {
    tokenString_Name = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Name;
  /**
   * @ast method 
   * 
   */
  
  public int Namestart;
  /**
   * @ast method 
   * 
   */
  
  public int Nameend;
  /**
   * JastAdd-internal setter for lexeme Name using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setName(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setName is only valid for String lexemes");
    tokenString_Name = (String)symbol.value;
    Namestart = symbol.getStart();
    Nameend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
  /**
   * Replaces the lexeme Shape.
   * @param value The new value for the lexeme Shape.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setShape(ArrayList value) {
    tokenArrayList_Shape = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected ArrayList tokenArrayList_Shape;
  /**
   * Retrieves the value for the lexeme Shape.
   * @return The value for the lexeme Shape.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public ArrayList getShape() {
    return tokenArrayList_Shape;
  }
  /**
   * Replaces the lexeme isComplex.
   * @param value The new value for the lexeme isComplex.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setisComplex(String value) {
    tokenString_isComplex = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_isComplex;
  /**
   * @ast method 
   * 
   */
  
  public int isComplexstart;
  /**
   * @ast method 
   * 
   */
  
  public int isComplexend;
  /**
   * JastAdd-internal setter for lexeme isComplex using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setisComplex(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setisComplex is only valid for String lexemes");
    tokenString_isComplex = (String)symbol.value;
    isComplexstart = symbol.getStart();
    isComplexend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme isComplex.
   * @return The value for the lexeme isComplex.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getisComplex() {
    return tokenString_isComplex != null ? tokenString_isComplex : "";
  }
  /**
   * Replaces the Value child.
   * @param node The new node to replace the Value child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setValue(Exp node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the Value child.
   * @return The current node used as the Value child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Exp getValue() {
    return (Exp)getChild(1);
  }
  /**
   * Retrieves the Value child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Value child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Exp getValueNoTransform() {
    return (Exp)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
