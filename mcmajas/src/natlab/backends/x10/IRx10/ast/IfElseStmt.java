/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production IfElseStmt : {@link Stmt} ::= <span class="component">{@link IfElseIf}*</span> <span class="component">[{@link ElseBody}]</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:80
 */
public class IfElseStmt extends Stmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IfElseStmt clone() throws CloneNotSupportedException {
    IfElseStmt node = (IfElseStmt)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IfElseStmt copy() {
      try {
        IfElseStmt node = (IfElseStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IfElseStmt fullCopy() {
    try {
      IfElseStmt tree = (IfElseStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/pretty.jadd:345
   */
  String pp(String indent)
{   
	
	StringBuffer x = new StringBuffer();
	x.append(indent);
	  x.append("inside if else ");
	x.append("if ("+getIfElseIfList().getChild(0).getCondition().pp("")+")\n");
	x.append(getIfElseIfList().getChild(0).getIfBody().pp(indent+"    "));
	for (int i=1; i<getIfElseIfList().getNumChild() ; i++){
		x.append("else if ("+getIfElseIfList().getChild(i).getCondition().pp("")+")\n");
	  	x.append(getIfElseIfList().getChild(i).getIfBody().pp(indent+"    "));
	}
	if(null != getElseBody()){
		x.append("else \n"+getElseBody().pp(indent+"    "));
	}
	return x.toString();
}
  /**
   * @ast method 
   * 
   */
  public IfElseStmt() {
    super();

    setChild(new List(), 0);
    setChild(new Opt(), 1);

  }
  /**
   * @ast method 
   * 
   */
  public IfElseStmt(List<IfElseIf> p0, Opt<ElseBody> p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the IfElseIf list.
   * @param list The new list node to be used as the IfElseIf list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIfElseIfList(List<IfElseIf> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the IfElseIf list.
   * @return Number of children in the IfElseIf list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumIfElseIf() {
    return getIfElseIfList().getNumChild();
  }
  /**
   * Retrieves the number of children in the IfElseIf list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the IfElseIf list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumIfElseIfNoTransform() {
    return getIfElseIfListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the IfElseIf list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the IfElseIf list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IfElseIf getIfElseIf(int i) {
    return (IfElseIf)getIfElseIfList().getChild(i);
  }
  /**
   * Append an element to the IfElseIf list.
   * @param node The element to append to the IfElseIf list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addIfElseIf(IfElseIf node) {
    List<IfElseIf> list = (parent == null || state == null) ? getIfElseIfListNoTransform() : getIfElseIfList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addIfElseIfNoTransform(IfElseIf node) {
    List<IfElseIf> list = getIfElseIfListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the IfElseIf list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIfElseIf(IfElseIf node, int i) {
    List<IfElseIf> list = getIfElseIfList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the IfElseIf list.
   * @return The node representing the IfElseIf list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<IfElseIf> getIfElseIfs() {
    return getIfElseIfList();
  }
  /**
   * Retrieves the IfElseIf list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the IfElseIf list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<IfElseIf> getIfElseIfsNoTransform() {
    return getIfElseIfListNoTransform();
  }
  /**
   * Retrieves the IfElseIf list.
   * @return The node representing the IfElseIf list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<IfElseIf> getIfElseIfList() {
    List<IfElseIf> list = (List<IfElseIf>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the IfElseIf list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the IfElseIf list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<IfElseIf> getIfElseIfListNoTransform() {
    return (List<IfElseIf>)getChildNoTransform(0);
  }
  /**
   * Replaces the optional node for the ElseBody child. This is the {@code Opt} node containing the child ElseBody, not the actual child!
   * @param opt The new node to be used as the optional node for the ElseBody child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setElseBodyOpt(Opt<ElseBody> opt) {
    setChild(opt, 1);
  }
  /**
   * Check whether the optional ElseBody child exists.
   * @return {@code true} if the optional ElseBody child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasElseBody() {
    return getElseBodyOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) ElseBody child.
   * @return The ElseBody child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ElseBody getElseBody() {
    return (ElseBody)getElseBodyOpt().getChild(0);
  }
  /**
   * Replaces the (optional) ElseBody child.
   * @param node The new node to be used as the ElseBody child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setElseBody(ElseBody node) {
    getElseBodyOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the ElseBody child. This is the {@code Opt} node containing the child ElseBody, not the actual child!
   * @return The optional node for child the ElseBody child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<ElseBody> getElseBodyOpt() {
    return (Opt<ElseBody>)getChild(1);
  }
  /**
   * Retrieves the optional node for child ElseBody. This is the {@code Opt} node containing the child ElseBody, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child ElseBody.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<ElseBody> getElseBodyOptNoTransform() {
    return (Opt<ElseBody>)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
