/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production BuiltinMethodCall : {@link MethodCall} ::= <span class="component">BuiltinMethodName:{@link MethodId}</span> <span class="component">Argument:{@link Exp}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:88
 */
public class BuiltinMethodCall extends MethodCall implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public BuiltinMethodCall clone() throws CloneNotSupportedException {
    BuiltinMethodCall node = (BuiltinMethodCall)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public BuiltinMethodCall copy() {
      try {
        BuiltinMethodCall node = (BuiltinMethodCall)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public BuiltinMethodCall fullCopy() {
    try {
      BuiltinMethodCall tree = (BuiltinMethodCall) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/pretty.jadd:363
   */
  String pp(String indent)
{
	  StringBuffer x = new StringBuffer();
	  x.append(indent);
	  x.append(getBuiltinMethodName().getName()+"(");
	  x.append(getArgumentList().getChild(0).pp(""));
	  for(int i=1; i<getArgumentList().getNumChild() ; i++)
	  {
		  x.append(", "+getArgumentList().getChild(i).pp(""));
	  }
	  x.append(")");
	  return x.toString();
}
  /**
   * @ast method 
   * 
   */
  public BuiltinMethodCall() {
    super();

    setChild(new List(), 1);

  }
  /**
   * @ast method 
   * 
   */
  public BuiltinMethodCall(MethodId p0, List<Exp> p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the BuiltinMethodName child.
   * @param node The new node to replace the BuiltinMethodName child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setBuiltinMethodName(MethodId node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the BuiltinMethodName child.
   * @return The current node used as the BuiltinMethodName child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public MethodId getBuiltinMethodName() {
    return (MethodId)getChild(0);
  }
  /**
   * Retrieves the BuiltinMethodName child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the BuiltinMethodName child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public MethodId getBuiltinMethodNameNoTransform() {
    return (MethodId)getChildNoTransform(0);
  }
  /**
   * Replaces the Argument list.
   * @param list The new list node to be used as the Argument list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setArgumentList(List<Exp> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Argument list.
   * @return Number of children in the Argument list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumArgument() {
    return getArgumentList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Argument list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Argument list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumArgumentNoTransform() {
    return getArgumentListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Argument list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Argument list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Exp getArgument(int i) {
    return (Exp)getArgumentList().getChild(i);
  }
  /**
   * Append an element to the Argument list.
   * @param node The element to append to the Argument list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addArgument(Exp node) {
    List<Exp> list = (parent == null || state == null) ? getArgumentListNoTransform() : getArgumentList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addArgumentNoTransform(Exp node) {
    List<Exp> list = getArgumentListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Argument list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setArgument(Exp node, int i) {
    List<Exp> list = getArgumentList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Argument list.
   * @return The node representing the Argument list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Exp> getArguments() {
    return getArgumentList();
  }
  /**
   * Retrieves the Argument list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Argument list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Exp> getArgumentsNoTransform() {
    return getArgumentListNoTransform();
  }
  /**
   * Retrieves the Argument list.
   * @return The node representing the Argument list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Exp> getArgumentList() {
    List<Exp> list = (List<Exp>)getChild(1);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Argument list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Argument list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Exp> getArgumentListNoTransform() {
    return (List<Exp>)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
