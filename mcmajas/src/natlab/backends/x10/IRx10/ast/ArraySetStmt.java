/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production ArraySetStmt : {@link Stmt} ::= <span class="component">LHS:{@link IDInfo}</span> <span class="component">RHS:{@link Exp}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:28
 */
public class ArraySetStmt extends Stmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArraySetStmt clone() throws CloneNotSupportedException {
    ArraySetStmt node = (ArraySetStmt)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArraySetStmt copy() {
      try {
        ArraySetStmt node = (ArraySetStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArraySetStmt fullCopy() {
    try {
      ArraySetStmt tree = (ArraySetStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/pretty.jadd:77
   */
  String pp(String indent){
		StringBuffer x = new StringBuffer();
		
						
			x.append(getLHS().getValue().pp(indent));
			x.append(" = ");
			x.append(getRHS().pp("")+" ;\n");
		return x.toString();
	}
  /**
   * @ast method 
   * 
   */
  public ArraySetStmt() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public ArraySetStmt(IDInfo p0, Exp p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the LHS child.
   * @param node The new node to replace the LHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setLHS(IDInfo node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the LHS child.
   * @return The current node used as the LHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public IDInfo getLHS() {
    return (IDInfo)getChild(0);
  }
  /**
   * Retrieves the LHS child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the LHS child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public IDInfo getLHSNoTransform() {
    return (IDInfo)getChildNoTransform(0);
  }
  /**
   * Replaces the RHS child.
   * @param node The new node to replace the RHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setRHS(Exp node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the RHS child.
   * @return The current node used as the RHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Exp getRHS() {
    return (Exp)getChild(1);
  }
  /**
   * Retrieves the RHS child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the RHS child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Exp getRHSNoTransform() {
    return (Exp)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
