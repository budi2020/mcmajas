/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production MultiAssignLHS : {@link Exp} ::= <span class="component">{@link IDInfo}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:18
 */
public class MultiAssignLHS extends Exp implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public MultiAssignLHS clone() throws CloneNotSupportedException {
    MultiAssignLHS node = (MultiAssignLHS)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public MultiAssignLHS copy() {
      try {
        MultiAssignLHS node = (MultiAssignLHS)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public MultiAssignLHS fullCopy() {
    try {
      MultiAssignLHS tree = (MultiAssignLHS) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * 
   */
  public MultiAssignLHS() {
    super();

    setChild(new List(), 0);

  }
  /**
   * @ast method 
   * 
   */
  public MultiAssignLHS(List<IDInfo> p0) {
    setChild(p0, 0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the IDInfo list.
   * @param list The new list node to be used as the IDInfo list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIDInfoList(List<IDInfo> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the IDInfo list.
   * @return Number of children in the IDInfo list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumIDInfo() {
    return getIDInfoList().getNumChild();
  }
  /**
   * Retrieves the number of children in the IDInfo list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the IDInfo list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumIDInfoNoTransform() {
    return getIDInfoListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the IDInfo list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the IDInfo list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IDInfo getIDInfo(int i) {
    return (IDInfo)getIDInfoList().getChild(i);
  }
  /**
   * Append an element to the IDInfo list.
   * @param node The element to append to the IDInfo list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addIDInfo(IDInfo node) {
    List<IDInfo> list = (parent == null || state == null) ? getIDInfoListNoTransform() : getIDInfoList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addIDInfoNoTransform(IDInfo node) {
    List<IDInfo> list = getIDInfoListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the IDInfo list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIDInfo(IDInfo node, int i) {
    List<IDInfo> list = getIDInfoList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the IDInfo list.
   * @return The node representing the IDInfo list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<IDInfo> getIDInfos() {
    return getIDInfoList();
  }
  /**
   * Retrieves the IDInfo list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the IDInfo list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<IDInfo> getIDInfosNoTransform() {
    return getIDInfoListNoTransform();
  }
  /**
   * Retrieves the IDInfo list.
   * @return The node representing the IDInfo list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<IDInfo> getIDInfoList() {
    List<IDInfo> list = (List<IDInfo>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the IDInfo list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the IDInfo list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<IDInfo> getIDInfoListNoTransform() {
    return (List<IDInfo>)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
