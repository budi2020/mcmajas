/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production IncExp : {@link AdditiveExp};
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:51
 */
public class IncExp extends AdditiveExp implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IncExp clone() throws CloneNotSupportedException {
    IncExp node = (IncExp)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IncExp copy() {
      try {
        IncExp node = (IncExp)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IncExp fullCopy() {
    try {
      IncExp tree = (IncExp) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/pretty.jadd:285
   */
  String pp(String indent)
{
	return(indent+"("+getLeftOp().pp("")+" + "+getRightOp().pp("")+")");
}
  /**
   * @ast method 
   * 
   */
  public IncExp() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public IncExp(Exp p0, Exp p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the LeftOp child.
   * @param node The new node to replace the LeftOp child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setLeftOp(Exp node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the LeftOp child.
   * @return The current node used as the LeftOp child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Exp getLeftOp() {
    return (Exp)getChild(0);
  }
  /**
   * Retrieves the LeftOp child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the LeftOp child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Exp getLeftOpNoTransform() {
    return (Exp)getChildNoTransform(0);
  }
  /**
   * Replaces the RightOp child.
   * @param node The new node to replace the RightOp child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setRightOp(Exp node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the RightOp child.
   * @return The current node used as the RightOp child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Exp getRightOp() {
    return (Exp)getChild(1);
  }
  /**
   * Retrieves the RightOp child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the RightOp child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Exp getRightOpNoTransform() {
    return (Exp)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
