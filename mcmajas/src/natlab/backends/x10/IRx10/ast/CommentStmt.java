/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production CommentStmt : {@link Stmt} ::= <span class="component">&lt;Comment:String&gt;</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:5
 */
public class CommentStmt extends Stmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public CommentStmt clone() throws CloneNotSupportedException {
    CommentStmt node = (CommentStmt)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public CommentStmt copy() {
      try {
        CommentStmt node = (CommentStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public CommentStmt fullCopy() {
    try {
      CommentStmt tree = (CommentStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/pretty.jadd:392
   */
  String pp(String indent)
{
	StringBuffer x = new StringBuffer();
	String trimmedComment = getComment().trim();
	if(!(("").equals(trimmedComment)))
	{
	x.append("/**\n");
	x.append(getComment());
	x.append("\n*/\n");
	}
	return x.toString();
}
  /**
   * @ast method 
   * 
   */
  public CommentStmt() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public CommentStmt(String p0) {
    setComment(p0);
  }
  /**
   * @ast method 
   * 
   */
  public CommentStmt(beaver.Symbol p0) {
    setComment(p0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the lexeme Comment.
   * @param value The new value for the lexeme Comment.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setComment(String value) {
    tokenString_Comment = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Comment;
  /**
   * @ast method 
   * 
   */
  
  public int Commentstart;
  /**
   * @ast method 
   * 
   */
  
  public int Commentend;
  /**
   * JastAdd-internal setter for lexeme Comment using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setComment(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setComment is only valid for String lexemes");
    tokenString_Comment = (String)symbol.value;
    Commentstart = symbol.getStart();
    Commentend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Comment.
   * @return The value for the lexeme Comment.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getComment() {
    return tokenString_Comment != null ? tokenString_Comment : "";
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
