/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production ArrayAccess : {@link AccessVal} ::= <span class="component">ArrayID:{@link IDUse}</span> <span class="component">Indices:{@link Exp}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:27
 */
public class ArrayAccess extends AccessVal implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArrayAccess clone() throws CloneNotSupportedException {
    ArrayAccess node = (ArrayAccess)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArrayAccess copy() {
      try {
        ArrayAccess node = (ArrayAccess)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArrayAccess fullCopy() {
    try {
      ArrayAccess tree = (ArrayAccess) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/pretty.jadd:88
   */
  String pp(String indent)
  {
  	  StringBuffer x = new StringBuffer();
  	  x.append(indent);
  	  x.append(getArrayID().getID()+"(");
  	  x.append(getIndicesList().getChild(0).pp(""));
  	  for(int i=1; i<getIndicesList().getNumChild() ; i++)
  	  {
  		  x.append(", "+getIndicesList().getChild(i).pp(""));
  	  }
  	  x.append(")");
  	  return x.toString();
  }
  /**
   * @ast method 
   * 
   */
  public ArrayAccess() {
    super();

    setChild(new List(), 1);

  }
  /**
   * @ast method 
   * 
   */
  public ArrayAccess(IDUse p0, List<Exp> p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the ArrayID child.
   * @param node The new node to replace the ArrayID child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setArrayID(IDUse node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the ArrayID child.
   * @return The current node used as the ArrayID child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public IDUse getArrayID() {
    return (IDUse)getChild(0);
  }
  /**
   * Retrieves the ArrayID child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the ArrayID child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public IDUse getArrayIDNoTransform() {
    return (IDUse)getChildNoTransform(0);
  }
  /**
   * Replaces the Indices list.
   * @param list The new list node to be used as the Indices list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIndicesList(List<Exp> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Indices list.
   * @return Number of children in the Indices list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumIndices() {
    return getIndicesList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Indices list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Indices list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumIndicesNoTransform() {
    return getIndicesListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Indices list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Indices list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Exp getIndices(int i) {
    return (Exp)getIndicesList().getChild(i);
  }
  /**
   * Append an element to the Indices list.
   * @param node The element to append to the Indices list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addIndices(Exp node) {
    List<Exp> list = (parent == null || state == null) ? getIndicesListNoTransform() : getIndicesList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addIndicesNoTransform(Exp node) {
    List<Exp> list = getIndicesListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Indices list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIndices(Exp node, int i) {
    List<Exp> list = getIndicesList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Indices list.
   * @return The node representing the Indices list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Exp> getIndicess() {
    return getIndicesList();
  }
  /**
   * Retrieves the Indices list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Indices list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Exp> getIndicessNoTransform() {
    return getIndicesListNoTransform();
  }
  /**
   * Retrieves the Indices list.
   * @return The node representing the Indices list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Exp> getIndicesList() {
    List<Exp> list = (List<Exp>)getChild(1);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Indices list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Indices list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Exp> getIndicesListNoTransform() {
    return (List<Exp>)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
