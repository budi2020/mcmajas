/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production DeclStmt : {@link Stmt} ::= <span class="component">[{@link MultiDeclLHS}]</span> <span class="component">LHS:{@link IDInfo}</span> <span class="component">[RHS:{@link Exp}]</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:6
 */
public class DeclStmt extends Stmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public DeclStmt clone() throws CloneNotSupportedException {
    DeclStmt node = (DeclStmt)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public DeclStmt copy() {
      try {
        DeclStmt node = (DeclStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public DeclStmt fullCopy() {
    try {
      DeclStmt tree = (DeclStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/pretty.jadd:28
   */
  String pp(String indent){
		StringBuffer x = new StringBuffer();

		if (null != getLHS()) {
			/**
			 * It is not a list assignment
			 */

			if (null == getLHS().getShape()) {
				/* Scalar variable */
				x.append("var " + getLHS().getName() + ": "
						+ getLHS().getType().getName());
				x.append(" = ");
				x.append(getRHS().pp("") + ";\n");

			}

			else if (null != getLHS().getShape()) {
				boolean tf = true;
				for (int i = 0; i < getLHS().getShape().size(); i++) {
					if(null != getLHS().getShape().get(i))
					tf &= ("1").equals(getLHS().getShape().get(i).toString());
				}
				if (tf) {

					/* Scalar variable */
					x.append("var " + getLHS().getName() + ": "
							+ getLHS().getType().getName());
					x.append(" = ");
					x.append(getRHS().pp("") + ";\n");
				}

			} else // array
			{

				x.append("val " + getLHS().getName() + " = " + "new Array["
						+ getLHS().getType().getName() + "]" + "("
						+ PPHelper.makeRange(getLHS().getShape()) + ", "
						+ getRHS().pp("") + ");\n");
			}

		} else {
			/**
			 * TODO Case when assigned to a list
			 */
		}
		return x.toString();
	}
  /**
   * @ast method 
   * 
   */
  public DeclStmt() {
    super();

    setChild(new Opt(), 0);
    setChild(new Opt(), 2);

  }
  /**
   * @ast method 
   * 
   */
  public DeclStmt(Opt<MultiDeclLHS> p0, IDInfo p1, Opt<Exp> p2) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 3;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the optional node for the MultiDeclLHS child. This is the {@code Opt} node containing the child MultiDeclLHS, not the actual child!
   * @param opt The new node to be used as the optional node for the MultiDeclLHS child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setMultiDeclLHSOpt(Opt<MultiDeclLHS> opt) {
    setChild(opt, 0);
  }
  /**
   * Check whether the optional MultiDeclLHS child exists.
   * @return {@code true} if the optional MultiDeclLHS child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasMultiDeclLHS() {
    return getMultiDeclLHSOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) MultiDeclLHS child.
   * @return The MultiDeclLHS child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public MultiDeclLHS getMultiDeclLHS() {
    return (MultiDeclLHS)getMultiDeclLHSOpt().getChild(0);
  }
  /**
   * Replaces the (optional) MultiDeclLHS child.
   * @param node The new node to be used as the MultiDeclLHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setMultiDeclLHS(MultiDeclLHS node) {
    getMultiDeclLHSOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the MultiDeclLHS child. This is the {@code Opt} node containing the child MultiDeclLHS, not the actual child!
   * @return The optional node for child the MultiDeclLHS child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<MultiDeclLHS> getMultiDeclLHSOpt() {
    return (Opt<MultiDeclLHS>)getChild(0);
  }
  /**
   * Retrieves the optional node for child MultiDeclLHS. This is the {@code Opt} node containing the child MultiDeclLHS, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child MultiDeclLHS.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<MultiDeclLHS> getMultiDeclLHSOptNoTransform() {
    return (Opt<MultiDeclLHS>)getChildNoTransform(0);
  }
  /**
   * Replaces the LHS child.
   * @param node The new node to replace the LHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setLHS(IDInfo node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the LHS child.
   * @return The current node used as the LHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public IDInfo getLHS() {
    return (IDInfo)getChild(1);
  }
  /**
   * Retrieves the LHS child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the LHS child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public IDInfo getLHSNoTransform() {
    return (IDInfo)getChildNoTransform(1);
  }
  /**
   * Replaces the optional node for the RHS child. This is the {@code Opt} node containing the child RHS, not the actual child!
   * @param opt The new node to be used as the optional node for the RHS child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setRHSOpt(Opt<Exp> opt) {
    setChild(opt, 2);
  }
  /**
   * Check whether the optional RHS child exists.
   * @return {@code true} if the optional RHS child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasRHS() {
    return getRHSOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) RHS child.
   * @return The RHS child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Exp getRHS() {
    return (Exp)getRHSOpt().getChild(0);
  }
  /**
   * Replaces the (optional) RHS child.
   * @param node The new node to be used as the RHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setRHS(Exp node) {
    getRHSOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the RHS child. This is the {@code Opt} node containing the child RHS, not the actual child!
   * @return The optional node for child the RHS child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<Exp> getRHSOpt() {
    return (Opt<Exp>)getChild(2);
  }
  /**
   * Retrieves the optional node for child RHS. This is the {@code Opt} node containing the child RHS, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child RHS.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<Exp> getRHSOptNoTransform() {
    return (Opt<Exp>)getChildNoTransform(2);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
