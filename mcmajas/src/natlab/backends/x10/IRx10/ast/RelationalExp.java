/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;

/**
 * @production RelationalExp : {@link BinaryExp};
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:57
 */
public abstract class RelationalExp extends BinaryExp implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public RelationalExp clone() throws CloneNotSupportedException {
    RelationalExp node = (RelationalExp)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @ast method 
   * 
   */
  public RelationalExp() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public RelationalExp(Exp p0, Exp p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the LeftOp child.
   * @param node The new node to replace the LeftOp child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setLeftOp(Exp node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the LeftOp child.
   * @return The current node used as the LeftOp child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Exp getLeftOp() {
    return (Exp)getChild(0);
  }
  /**
   * Retrieves the LeftOp child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the LeftOp child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Exp getLeftOpNoTransform() {
    return (Exp)getChildNoTransform(0);
  }
  /**
   * Replaces the RightOp child.
   * @param node The new node to replace the RightOp child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setRightOp(Exp node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the RightOp child.
   * @return The current node used as the RightOp child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Exp getRightOp() {
    return (Exp)getChild(1);
  }
  /**
   * Retrieves the RightOp child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the RightOp child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Exp getRightOpNoTransform() {
    return (Exp)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
