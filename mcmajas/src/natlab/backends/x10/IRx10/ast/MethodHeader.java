/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production MethodHeader : {@link ASTNode} ::= <span class="component">ReturnType:{@link AccessVal}</span> <span class="component">&lt;Name:String&gt;</span> <span class="component">{@link Args}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:8
 */
public class MethodHeader extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public MethodHeader clone() throws CloneNotSupportedException {
    MethodHeader node = (MethodHeader)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public MethodHeader copy() {
      try {
        MethodHeader node = (MethodHeader)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public MethodHeader fullCopy() {
    try {
      MethodHeader tree = (MethodHeader) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/pretty.jadd:122
   */
  String pp(String indent)
{
	StringBuffer x = new StringBuffer();
	
	x.append(indent+"static def "+getName()+" ("+PPHelper.makeArgs(getArgsList())+")");
	
	return x.toString();
}
  /**
   * @ast method 
   * 
   */
  public MethodHeader() {
    super();

    setChild(new List(), 1);

  }
  /**
   * @ast method 
   * 
   */
  public MethodHeader(AccessVal p0, String p1, List<Args> p2) {
    setChild(p0, 0);
    setName(p1);
    setChild(p2, 1);
  }
  /**
   * @ast method 
   * 
   */
  public MethodHeader(AccessVal p0, beaver.Symbol p1, List<Args> p2) {
    setChild(p0, 0);
    setName(p1);
    setChild(p2, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the ReturnType child.
   * @param node The new node to replace the ReturnType child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setReturnType(AccessVal node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the ReturnType child.
   * @return The current node used as the ReturnType child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public AccessVal getReturnType() {
    return (AccessVal)getChild(0);
  }
  /**
   * Retrieves the ReturnType child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the ReturnType child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public AccessVal getReturnTypeNoTransform() {
    return (AccessVal)getChildNoTransform(0);
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setName(String value) {
    tokenString_Name = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Name;
  /**
   * @ast method 
   * 
   */
  
  public int Namestart;
  /**
   * @ast method 
   * 
   */
  
  public int Nameend;
  /**
   * JastAdd-internal setter for lexeme Name using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setName(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setName is only valid for String lexemes");
    tokenString_Name = (String)symbol.value;
    Namestart = symbol.getStart();
    Nameend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
  /**
   * Replaces the Args list.
   * @param list The new list node to be used as the Args list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setArgsList(List<Args> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Args list.
   * @return Number of children in the Args list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumArgs() {
    return getArgsList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Args list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Args list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumArgsNoTransform() {
    return getArgsListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Args list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Args list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Args getArgs(int i) {
    return (Args)getArgsList().getChild(i);
  }
  /**
   * Append an element to the Args list.
   * @param node The element to append to the Args list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addArgs(Args node) {
    List<Args> list = (parent == null || state == null) ? getArgsListNoTransform() : getArgsList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addArgsNoTransform(Args node) {
    List<Args> list = getArgsListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Args list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setArgs(Args node, int i) {
    List<Args> list = getArgsList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Args list.
   * @return The node representing the Args list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Args> getArgss() {
    return getArgsList();
  }
  /**
   * Retrieves the Args list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Args list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Args> getArgssNoTransform() {
    return getArgsListNoTransform();
  }
  /**
   * Retrieves the Args list.
   * @return The node representing the Args list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Args> getArgsList() {
    List<Args> list = (List<Args>)getChild(1);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Args list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Args list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Args> getArgsListNoTransform() {
    return (List<Args>)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
