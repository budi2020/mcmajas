/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production ForStmt : {@link Stmt} ::= <span class="component">{@link AssignStmt}</span> <span class="component">Condition:{@link Exp}</span> <span class="component">Stepper:{@link AdditiveExp}</span> <span class="component">{@link LoopBody}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:75
 */
public class ForStmt extends Stmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ForStmt clone() throws CloneNotSupportedException {
    ForStmt node = (ForStmt)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ForStmt copy() {
      try {
        ForStmt node = (ForStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ForStmt fullCopy() {
    try {
      ForStmt tree = (ForStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect PrettyPrinter
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/pretty.jadd:330
   */
  String pp (String indent){
	  StringBuffer x= new StringBuffer();
	  x.append(indent);
	  x.append("for ("+this.getAssignStmt().pp("")+";"+getCondition().pp("")+";"+getStepper().pp("")+")\n"+getLoopBody().pp(indent+"    "));
	  return x.toString();
}
  /**
   * @ast method 
   * 
   */
  public ForStmt() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public ForStmt(AssignStmt p0, Exp p1, AdditiveExp p2, LoopBody p3) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
    setChild(p3, 3);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 4;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the AssignStmt child.
   * @param node The new node to replace the AssignStmt child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setAssignStmt(AssignStmt node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the AssignStmt child.
   * @return The current node used as the AssignStmt child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public AssignStmt getAssignStmt() {
    return (AssignStmt)getChild(0);
  }
  /**
   * Retrieves the AssignStmt child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the AssignStmt child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public AssignStmt getAssignStmtNoTransform() {
    return (AssignStmt)getChildNoTransform(0);
  }
  /**
   * Replaces the Condition child.
   * @param node The new node to replace the Condition child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setCondition(Exp node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the Condition child.
   * @return The current node used as the Condition child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Exp getCondition() {
    return (Exp)getChild(1);
  }
  /**
   * Retrieves the Condition child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Condition child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Exp getConditionNoTransform() {
    return (Exp)getChildNoTransform(1);
  }
  /**
   * Replaces the Stepper child.
   * @param node The new node to replace the Stepper child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setStepper(AdditiveExp node) {
    setChild(node, 2);
  }
  /**
   * Retrieves the Stepper child.
   * @return The current node used as the Stepper child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public AdditiveExp getStepper() {
    return (AdditiveExp)getChild(2);
  }
  /**
   * Retrieves the Stepper child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Stepper child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public AdditiveExp getStepperNoTransform() {
    return (AdditiveExp)getChildNoTransform(2);
  }
  /**
   * Replaces the LoopBody child.
   * @param node The new node to replace the LoopBody child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setLoopBody(LoopBody node) {
    setChild(node, 3);
  }
  /**
   * Retrieves the LoopBody child.
   * @return The current node used as the LoopBody child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public LoopBody getLoopBody() {
    return (LoopBody)getChild(3);
  }
  /**
   * Retrieves the LoopBody child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the LoopBody child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public LoopBody getLoopBodyNoTransform() {
    return (LoopBody)getChildNoTransform(3);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
