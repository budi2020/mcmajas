/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package natlab.backends.x10.IRx10.ast;

import natlab.backends.x10.IRx10.ast.Args;
import natlab.backends.x10.IRx10.ast.List;
import natlab.backends.x10.IRx10.ast.PPHelper;
import natlab.backends.x10.IRx10.ast.Stmt;
import java.util.*;
/**
 * @production Dims : {@link ASTNode} ::= <span class="component">[{@link Exp}]</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab/backends/x10/IRx10/astgen/irx10.ast:15
 */
public class Dims extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Dims clone() throws CloneNotSupportedException {
    Dims node = (Dims)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Dims copy() {
      try {
        Dims node = (Dims)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Dims fullCopy() {
    try {
      Dims tree = (Dims) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * 
   */
  public Dims() {
    super();

    setChild(new Opt(), 0);
    is$Final(true);

  }
  /**
   * @ast method 
   * 
   */
  public Dims(Opt<Exp> p0) {
    setChild(p0, 0);
    is$Final(true);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the optional node for the Exp child. This is the {@code Opt} node containing the child Exp, not the actual child!
   * @param opt The new node to be used as the optional node for the Exp child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setExpOpt(Opt<Exp> opt) {
    setChild(opt, 0);
  }
  /**
   * Check whether the optional Exp child exists.
   * @return {@code true} if the optional Exp child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasExp() {
    return getExpOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) Exp child.
   * @return The Exp child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Exp getExp() {
    return (Exp)getExpOpt().getChild(0);
  }
  /**
   * Replaces the (optional) Exp child.
   * @param node The new node to be used as the Exp child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setExp(Exp node) {
    getExpOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the Exp child. This is the {@code Opt} node containing the child Exp, not the actual child!
   * @return The optional node for child the Exp child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<Exp> getExpOpt() {
    return (Opt<Exp>)getChild(0);
  }
  /**
   * Retrieves the optional node for child Exp. This is the {@code Opt} node containing the child Exp, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child Exp.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<Exp> getExpOptNoTransform() {
    return (Opt<Exp>)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
