package natlab.cs621.analysis;

import java.util.HashSet;
import java.util.Set;

import natlab.toolkits.analysis.Merger;
import natlab.toolkits.analysis.Mergers;
import nodecases.AbstractNodeCaseHandler;
import analysis.AbstractSimpleStructuralBackwardAnalysis;
import ast.ASTNode;
import ast.AssignStmt;
import ast.BinaryExpr;
import ast.EmptyStmt;
import ast.Expr;
import ast.ExprStmt;
import ast.ForStmt;
import ast.IfStmt;
import ast.List;
import ast.MatrixExpr;
import ast.NameExpr;
import ast.ParameterizedExpr;
import ast.Row;
import ast.Stmt;
import ast.WhileStmt;

/**
 * This is a simple reaching defs analysis. It doesn't handle function
 * parameters, global variables, or persistent variables. (It just maps variable
 * names to assignment statements). It also doesn't handle nested functions.
 * 
 * See NatlabAbstractStructuralBackwardAnalysis and
 */
public class LiveVars extends
		AbstractSimpleStructuralBackwardAnalysis<Set<String>> {
	// Factory method, instantiates and runs the analysis

	public static LiveVars of(ASTNode<?> tree) {
		LiveVars analysis = new LiveVars(tree);
		analysis.analyze();
		return analysis;
	}

	public void prettyPrint() {
		getTree().analyze(this.new Printer());
	}

	private LiveVars(ASTNode tree) {
		super(tree);
		currentInSet = newInitialFlow();
		currentOutSet = newInitialFlow();
		DEBUG = true;
	}

	@Override
	public void copy(Set<String> a, Set<String> b) {
		b.addAll(a);
	}

	// Here we define the merge operation. There are two "levels" of set union
	// here:
	// union on the maps by key (the union method)
	// if a key is in both maps, union the two sets (the UNION merger passed to
	// the method)
	@Override
	public void merge(Set<String> in1, Set<String> in2, Set<String> out) {
		if (in2 == out) {
			out.addAll(in1);
		} else if (in1 == out) {
			out.addAll(in2);
		} else if (in1 == in2) {
			out.addAll(in1);
		} else {
			out.clear();
			out.addAll(in1);
			out.addAll(in2);
		}
		// in1.union(UNION, in2, out);
	}

	@Override
	public Set<String> newInitialFlow() {
		return new HashSet<String>();
	}


//	@Override
//	public void caseNameExpr(NameExpr node) {
//		handleAssignOrNameExpr(node);
//	}
	
	@Override
	public void caseExprStmt(ExprStmt node) {
		System.out.println("caseExprStmt. node:" + node.getNodeString());
		handleStmt(node);
	}

	@Override
	public void caseAssignStmt(AssignStmt node) {
		System.out.println("caseAssignStmt. node:" + node.getNodeString());
		handleStmt(node);
	}
	
	

	/* a - b */
	private Set<String> minus(Set<String> a, Set<String> b) {
		Set<String> result = new HashSet<String>();
		for (String s : a) {
			if (!b.contains(s)) {
				result.add(s);
			}
		}
		return result;
	}
	
	private Set<String> getNames(Expr expr) {
		//System.out.println("getNames: expr:" + expr.getNodeString() + ",  type:" + expr.getClass());
		Set<String> results = new HashSet<String>();
		if (expr instanceof ParameterizedExpr) {
			List<Expr> argList = ((ParameterizedExpr) expr).getArgList();
			for (Expr arg : argList) {
				results.addAll(getNames(arg));
			}
			return results;
		} else if (expr instanceof BinaryExpr) {
			BinaryExpr binExpr = (BinaryExpr) expr;
			results.addAll(getNames(binExpr.getLHS()));
			results.addAll(getNames(binExpr.getRHS()));
			return results;
		} else if (expr instanceof MatrixExpr) {
			MatrixExpr matrixExpr = (MatrixExpr) expr;
			List<Row> rows = matrixExpr.getRowList();
			for (Row row : rows) {
				List<Expr> elements = row.getElementList();
				for (Expr arg : elements) {
					results.addAll(getNames(arg));
				}
			}
			return results;
		}

		Set<NameExpr> nameExprs = expr.getAllNameExpressions();
		for (NameExpr nameExpr : nameExprs) {
			results.add(nameExpr.getVarName());
		}
		
		return results;
	}

	private void handleStmt(Stmt node) {
		// live var = in = gen(n) + (out(n) \ kill(n))
		Set<String> kill = newInitialFlow();
		Set<String> gen = newInitialFlow();
		
		if (node instanceof AssignStmt) {
			AssignStmt assignStmt = (AssignStmt) node;
			gen = getNames(((AssignStmt) node).getRHS());
			if (assignStmt.getLHS() instanceof ParameterizedExpr) {
				// e.g. H(i, n) = 1;
				gen.addAll(getNames((ParameterizedExpr) assignStmt.getLHS()));
			} else {
				kill = ((AssignStmt) node).getLValues();
			}
		} else {
			int numChild = node.getNumChild();
			for (int i = 0; i < numChild; i++) {
				ASTNode<?> child = node.getChild(i);
				if (child instanceof ParameterizedExpr) {
					gen.addAll(getNames((ParameterizedExpr) child));
				}
			}
		}
		currentOutSet = currentInSet;

		// make sure currentInSet and currentOutSet are dif. objects from this point
		Set<String> outMinusKill = minus(currentOutSet, kill);
		currentInSet = newInitialFlow();
		currentInSet.addAll(gen);
		currentInSet.addAll(outMinusKill);
		inFlowSets.put(node, currentInSet);

		if (DEBUG) {
			System.out.println("assignStmt:" + node.getNodeString());
			System.out.println("-------------- gen:" + gen);
			System.out.println("-------------- kill:" + kill);
			System.out.println("-------------- out:" + currentOutSet);
			System.out.println("-------------- in:" + currentInSet);
		}
		
	}

	// This class pretty prints the program annotated with analysis results.
	class Printer extends AbstractNodeCaseHandler {

		private int getLine(ASTNode<?> node) {
			return beaver.Symbol.getLine(node.getStart());
		}

		private int getColumn(ASTNode<?> node) {
			return beaver.Symbol.getColumn(node.getStart());
		}

		@Override
		public void caseASTNode(ASTNode node) {
			for (int i = 0; i < node.getNumChild(); i++) {
				node.getChild(i).analyze(this);
			}
		}

		@Override
		public void caseStmt(Stmt node) {
			// System.out.println("in {");
			// printMap(node, inFlowSets.get(node));
			// System.out.println("}");
			//System.out.println(node.getPrettyPrinted());
			printMap(node, inFlowSets.get(node));
			// System.out.println("out {");
			// printMap(node, outFlowSets.get(node));
			// System.out.println("}");
			//System.out.println();

			// caseASTNode(node);
		}
		
		@Override
		public void caseIfStmt(IfStmt node) {
			printMap(node, inFlowSets.get(node));
			int childCount = node.getNumChild();
			for (int i = 0; i < childCount; i++) {
				node.getChild(i).analyze(this);
			}
		}

		@Override
		public void caseForStmt(ForStmt node) {
			printMap(node, inFlowSets.get(node));
			List<Stmt> stmtList = node.getStmts();
			stmtList.analyze(this);
		}

		@Override
		public void caseWhileStmt(WhileStmt node) {
			printMap(node, inFlowSets.get(node));
			List<Stmt> stmtList = node.getStmts();
			stmtList.analyze(this);
		}
		
		@Override
		public void caseEmptyStmt(EmptyStmt node) {
			return;
		}

		private String getFirstLine(String s) {
			if (s == null) {
				return null;
			}

			int index = s.indexOf("\n");
			if (index != -1) {
				return s.substring(0, index);
			} else {
				return s;
			}
		}

		private void printMap(ASTNode<?> node, Set<String> vars) {
			if (vars == null) {
				return;
			}
			boolean first = true;
			System.out.print("Live variables at \""
					+ getFirstLine(node.getNodeString()) + "\" at ("
					+ getLine(node) + ", " + getColumn(node) + ") are {");
			for (String var : vars) {
				if (!first) {
					System.out.print(", ");
				}
				first = false;
				System.out.print(var);
			}
			System.out.println("}");
		}
	}
}
