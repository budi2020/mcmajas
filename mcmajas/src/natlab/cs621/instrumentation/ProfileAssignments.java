package natlab.cs621.instrumentation;

import java.util.HashSet;
import java.util.Set;

import nodecases.AbstractNodeCaseHandler;
import ast.ASTNode;
import ast.AssignStmt;
import ast.Expr;
import ast.ExprStmt;
import ast.ForStmt;
import ast.Function;
import ast.List;
import ast.Name;
import ast.NameExpr;
import ast.ParameterizedExpr;
import ast.Script;
import ast.Stmt;
import ast.StringLiteralExpr;

/**
 * This class is an example of instrumentation. It rewrites an input program to
 * keep track of the number of assignments made at runtime.
 */
public class ProfileAssignments extends AbstractNodeCaseHandler {
	
	int counter = 1;

	private int getLine(ASTNode<?> node) {
		return beaver.Symbol.getLine(node.getStart());
	}

	private int getColumn(ASTNode<?> node) {
		return beaver.Symbol.getColumn(node.getStart());
	}
	
	public static void instrument(ASTNode<?> node) {
		node.analyze(new ProfileAssignments());
	}

	// This remembers either the current function name, or "script" for scripts
	private String currentScope;

	// Statements in the skip set won't be analyzed / instrumented.
	// In general we only want to analyze the input program, so we'll
	// add the instrumentation statements we create to this set.
	// There may also be other cases where we just want to skip a node for
	// whatever reason.
	private Set<Stmt> skip = new HashSet<Stmt>();

	// Little helper to add something to the skip set while still using it as an
	// expression
	private Stmt skip(Stmt node) {
		skip.add(node);
		return node;
	}

	private Stmt tic(String varName) {
		return new AssignStmt(new NameExpr(new Name(varName)), 
				new NameExpr(new Name("tic")));
	}

	private Stmt toc(String tocVar, String ticVar) {
		NameExpr nameExpr = new NameExpr(new Name(tocVar));
		
		List<Expr> exprs = new List<Expr>();
		exprs.add(new NameExpr(new Name(ticVar)));
		ParameterizedExpr parExpr = new ParameterizedExpr(new NameExpr(new Name("toc")), exprs);
		return new AssignStmt(nameExpr, parExpr);
	}

	private Stmt printTime(String s, String t) {
		// disp(sprintf('for took %d second(s).', t2));
		List<Expr> exprs = new List<Expr>();
		exprs.add(new StringLiteralExpr(s + " took %d second(s)"));
		exprs.add(new NameExpr(new Name(t)));

		ParameterizedExpr parExpt1 = new ParameterizedExpr(new NameExpr(new Name("sprintf")),
				exprs);
		// add a disp
		List<Expr> exprs2 = new List<Expr>();
		exprs2.add(parExpt1);
		ParameterizedExpr parExpt2 = new ParameterizedExpr(new NameExpr(new Name("disp")),
				exprs2);
		return new ExprStmt(parExpt2);
	}

	// This is the default node case. We recurse on the children from left to
	// right,
	// so we're traversing the AST depth-first.
	@Override
	public void caseASTNode(ASTNode node) {
		int numChild = node.getNumChild();
		for (int i = 0; i < numChild; ++i) {
			ASTNode child = node.getChild(i);
			child.analyze(this);
		}
	}

	@Override
	public void caseScript(Script node) {
		for (int i = 0; i < node.getNumStmt(); i++) {
			node.getStmt(i).analyze(this);
		}
	}

	
	@Override
	public void caseFunction(Function node) {
		for (int i = 0; i < node.getNumStmt(); i++) {
			node.getStmt(i).analyze(this);
		}
		node.getNestedFunctions().analyze(this);
	}

	/**
	 * We want to count assignments made in for loops, e.g. in for i = 1:10 a(i)
	 * = i; end
	 * 
	 * We want to count the assignments to a(i), but also the implicit
	 * assignments to i. We do this by adding a counter update as the first
	 * statement in the loop body.
	 */
	@Override
	public void caseForStmt(ForStmt node) {
		
		if (!skip.contains(node)) {
			String ticVar = "t" + counter++;
			AstUtil.insertBefore(node, tic(ticVar));
			String tocVar = "t" + counter++;
			String forPrettyPrint = node.getPrettyPrinted();
			int index = forPrettyPrint.indexOf("\n");
			StringBuilder sb = new StringBuilder();
			if (index != -1) {
				sb.append(forPrettyPrint.substring(0, index));
			} else {
				sb.append(forPrettyPrint);
			}
			sb.append(" at (" + getLine(node) + ", " + getColumn(node) + ")");
			AstUtil.insertAfter(node, printTime(sb.toString(), tocVar));
			AstUtil.insertAfter(node, toc(tocVar, ticVar));
		}
		// skip the assignment in the for loop header, as we're handling it
		// ourselves
		skip.add(node);
		caseASTNode(node);
	}
}