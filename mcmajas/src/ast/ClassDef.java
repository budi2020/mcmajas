/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production ClassDef : {@link Program} ::= <span class="component">{@link Attribute}*</span> <span class="component">&lt;Name:String&gt;</span> <span class="component">{@link SuperClass}*</span> <span class="component">{@link HelpComment}*</span> <span class="component">Property:{@link Properties}*</span> <span class="component">Method:{@link Methods}*</span> <span class="component">ClassEvent:{@link ClassEvents}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:33
 */
public class ClassDef extends Program implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getPrettyPrintedLessComments_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ClassDef clone() throws CloneNotSupportedException {
    ClassDef node = (ClassDef)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ClassDef copy() {
      try {
        ClassDef node = (ClassDef)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ClassDef fullCopy() {
    try {
      ClassDef tree = (ClassDef) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:319
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseClassDef(this);
    }
  /**
   * @ast method 
   * 
   */
  public ClassDef() {
    super();

    setChild(new List(), 0);
    setChild(new List(), 1);
    setChild(new List(), 2);
    setChild(new List(), 3);
    setChild(new List(), 4);
    setChild(new List(), 5);

  }
  /**
   * @ast method 
   * 
   */
  public ClassDef(List<Attribute> p0, String p1, List<SuperClass> p2, List<HelpComment> p3, List<Properties> p4, List<Methods> p5, List<ClassEvents> p6) {
    setChild(p0, 0);
    setName(p1);
    setChild(p2, 1);
    setChild(p3, 2);
    setChild(p4, 3);
    setChild(p5, 4);
    setChild(p6, 5);
  }
  /**
   * @ast method 
   * 
   */
  public ClassDef(List<Attribute> p0, beaver.Symbol p1, List<SuperClass> p2, List<HelpComment> p3, List<Properties> p4, List<Methods> p5, List<ClassEvents> p6) {
    setChild(p0, 0);
    setName(p1);
    setChild(p2, 1);
    setChild(p3, 2);
    setChild(p4, 3);
    setChild(p5, 4);
    setChild(p6, 5);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 6;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Attribute list.
   * @param list The new list node to be used as the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setAttributeList(List<Attribute> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Attribute list.
   * @return Number of children in the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumAttribute() {
    return getAttributeList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Attribute list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Attribute list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumAttributeNoTransform() {
    return getAttributeListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Attribute list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Attribute getAttribute(int i) {
    return (Attribute)getAttributeList().getChild(i);
  }
  /**
   * Append an element to the Attribute list.
   * @param node The element to append to the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addAttribute(Attribute node) {
    List<Attribute> list = (parent == null || state == null) ? getAttributeListNoTransform() : getAttributeList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addAttributeNoTransform(Attribute node) {
    List<Attribute> list = getAttributeListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Attribute list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setAttribute(Attribute node, int i) {
    List<Attribute> list = getAttributeList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Attribute list.
   * @return The node representing the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Attribute> getAttributes() {
    return getAttributeList();
  }
  /**
   * Retrieves the Attribute list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Attribute list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Attribute> getAttributesNoTransform() {
    return getAttributeListNoTransform();
  }
  /**
   * Retrieves the Attribute list.
   * @return The node representing the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Attribute> getAttributeList() {
    List<Attribute> list = (List<Attribute>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Attribute list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Attribute list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Attribute> getAttributeListNoTransform() {
    return (List<Attribute>)getChildNoTransform(0);
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setName(String value) {
    tokenString_Name = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Name;
  /**
   * @ast method 
   * 
   */
  
  public int Namestart;
  /**
   * @ast method 
   * 
   */
  
  public int Nameend;
  /**
   * JastAdd-internal setter for lexeme Name using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setName(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setName is only valid for String lexemes");
    tokenString_Name = (String)symbol.value;
    Namestart = symbol.getStart();
    Nameend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
  /**
   * Replaces the SuperClass list.
   * @param list The new list node to be used as the SuperClass list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setSuperClassList(List<SuperClass> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the SuperClass list.
   * @return Number of children in the SuperClass list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumSuperClass() {
    return getSuperClassList().getNumChild();
  }
  /**
   * Retrieves the number of children in the SuperClass list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the SuperClass list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumSuperClassNoTransform() {
    return getSuperClassListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the SuperClass list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the SuperClass list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public SuperClass getSuperClass(int i) {
    return (SuperClass)getSuperClassList().getChild(i);
  }
  /**
   * Append an element to the SuperClass list.
   * @param node The element to append to the SuperClass list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addSuperClass(SuperClass node) {
    List<SuperClass> list = (parent == null || state == null) ? getSuperClassListNoTransform() : getSuperClassList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addSuperClassNoTransform(SuperClass node) {
    List<SuperClass> list = getSuperClassListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the SuperClass list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setSuperClass(SuperClass node, int i) {
    List<SuperClass> list = getSuperClassList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the SuperClass list.
   * @return The node representing the SuperClass list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<SuperClass> getSuperClasss() {
    return getSuperClassList();
  }
  /**
   * Retrieves the SuperClass list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SuperClass list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<SuperClass> getSuperClasssNoTransform() {
    return getSuperClassListNoTransform();
  }
  /**
   * Retrieves the SuperClass list.
   * @return The node representing the SuperClass list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<SuperClass> getSuperClassList() {
    List<SuperClass> list = (List<SuperClass>)getChild(1);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the SuperClass list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SuperClass list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<SuperClass> getSuperClassListNoTransform() {
    return (List<SuperClass>)getChildNoTransform(1);
  }
  /**
   * Replaces the HelpComment list.
   * @param list The new list node to be used as the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setHelpCommentList(List<HelpComment> list) {
    setChild(list, 2);
  }
  /**
   * Retrieves the number of children in the HelpComment list.
   * @return Number of children in the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumHelpComment() {
    return getHelpCommentList().getNumChild();
  }
  /**
   * Retrieves the number of children in the HelpComment list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the HelpComment list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumHelpCommentNoTransform() {
    return getHelpCommentListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the HelpComment list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public HelpComment getHelpComment(int i) {
    return (HelpComment)getHelpCommentList().getChild(i);
  }
  /**
   * Append an element to the HelpComment list.
   * @param node The element to append to the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addHelpComment(HelpComment node) {
    List<HelpComment> list = (parent == null || state == null) ? getHelpCommentListNoTransform() : getHelpCommentList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addHelpCommentNoTransform(HelpComment node) {
    List<HelpComment> list = getHelpCommentListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the HelpComment list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setHelpComment(HelpComment node, int i) {
    List<HelpComment> list = getHelpCommentList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the HelpComment list.
   * @return The node representing the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<HelpComment> getHelpComments() {
    return getHelpCommentList();
  }
  /**
   * Retrieves the HelpComment list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the HelpComment list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<HelpComment> getHelpCommentsNoTransform() {
    return getHelpCommentListNoTransform();
  }
  /**
   * Retrieves the HelpComment list.
   * @return The node representing the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<HelpComment> getHelpCommentList() {
    List<HelpComment> list = (List<HelpComment>)getChild(2);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the HelpComment list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the HelpComment list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<HelpComment> getHelpCommentListNoTransform() {
    return (List<HelpComment>)getChildNoTransform(2);
  }
  /**
   * Replaces the Property list.
   * @param list The new list node to be used as the Property list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setPropertyList(List<Properties> list) {
    setChild(list, 3);
  }
  /**
   * Retrieves the number of children in the Property list.
   * @return Number of children in the Property list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumProperty() {
    return getPropertyList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Property list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Property list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumPropertyNoTransform() {
    return getPropertyListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Property list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Property list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Properties getProperty(int i) {
    return (Properties)getPropertyList().getChild(i);
  }
  /**
   * Append an element to the Property list.
   * @param node The element to append to the Property list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addProperty(Properties node) {
    List<Properties> list = (parent == null || state == null) ? getPropertyListNoTransform() : getPropertyList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addPropertyNoTransform(Properties node) {
    List<Properties> list = getPropertyListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Property list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setProperty(Properties node, int i) {
    List<Properties> list = getPropertyList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Property list.
   * @return The node representing the Property list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Properties> getPropertys() {
    return getPropertyList();
  }
  /**
   * Retrieves the Property list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Property list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Properties> getPropertysNoTransform() {
    return getPropertyListNoTransform();
  }
  /**
   * Retrieves the Property list.
   * @return The node representing the Property list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Properties> getPropertyList() {
    List<Properties> list = (List<Properties>)getChild(3);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Property list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Property list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Properties> getPropertyListNoTransform() {
    return (List<Properties>)getChildNoTransform(3);
  }
  /**
   * Replaces the Method list.
   * @param list The new list node to be used as the Method list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setMethodList(List<Methods> list) {
    setChild(list, 4);
  }
  /**
   * Retrieves the number of children in the Method list.
   * @return Number of children in the Method list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumMethod() {
    return getMethodList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Method list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Method list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumMethodNoTransform() {
    return getMethodListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Method list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Method list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Methods getMethod(int i) {
    return (Methods)getMethodList().getChild(i);
  }
  /**
   * Append an element to the Method list.
   * @param node The element to append to the Method list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addMethod(Methods node) {
    List<Methods> list = (parent == null || state == null) ? getMethodListNoTransform() : getMethodList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addMethodNoTransform(Methods node) {
    List<Methods> list = getMethodListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Method list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setMethod(Methods node, int i) {
    List<Methods> list = getMethodList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Method list.
   * @return The node representing the Method list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Methods> getMethods() {
    return getMethodList();
  }
  /**
   * Retrieves the Method list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Method list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Methods> getMethodsNoTransform() {
    return getMethodListNoTransform();
  }
  /**
   * Retrieves the Method list.
   * @return The node representing the Method list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Methods> getMethodList() {
    List<Methods> list = (List<Methods>)getChild(4);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Method list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Method list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Methods> getMethodListNoTransform() {
    return (List<Methods>)getChildNoTransform(4);
  }
  /**
   * Replaces the ClassEvent list.
   * @param list The new list node to be used as the ClassEvent list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setClassEventList(List<ClassEvents> list) {
    setChild(list, 5);
  }
  /**
   * Retrieves the number of children in the ClassEvent list.
   * @return Number of children in the ClassEvent list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumClassEvent() {
    return getClassEventList().getNumChild();
  }
  /**
   * Retrieves the number of children in the ClassEvent list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the ClassEvent list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumClassEventNoTransform() {
    return getClassEventListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the ClassEvent list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the ClassEvent list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ClassEvents getClassEvent(int i) {
    return (ClassEvents)getClassEventList().getChild(i);
  }
  /**
   * Append an element to the ClassEvent list.
   * @param node The element to append to the ClassEvent list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addClassEvent(ClassEvents node) {
    List<ClassEvents> list = (parent == null || state == null) ? getClassEventListNoTransform() : getClassEventList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addClassEventNoTransform(ClassEvents node) {
    List<ClassEvents> list = getClassEventListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the ClassEvent list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setClassEvent(ClassEvents node, int i) {
    List<ClassEvents> list = getClassEventList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the ClassEvent list.
   * @return The node representing the ClassEvent list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<ClassEvents> getClassEvents() {
    return getClassEventList();
  }
  /**
   * Retrieves the ClassEvent list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the ClassEvent list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<ClassEvents> getClassEventsNoTransform() {
    return getClassEventListNoTransform();
  }
  /**
   * Retrieves the ClassEvent list.
   * @return The node representing the ClassEvent list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<ClassEvents> getClassEventList() {
    List<ClassEvents> list = (List<ClassEvents>)getChild(5);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the ClassEvent list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the ClassEvent list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<ClassEvents> getClassEventListNoTransform() {
    return (List<ClassEvents>)getChildNoTransform(5);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:239
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
        long id = nodeCounter();
        setuID(id);

        Element e = doc.createElement("ClassDef");
		e.setAttribute("id", Long.toString(id));
		e.setAttribute("name", getName());

        
        for(Attribute attr : getAttributes()) {
            attr.getXML(doc, e);
        }        		
		
        for(SuperClass sup : getSuperClasss()) {
            sup.getXML(doc, e);
        }
        
        for(HelpComment comment : getHelpComments()) {
            comment.getXML(doc, e);
        }
        for(Properties prop : getPropertys()) {
            prop.getXML(doc, e);
        }
        for(Methods methods : getMethods()) {
            methods.getXML(doc, e);
        }
        for(ClassEvents events : getClassEvents()) {
            events.getXML(doc, e);
        }
        e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
        parent.appendChild(e);
        return true;
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:516
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append("classdef ");
		boolean first = true;
        for(Attribute attr : getAttributes()) {
            if(first)
            	buf.append("(");            
            else
                buf.append(", ");
            buf.append(attr.getPrettyPrinted());
            first = false;
        }
        if(!first)
        	buf.append(") ");
        buf.append(getName());
        first = true;
        for(SuperClass sup : getSuperClasss()) {
            if(first)
            	buf.append(" < ");
            else
                buf.append(" & ");
            buf.append(sup.getPrettyPrinted());
            first = false;
        }
        buf.append('\n');
        for(HelpComment comment : getHelpComments()) {
            buf.append(comment.getPrettyPrinted());
            buf.append('\n');
        }
        for(Properties prop : getPropertys()) {
            buf.append(prop.getPrettyPrinted());
            buf.append('\n');
        }
        for(Methods methods : getMethods()) {
            buf.append(methods.getPrettyPrinted());
            buf.append('\n');
        }
        for(ClassEvents events : getClassEvents()) {
            buf.append(events.getPrettyPrinted());
            buf.append('\n');
        }
        buf.append("end");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:95
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append("classdef ");
		boolean first = true;
        for(Attribute attr : getAttributes()) {
            if(first)
            	buf.append("(");            
            else
                buf.append(", ");
            buf.append(attr.getStructureString());
            first = false;
        }
        if(!first)
        	buf.append(") ");
        buf.append(getName());
        first = true;
        for(SuperClass sup : getSuperClasss()) {
            if(first)
            	buf.append(" < ");
            else
                buf.append(" & ");
            buf.append(sup.getStructureString());
            first = false;
        }
        buf.append('\n');
        for(HelpComment comment : getHelpComments()) {
            buf.append(comment.getStructureString());
            buf.append('\n');
        }
        for(Properties prop : getPropertys()) {
            buf.append(prop.getStructureString());
            buf.append('\n');
        }
        for(Methods methods : getMethods()) {
            buf.append(methods.getStructureString());
            buf.append('\n');
        }
        for(ClassEvents events : getClassEvents()) {
            buf.append(events.getStructureString());
            buf.append('\n');
        }
        buf.append("end");
        return buf.toString();
    }
  /**
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:52
   * @apilevel internal
   */
  public Map<String, Function> Define_Map_String__Function__getSiblings(ASTNode caller, ASTNode child) {
    if(caller == getMethodListNoTransform()) {
      int i = caller.getIndexOfChild(child);
      return new HashMap<String,Function>();
    }
    else {      return getParent().Define_Map_String__Function__getSiblings(this, caller);
    }
  }
  /**
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:57
   * @apilevel internal
   */
  public LocalFunctionLookupInterface Define_LocalFunctionLookupInterface_getParentFunction(ASTNode caller, ASTNode child) {
    if(caller == getMethodListNoTransform()) {
      int i = caller.getIndexOfChild(child);
      return null;
    }
    else {      return getParent().Define_LocalFunctionLookupInterface_getParentFunction(this, caller);
    }
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
