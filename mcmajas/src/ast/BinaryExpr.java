/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production BinaryExpr : {@link Expr} ::= <span class="component">LHS:{@link Expr}</span> <span class="component">RHS:{@link Expr}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:107
 */
public abstract class BinaryExpr extends Expr implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getOperatorName_visited = -1;
    getPrettyPrintedLessComments_String_visited = null;
    getStructureStringLessComments_String_visited = null;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public BinaryExpr clone() throws CloneNotSupportedException {
    BinaryExpr node = (BinaryExpr)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getOperatorName_visited = -1;
    node.getPrettyPrintedLessComments_String_visited = null;
    node.getStructureStringLessComments_String_visited = null;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:391
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseBinaryExpr(this);
    }
  /**
   * @ast method 
   * 
   */
  public BinaryExpr() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public BinaryExpr(Expr p0, Expr p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the LHS child.
   * @param node The new node to replace the LHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setLHS(Expr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the LHS child.
   * @return The current node used as the LHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Expr getLHS() {
    return (Expr)getChild(0);
  }
  /**
   * Retrieves the LHS child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the LHS child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Expr getLHSNoTransform() {
    return (Expr)getChildNoTransform(0);
  }
  /**
   * Replaces the RHS child.
   * @param node The new node to replace the RHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setRHS(Expr node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the RHS child.
   * @return The current node used as the RHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Expr getRHS() {
    return (Expr)getChild(1);
  }
  /**
   * Retrieves the RHS child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the RHS child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Expr getRHSNoTransform() {
    return (Expr)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:1069
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
        long id = nodeCounter();
        setuID(id);
    		String name = getClass().getName(); 

		Element e = doc.createElement(name.substring(name.lastIndexOf('.')+1));
		e.setAttribute("id", Long.toString(id));

        getLHS().getXML(doc, e);
        getRHS().getXML(doc, e);

        e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
parent.appendChild(e);
        return true;
    }
  /**
   * @apilevel internal
   */
  protected int getOperatorName_visited = -1;
  /**
     * Returns the name of the matlab function which is associated with the operator as a String.
     * @attribute syn
   * @aspect OperatorNames
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/OperatorNames.jrag:30
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getOperatorName() {
      ASTNode$State state = state();
    if(getOperatorName_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getOperatorName in class: ");
    getOperatorName_visited = state().boundariesCrossed;
    String getOperatorName_value = getOperatorName_compute();
    getOperatorName_visited = -1;
    return getOperatorName_value;
  }
  /**
   * @apilevel internal
   */
  private String getOperatorName_compute() { return ""; }
  /**
   * @apilevel internal
   */
  protected java.util.Map getPrettyPrintedLessComments_String_visited;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:488
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments(String op) {
    Object _parameters = op;
    if(getPrettyPrintedLessComments_String_visited == null) getPrettyPrintedLessComments_String_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getPrettyPrintedLessComments_String_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String getPrettyPrintedLessComments_String_value = getPrettyPrintedLessComments_compute(op);
    getPrettyPrintedLessComments_String_visited.remove(_parameters);
    return getPrettyPrintedLessComments_String_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute(String op) {  return "(" + getLHS().getPrettyPrinted() + " " + op + " " + getRHS().getPrettyPrinted() + ")";  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getStructureStringLessComments_String_visited;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:595
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments(String op) {
    Object _parameters = op;
    if(getStructureStringLessComments_String_visited == null) getStructureStringLessComments_String_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getStructureStringLessComments_String_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String getStructureStringLessComments_String_value = getStructureStringLessComments_compute(op);
    getStructureStringLessComments_String_visited.remove(_parameters);
    return getStructureStringLessComments_String_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute(String op) {  return "(" + getLHS().getStructureString() + " " + op + " " + getRHS().getStructureString() + ")";  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
