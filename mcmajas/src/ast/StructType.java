/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production StructType : {@link BaseType} ::= <span class="component">{@link FieldEntry}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/types.ast:38
 */
public class StructType extends BaseType implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public StructType clone() throws CloneNotSupportedException {
    StructType node = (StructType)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public StructType copy() {
      try {
        StructType node = (StructType)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public StructType fullCopy() {
    try {
      StructType tree = (StructType) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:494
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseStructType(this);
    }
  /**
   * @ast method 
   * 
   */
  public StructType() {
    super();

    setChild(new List(), 0);

  }
  /**
   * @ast method 
   * 
   */
  public StructType(List<FieldEntry> p0) {
    setChild(p0, 0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the FieldEntry list.
   * @param list The new list node to be used as the FieldEntry list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setFieldEntryList(List<FieldEntry> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the FieldEntry list.
   * @return Number of children in the FieldEntry list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumFieldEntry() {
    return getFieldEntryList().getNumChild();
  }
  /**
   * Retrieves the number of children in the FieldEntry list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the FieldEntry list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumFieldEntryNoTransform() {
    return getFieldEntryListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the FieldEntry list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the FieldEntry list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public FieldEntry getFieldEntry(int i) {
    return (FieldEntry)getFieldEntryList().getChild(i);
  }
  /**
   * Append an element to the FieldEntry list.
   * @param node The element to append to the FieldEntry list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addFieldEntry(FieldEntry node) {
    List<FieldEntry> list = (parent == null || state == null) ? getFieldEntryListNoTransform() : getFieldEntryList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addFieldEntryNoTransform(FieldEntry node) {
    List<FieldEntry> list = getFieldEntryListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the FieldEntry list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setFieldEntry(FieldEntry node, int i) {
    List<FieldEntry> list = getFieldEntryList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the FieldEntry list.
   * @return The node representing the FieldEntry list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<FieldEntry> getFieldEntrys() {
    return getFieldEntryList();
  }
  /**
   * Retrieves the FieldEntry list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the FieldEntry list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<FieldEntry> getFieldEntrysNoTransform() {
    return getFieldEntryListNoTransform();
  }
  /**
   * Retrieves the FieldEntry list.
   * @return The node representing the FieldEntry list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<FieldEntry> getFieldEntryList() {
    List<FieldEntry> list = (List<FieldEntry>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the FieldEntry list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the FieldEntry list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<FieldEntry> getFieldEntryListNoTransform() {
    return (List<FieldEntry>)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
