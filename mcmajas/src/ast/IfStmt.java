/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production IfStmt : {@link Stmt} ::= <span class="component">{@link IfBlock}*</span> <span class="component">[{@link ElseBlock}]</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:74
 */
public class IfStmt extends Stmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    getLValues_visited = -1;
    getLValues_computed = false;
    getLValues_value = null;
    getPrettyPrintedLessComments_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IfStmt clone() throws CloneNotSupportedException {
    IfStmt node = (IfStmt)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    node.getLValues_visited = -1;
    node.getLValues_computed = false;
    node.getLValues_value = null;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IfStmt copy() {
      try {
        IfStmt node = (IfStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IfStmt fullCopy() {
    try {
      IfStmt tree = (IfStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:76
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseIfStmt(this);
    }
  /**
   * @ast method 
   * 
   */
  public IfStmt() {
    super();

    setChild(new List(), 0);
    setChild(new Opt(), 1);

  }
  /**
   * @ast method 
   * 
   */
  public IfStmt(List<IfBlock> p0, Opt<ElseBlock> p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the IfBlock list.
   * @param list The new list node to be used as the IfBlock list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIfBlockList(List<IfBlock> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the IfBlock list.
   * @return Number of children in the IfBlock list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumIfBlock() {
    return getIfBlockList().getNumChild();
  }
  /**
   * Retrieves the number of children in the IfBlock list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the IfBlock list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumIfBlockNoTransform() {
    return getIfBlockListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the IfBlock list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the IfBlock list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IfBlock getIfBlock(int i) {
    return (IfBlock)getIfBlockList().getChild(i);
  }
  /**
   * Append an element to the IfBlock list.
   * @param node The element to append to the IfBlock list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addIfBlock(IfBlock node) {
    List<IfBlock> list = (parent == null || state == null) ? getIfBlockListNoTransform() : getIfBlockList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addIfBlockNoTransform(IfBlock node) {
    List<IfBlock> list = getIfBlockListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the IfBlock list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIfBlock(IfBlock node, int i) {
    List<IfBlock> list = getIfBlockList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the IfBlock list.
   * @return The node representing the IfBlock list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<IfBlock> getIfBlocks() {
    return getIfBlockList();
  }
  /**
   * Retrieves the IfBlock list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the IfBlock list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<IfBlock> getIfBlocksNoTransform() {
    return getIfBlockListNoTransform();
  }
  /**
   * Retrieves the IfBlock list.
   * @return The node representing the IfBlock list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<IfBlock> getIfBlockList() {
    List<IfBlock> list = (List<IfBlock>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the IfBlock list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the IfBlock list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<IfBlock> getIfBlockListNoTransform() {
    return (List<IfBlock>)getChildNoTransform(0);
  }
  /**
   * Replaces the optional node for the ElseBlock child. This is the {@code Opt} node containing the child ElseBlock, not the actual child!
   * @param opt The new node to be used as the optional node for the ElseBlock child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setElseBlockOpt(Opt<ElseBlock> opt) {
    setChild(opt, 1);
  }
  /**
   * Check whether the optional ElseBlock child exists.
   * @return {@code true} if the optional ElseBlock child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasElseBlock() {
    return getElseBlockOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) ElseBlock child.
   * @return The ElseBlock child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ElseBlock getElseBlock() {
    return (ElseBlock)getElseBlockOpt().getChild(0);
  }
  /**
   * Replaces the (optional) ElseBlock child.
   * @param node The new node to be used as the ElseBlock child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setElseBlock(ElseBlock node) {
    getElseBlockOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the ElseBlock child. This is the {@code Opt} node containing the child ElseBlock, not the actual child!
   * @return The optional node for child the ElseBlock child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<ElseBlock> getElseBlockOpt() {
    return (Opt<ElseBlock>)getChild(1);
  }
  /**
   * Retrieves the optional node for child ElseBlock. This is the {@code Opt} node containing the child ElseBlock, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child ElseBlock.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<ElseBlock> getElseBlockOptNoTransform() {
    return (Opt<ElseBlock>)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:639
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
        long id = nodeCounter();
        setuID(id);

		Element e = doc.createElement("IfStmt");
		e.setAttribute("id", Long.toString(id));

        for(IfBlock block : getIfBlocks()) {
            block.getXML(doc, e);
        }
        if(hasElseBlock()) {
            getElseBlock().getXML(doc, e);
        } 
        e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
        parent.appendChild(e);
        return true;
    }
  /**
   * @apilevel internal
   */
  protected java.util.Map getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited;
  /**
   * @attribute syn
   * @aspect AnalysisPrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AnalysisPrettyPrint.jrag:212
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getAnalysisPrettyPrintedLessComments(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
    java.util.List _parameters = new java.util.ArrayList(3);
    _parameters.add(analysis);
    _parameters.add(Boolean.valueOf(compact));
    _parameters.add(Boolean.valueOf(hideComments));
    if(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited == null) getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getAnalysisPrettyPrintedLessComments in class: ");
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value = getAnalysisPrettyPrintedLessComments_compute(analysis, compact, hideComments);
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.remove(_parameters);
    return getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value;
  }
  /**
   * @apilevel internal
   */
  private String getAnalysisPrettyPrintedLessComments_compute(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
        StringBuffer buf = new StringBuffer();
        String indent = getIndent();
        boolean first = true;
        for(IfBlock block : getIfBlocks()) {
            if(!first) {
                buf.append( indent + "else" );
            }
            buf.append(block.getAnalysisPrettyPrinted(analysis, compact, hideComments));
            first = false;
        }
        if(hasElseBlock()) {
            buf.append(getElseBlock().getAnalysisPrettyPrinted(analysis, compact, hideComments));
        }
        buf.append(indent + "end");
        Object in, out;
        in = analysis.getInFlowSets().get( this );
        out = analysis.getOutFlowSets().get( this );
        printFlowData(this,in,out,buf,compact, hideComments);
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getLValues_visited = -1;
  /**
   * @apilevel internal
   */
  protected boolean getLValues_computed = false;
  /**
   * @apilevel internal
   */
  protected java.util.Set<String> getLValues_value;
  /**
   * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:73
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<String> getLValues() {
    if(getLValues_computed) {
      return getLValues_value;
    }
      ASTNode$State state = state();
    if(getLValues_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getLValues in class: ");
    getLValues_visited = state().boundariesCrossed;
  int num = state.boundariesCrossed;
  boolean isFinal = this.is$Final();
    getLValues_value = getLValues_compute();
if(isFinal && num == state().boundariesCrossed) getLValues_computed = true;
    getLValues_visited = -1;
    return getLValues_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<String> getLValues_compute() {
        java.util.LinkedHashSet<String> names = new java.util.LinkedHashSet<String>();
        for( int i=0; i<getNumIfBlock(); i++ ){
            for( int j=0; j<getIfBlock(i).getNumStmt(); j++ ){
                names.addAll(getIfBlock(i).getStmt(j).getLValues());
            }
        }
        if( hasElseBlock() )
            for( int i=0; i<getElseBlock().getNumStmt(); i++ )
                names.addAll(getElseBlock().getStmt(i).getLValues());
        return names;
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:253
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        String indent = getIndent();
        boolean first = true;
        for(IfBlock block : getIfBlocks()) {
            if(!first) {
                buf.append( indent + "elseif " );
            }else {
            	buf.append( indent + "if " );
            }
            buf.append(block.getPrettyPrinted());
            first = false;
        }
        if(hasElseBlock()) {
            buf.append(getElseBlock().getPrettyPrinted());
        }
        buf.append(indent + "end");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:366
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        boolean first = true;
        for(IfBlock block : getIfBlocks()) {
            if(!first) {
                buf.append("else");
            }
            buf.append(block.getStructureString());
            first = false;
        }
        if(hasElseBlock()) {
            buf.append(getElseBlock().getStructureString());
        }
        buf.append("end");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
