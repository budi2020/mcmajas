/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production MatrixType : {@link Type} ::= <span class="component">ElementType:{@link BaseType}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/types.ast:31
 */
public class MatrixType extends Type implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public MatrixType clone() throws CloneNotSupportedException {
    MatrixType node = (MatrixType)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public MatrixType copy() {
      try {
        MatrixType node = (MatrixType)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public MatrixType fullCopy() {
    try {
      MatrixType tree = (MatrixType) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:478
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseMatrixType(this);
    }
  /**
   * @ast method 
   * 
   */
  public MatrixType() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public MatrixType(BaseType p0) {
    setChild(p0, 0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the ElementType child.
   * @param node The new node to replace the ElementType child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setElementType(BaseType node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the ElementType child.
   * @return The current node used as the ElementType child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public BaseType getElementType() {
    return (BaseType)getChild(0);
  }
  /**
   * Retrieves the ElementType child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the ElementType child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public BaseType getElementTypeNoTransform() {
    return (BaseType)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
