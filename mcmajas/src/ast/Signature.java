/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production Signature : {@link FunctionOrSignatureOrPropertyAccessOrStmt} ::= <span class="component">OutputParam:{@link Name}*</span> <span class="component">&lt;Name:String&gt;</span> <span class="component">InputParam:{@link Name}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:39
 */
public class Signature extends FunctionOrSignatureOrPropertyAccessOrStmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getPrettyPrintedLessComments_visited = -1;
    getIndent_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Signature clone() throws CloneNotSupportedException {
    Signature node = (Signature)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getIndent_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Signature copy() {
      try {
        Signature node = (Signature)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Signature fullCopy() {
    try {
      Signature tree = (Signature) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:335
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseSignature(this);
    }
  /**
   * @ast method 
   * 
   */
  public Signature() {
    super();

    setChild(new List(), 0);
    setChild(new List(), 1);

  }
  /**
   * @ast method 
   * 
   */
  public Signature(List<Name> p0, String p1, List<Name> p2) {
    setChild(p0, 0);
    setName(p1);
    setChild(p2, 1);
  }
  /**
   * @ast method 
   * 
   */
  public Signature(List<Name> p0, beaver.Symbol p1, List<Name> p2) {
    setChild(p0, 0);
    setName(p1);
    setChild(p2, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the OutputParam list.
   * @param list The new list node to be used as the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setOutputParamList(List<Name> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the OutputParam list.
   * @return Number of children in the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumOutputParam() {
    return getOutputParamList().getNumChild();
  }
  /**
   * Retrieves the number of children in the OutputParam list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the OutputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumOutputParamNoTransform() {
    return getOutputParamListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the OutputParam list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Name getOutputParam(int i) {
    return (Name)getOutputParamList().getChild(i);
  }
  /**
   * Append an element to the OutputParam list.
   * @param node The element to append to the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addOutputParam(Name node) {
    List<Name> list = (parent == null || state == null) ? getOutputParamListNoTransform() : getOutputParamList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addOutputParamNoTransform(Name node) {
    List<Name> list = getOutputParamListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the OutputParam list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setOutputParam(Name node, int i) {
    List<Name> list = getOutputParamList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the OutputParam list.
   * @return The node representing the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Name> getOutputParams() {
    return getOutputParamList();
  }
  /**
   * Retrieves the OutputParam list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the OutputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Name> getOutputParamsNoTransform() {
    return getOutputParamListNoTransform();
  }
  /**
   * Retrieves the OutputParam list.
   * @return The node representing the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Name> getOutputParamList() {
    List<Name> list = (List<Name>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the OutputParam list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the OutputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Name> getOutputParamListNoTransform() {
    return (List<Name>)getChildNoTransform(0);
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setName(String value) {
    tokenString_Name = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Name;
  /**
   * @ast method 
   * 
   */
  
  public int Namestart;
  /**
   * @ast method 
   * 
   */
  
  public int Nameend;
  /**
   * JastAdd-internal setter for lexeme Name using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setName(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setName is only valid for String lexemes");
    tokenString_Name = (String)symbol.value;
    Namestart = symbol.getStart();
    Nameend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
  /**
   * Replaces the InputParam list.
   * @param list The new list node to be used as the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setInputParamList(List<Name> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the InputParam list.
   * @return Number of children in the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumInputParam() {
    return getInputParamList().getNumChild();
  }
  /**
   * Retrieves the number of children in the InputParam list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the InputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumInputParamNoTransform() {
    return getInputParamListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the InputParam list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Name getInputParam(int i) {
    return (Name)getInputParamList().getChild(i);
  }
  /**
   * Append an element to the InputParam list.
   * @param node The element to append to the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addInputParam(Name node) {
    List<Name> list = (parent == null || state == null) ? getInputParamListNoTransform() : getInputParamList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addInputParamNoTransform(Name node) {
    List<Name> list = getInputParamListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the InputParam list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setInputParam(Name node, int i) {
    List<Name> list = getInputParamList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the InputParam list.
   * @return The node representing the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Name> getInputParams() {
    return getInputParamList();
  }
  /**
   * Retrieves the InputParam list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Name> getInputParamsNoTransform() {
    return getInputParamListNoTransform();
  }
  /**
   * Retrieves the InputParam list.
   * @return The node representing the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Name> getInputParamList() {
    List<Name> list = (List<Name>)getChild(1);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the InputParam list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Name> getInputParamListNoTransform() {
    return (List<Name>)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:315
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
        long id = nodeCounter();
        setuID(id);

       	Element e = doc.createElement("Signature");
		e.setAttribute("id", Long.toString(id));
		e.setAttribute("name", getName());

		Element e1 = doc.createElement("InputParamList");
        	for(Name param : getInputParams()) {
            	param.getXML(doc, e1);
        	}
		e.appendChild(e1);

		Element e2 = doc.createElement("OutputParamList");
        	for(Name param : getOutputParams()) {
            	param.getXML(doc, e2);
        	}
		e.appendChild(e2);

        e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
parent.appendChild(e);
        return true;
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:615
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append(getIndent() + "[");
        boolean first = true;
        for(Name param : getOutputParams()) {
            if(!first) {
                buf.append(", ");
            }
            buf.append(param.getPrettyPrinted());
            first = false;
        }
        buf.append("] = ");
        buf.append(getName());
        buf.append("(");
        first = true;
        for(Name param : getInputParams()) {
            if(!first) {
                buf.append(", ");
            }
            buf.append(param.getPrettyPrinted());
            first = false;
        }
        buf.append(")");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getIndent_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:792
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getIndent() {
      ASTNode$State state = state();
    if(getIndent_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getIndent in class: ");
    getIndent_visited = state().boundariesCrossed;
    String getIndent_value = getIndent_compute();
    getIndent_visited = -1;
    return getIndent_value;
  }
  /**
   * @apilevel internal
   */
  private String getIndent_compute() {
    	ASTNode parent = getParent();
    	if ( parent != null ){
        	return parent.getIndent() + ASTNode.INDENT_TAB;
        }
        return "" ;
    }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:194
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append("[");
        boolean first = true;
        for(Name param : getOutputParams()) {
            if(!first) {
                buf.append(", ");
            }
            buf.append(param.getStructureString());
            first = false;
        }
        buf.append("] = ");
        buf.append(getName());
        buf.append("(");
        first = true;
        for(Name param : getInputParams()) {
            if(!first) {
                buf.append(", ");
            }
            buf.append(param.getStructureString());
            first = false;
        }
        buf.append(")");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
