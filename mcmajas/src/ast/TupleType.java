/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production TupleType : {@link Type} ::= <span class="component">ElementType:{@link Type}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/types.ast:29
 */
public class TupleType extends Type implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public TupleType clone() throws CloneNotSupportedException {
    TupleType node = (TupleType)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public TupleType copy() {
      try {
        TupleType node = (TupleType)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public TupleType fullCopy() {
    try {
      TupleType tree = (TupleType) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:470
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseTupleType(this);
    }
  /**
   * @ast method 
   * 
   */
  public TupleType() {
    super();

    setChild(new List(), 0);

  }
  /**
   * @ast method 
   * 
   */
  public TupleType(List<Type> p0) {
    setChild(p0, 0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the ElementType list.
   * @param list The new list node to be used as the ElementType list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setElementTypeList(List<Type> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the ElementType list.
   * @return Number of children in the ElementType list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumElementType() {
    return getElementTypeList().getNumChild();
  }
  /**
   * Retrieves the number of children in the ElementType list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the ElementType list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumElementTypeNoTransform() {
    return getElementTypeListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the ElementType list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the ElementType list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Type getElementType(int i) {
    return (Type)getElementTypeList().getChild(i);
  }
  /**
   * Append an element to the ElementType list.
   * @param node The element to append to the ElementType list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addElementType(Type node) {
    List<Type> list = (parent == null || state == null) ? getElementTypeListNoTransform() : getElementTypeList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addElementTypeNoTransform(Type node) {
    List<Type> list = getElementTypeListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the ElementType list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setElementType(Type node, int i) {
    List<Type> list = getElementTypeList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the ElementType list.
   * @return The node representing the ElementType list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Type> getElementTypes() {
    return getElementTypeList();
  }
  /**
   * Retrieves the ElementType list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the ElementType list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Type> getElementTypesNoTransform() {
    return getElementTypeListNoTransform();
  }
  /**
   * Retrieves the ElementType list.
   * @return The node representing the ElementType list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Type> getElementTypeList() {
    List<Type> list = (List<Type>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the ElementType list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the ElementType list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Type> getElementTypeListNoTransform() {
    return (List<Type>)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
