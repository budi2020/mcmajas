/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production NotExpr : {@link UnaryExpr};
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:103
 */
public class NotExpr extends UnaryExpr implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getOperatorName_visited = -1;
    getPrettyPrintedLessComments_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public NotExpr clone() throws CloneNotSupportedException {
    NotExpr node = (NotExpr)super.clone();
    node.getOperatorName_visited = -1;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public NotExpr copy() {
      try {
        NotExpr node = (NotExpr)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public NotExpr fullCopy() {
    try {
      NotExpr tree = (NotExpr) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:173
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseNotExpr(this);
    }
  /**
   * @ast method 
   * 
   */
  public NotExpr() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public NotExpr(Expr p0) {
    setChild(p0, 0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Operand child.
   * @param node The new node to replace the Operand child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setOperand(Expr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Operand child.
   * @return The current node used as the Operand child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Expr getOperand() {
    return (Expr)getChild(0);
  }
  /**
   * Retrieves the Operand child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Operand child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Expr getOperandNoTransform() {
    return (Expr)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  protected int getOperatorName_visited = -1;
  /**
   * @attribute syn
   * @aspect OperatorNames
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/OperatorNames.jrag:61
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getOperatorName() {
      ASTNode$State state = state();
    if(getOperatorName_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getOperatorName in class: ");
    getOperatorName_visited = state().boundariesCrossed;
    String getOperatorName_value = getOperatorName_compute();
    getOperatorName_visited = -1;
    return getOperatorName_value;
  }
  /**
   * @apilevel internal
   */
  private String getOperatorName_compute() { return "not"; }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:484
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {  return "(~" + getOperand().getPrettyPrinted() + ")";  }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:591
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {  return "(~" + getOperand().getStructureString() + ")";  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
