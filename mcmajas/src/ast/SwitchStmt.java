/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production SwitchStmt : {@link Stmt} ::= <span class="component">{@link Expr}</span> <span class="component">{@link SwitchCaseBlock}*</span> <span class="component">[{@link DefaultCaseBlock}]</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:70
 */
public class SwitchStmt extends Stmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    getLValues_visited = -1;
    getLValues_computed = false;
    getLValues_value = null;
    getPrettyPrintedLessComments_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public SwitchStmt clone() throws CloneNotSupportedException {
    SwitchStmt node = (SwitchStmt)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    node.getLValues_visited = -1;
    node.getLValues_computed = false;
    node.getLValues_value = null;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public SwitchStmt copy() {
      try {
        SwitchStmt node = (SwitchStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public SwitchStmt fullCopy() {
    try {
      SwitchStmt tree = (SwitchStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:72
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseSwitchStmt(this);
    }
  /**
   * @ast method 
   * 
   */
  public SwitchStmt() {
    super();

    setChild(new List(), 1);
    setChild(new Opt(), 2);

  }
  /**
   * @ast method 
   * 
   */
  public SwitchStmt(Expr p0, List<SwitchCaseBlock> p1, Opt<DefaultCaseBlock> p2) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 3;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Expr child.
   * @param node The new node to replace the Expr child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setExpr(Expr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Expr child.
   * @return The current node used as the Expr child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Expr getExpr() {
    return (Expr)getChild(0);
  }
  /**
   * Retrieves the Expr child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Expr child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Expr getExprNoTransform() {
    return (Expr)getChildNoTransform(0);
  }
  /**
   * Replaces the SwitchCaseBlock list.
   * @param list The new list node to be used as the SwitchCaseBlock list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setSwitchCaseBlockList(List<SwitchCaseBlock> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the SwitchCaseBlock list.
   * @return Number of children in the SwitchCaseBlock list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumSwitchCaseBlock() {
    return getSwitchCaseBlockList().getNumChild();
  }
  /**
   * Retrieves the number of children in the SwitchCaseBlock list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the SwitchCaseBlock list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumSwitchCaseBlockNoTransform() {
    return getSwitchCaseBlockListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the SwitchCaseBlock list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the SwitchCaseBlock list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public SwitchCaseBlock getSwitchCaseBlock(int i) {
    return (SwitchCaseBlock)getSwitchCaseBlockList().getChild(i);
  }
  /**
   * Append an element to the SwitchCaseBlock list.
   * @param node The element to append to the SwitchCaseBlock list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addSwitchCaseBlock(SwitchCaseBlock node) {
    List<SwitchCaseBlock> list = (parent == null || state == null) ? getSwitchCaseBlockListNoTransform() : getSwitchCaseBlockList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addSwitchCaseBlockNoTransform(SwitchCaseBlock node) {
    List<SwitchCaseBlock> list = getSwitchCaseBlockListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the SwitchCaseBlock list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setSwitchCaseBlock(SwitchCaseBlock node, int i) {
    List<SwitchCaseBlock> list = getSwitchCaseBlockList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the SwitchCaseBlock list.
   * @return The node representing the SwitchCaseBlock list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<SwitchCaseBlock> getSwitchCaseBlocks() {
    return getSwitchCaseBlockList();
  }
  /**
   * Retrieves the SwitchCaseBlock list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SwitchCaseBlock list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<SwitchCaseBlock> getSwitchCaseBlocksNoTransform() {
    return getSwitchCaseBlockListNoTransform();
  }
  /**
   * Retrieves the SwitchCaseBlock list.
   * @return The node representing the SwitchCaseBlock list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<SwitchCaseBlock> getSwitchCaseBlockList() {
    List<SwitchCaseBlock> list = (List<SwitchCaseBlock>)getChild(1);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the SwitchCaseBlock list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SwitchCaseBlock list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<SwitchCaseBlock> getSwitchCaseBlockListNoTransform() {
    return (List<SwitchCaseBlock>)getChildNoTransform(1);
  }
  /**
   * Replaces the optional node for the DefaultCaseBlock child. This is the {@code Opt} node containing the child DefaultCaseBlock, not the actual child!
   * @param opt The new node to be used as the optional node for the DefaultCaseBlock child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setDefaultCaseBlockOpt(Opt<DefaultCaseBlock> opt) {
    setChild(opt, 2);
  }
  /**
   * Check whether the optional DefaultCaseBlock child exists.
   * @return {@code true} if the optional DefaultCaseBlock child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasDefaultCaseBlock() {
    return getDefaultCaseBlockOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) DefaultCaseBlock child.
   * @return The DefaultCaseBlock child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public DefaultCaseBlock getDefaultCaseBlock() {
    return (DefaultCaseBlock)getDefaultCaseBlockOpt().getChild(0);
  }
  /**
   * Replaces the (optional) DefaultCaseBlock child.
   * @param node The new node to be used as the DefaultCaseBlock child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setDefaultCaseBlock(DefaultCaseBlock node) {
    getDefaultCaseBlockOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the DefaultCaseBlock child. This is the {@code Opt} node containing the child DefaultCaseBlock, not the actual child!
   * @return The optional node for child the DefaultCaseBlock child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<DefaultCaseBlock> getDefaultCaseBlockOpt() {
    return (Opt<DefaultCaseBlock>)getChild(2);
  }
  /**
   * Retrieves the optional node for child DefaultCaseBlock. This is the {@code Opt} node containing the child DefaultCaseBlock, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child DefaultCaseBlock.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<DefaultCaseBlock> getDefaultCaseBlockOptNoTransform() {
    return (Opt<DefaultCaseBlock>)getChildNoTransform(2);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:695
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
        long id = nodeCounter();
        setuID(id);

        Element e = doc.createElement("SwitchStmt");
		e.setAttribute("id", Long.toString(id));

        getExpr().getXML(doc, e);

        for(SwitchCaseBlock scase : getSwitchCaseBlocks()) {
            scase.getXML(doc, e);
        }
        if(hasDefaultCaseBlock()) {
            getDefaultCaseBlock().getXML(doc, e);
        } 
    
		e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
parent.appendChild(e);
        return true;
    }
  /**
   * @apilevel internal
   */
  protected java.util.Map getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited;
  /**
   * @attribute syn
   * @aspect AnalysisPrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AnalysisPrettyPrint.jrag:260
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getAnalysisPrettyPrintedLessComments(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
    java.util.List _parameters = new java.util.ArrayList(3);
    _parameters.add(analysis);
    _parameters.add(Boolean.valueOf(compact));
    _parameters.add(Boolean.valueOf(hideComments));
    if(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited == null) getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getAnalysisPrettyPrintedLessComments in class: ");
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value = getAnalysisPrettyPrintedLessComments_compute(analysis, compact, hideComments);
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.remove(_parameters);
    return getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value;
  }
  /**
   * @apilevel internal
   */
  private String getAnalysisPrettyPrintedLessComments_compute(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
        StringBuffer buf = new StringBuffer();
        String indent = getIndent();
        buf.append(indent + "switch ");
        buf.append(getExpr().getAnalysisPrettyPrinted(analysis, compact, hideComments));
        buf.append('\n');
        for(SwitchCaseBlock scase : getSwitchCaseBlocks()) {
            buf.append(scase.getAnalysisPrettyPrinted(analysis, compact, hideComments));
        }
        if(hasDefaultCaseBlock()) {
            buf.append(getDefaultCaseBlock().getAnalysisPrettyPrinted(analysis, compact, hideComments));
        }
        buf.append( indent + "end");
        Object in, out;
        in = analysis.getInFlowSets().get( this );
        out = analysis.getOutFlowSets().get( this );
        printFlowData(this,in,out,buf,compact, hideComments);
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getLValues_visited = -1;
  /**
   * @apilevel internal
   */
  protected boolean getLValues_computed = false;
  /**
   * @apilevel internal
   */
  protected java.util.Set<String> getLValues_value;
  /**
   * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:95
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<String> getLValues() {
    if(getLValues_computed) {
      return getLValues_value;
    }
      ASTNode$State state = state();
    if(getLValues_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getLValues in class: ");
    getLValues_visited = state().boundariesCrossed;
  int num = state.boundariesCrossed;
  boolean isFinal = this.is$Final();
    getLValues_value = getLValues_compute();
if(isFinal && num == state().boundariesCrossed) getLValues_computed = true;
    getLValues_visited = -1;
    return getLValues_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<String> getLValues_compute() {
        java.util.LinkedHashSet<String> names = new java.util.LinkedHashSet<String>();
        for( int i=0; i<getNumSwitchCaseBlock(); i++){
            SwitchCaseBlock b = getSwitchCaseBlock(i);
            for( int j=0; j<b.getNumStmt(); j++ )
                names.addAll( b.getStmt(j).getLValues() );
        }
        if( hasDefaultCaseBlock() ){
            DefaultCaseBlock b = getDefaultCaseBlock();
            for( int i=0; i< b.getNumStmt(); i++ )
                names.addAll( b.getStmt(i).getLValues() );
        }
        return names;
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:299
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        String indent = getIndent();
        buf.append(indent + "switch ");
        buf.append(getExpr().getPrettyPrinted());
        buf.append('\n');
        for(SwitchCaseBlock scase : getSwitchCaseBlocks()) {
            buf.append(scase.getPrettyPrinted());
        }
        if(hasDefaultCaseBlock()) {
            buf.append(getDefaultCaseBlock().getPrettyPrinted());
        }
        buf.append( indent + "end");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:409
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append("switch ");
        buf.append(getExpr().getStructureString());
        buf.append('\n');
        for(SwitchCaseBlock scase : getSwitchCaseBlocks()) {
            buf.append(scase.getStructureString());
        }
        if(hasDefaultCaseBlock()) {
            buf.append(getDefaultCaseBlock().getStructureString());
        }
        buf.append("end");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
