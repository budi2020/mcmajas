/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;

/**
 * @production CompilationUnits : {@link ASTNode} ::= <span class="component">{@link Program}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:23
 */
public class CompilationUnits extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    makeUId_visited = -1;
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    getPrettyPrintedLessComments_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public CompilationUnits clone() throws CloneNotSupportedException {
    CompilationUnits node = (CompilationUnits)super.clone();
    node.makeUId_visited = -1;
    node.getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public CompilationUnits copy() {
      try {
        CompilationUnits node = (CompilationUnits)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public CompilationUnits fullCopy() {
    try {
      CompilationUnits tree = (CompilationUnits) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:62
   */
  
    
	public long nodeCounter = 0;
  /**
   * @ast method 
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:78
   */
  public Document ASTtoXML(boolean includeKinds) {
        boolean oldIncludeKinds = include_kinds_in_xml;
        include_kinds_in_xml = includeKinds;
        Document result = ASTtoXML();
        include_kinds_in_xml = oldIncludeKinds;
        return result;
    }
  /**
   * @ast method 
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:86
   */
  public Document ASTtoXML() {
		try{
	        DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
	        DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
	        Document doc = docBuilder.newDocument();

            if (include_kinds_in_xml) {
                kind_analysis_sensitive=new VFFlowSensitiveAnalysis(this);
                kind_analysis_sensitive.analyze();

                kind_analysis_insensitive=new VFFlowInsensitiveAnalysis(this);
                kind_analysis_insensitive.analyze();

                kind_analysis_preorder=new VFPreorderAnalysis(this);
                kind_analysis_preorder.analyze();
            }

            parentProgram=null;
	        long id = nodeCounter();
			setuID(id);
			
			Element e = doc.createElement("CompilationUnits");
			e.setAttribute("id", Long.toString(id));
			e.setAttribute("name", "name"); 
			for(Program p : getPrograms()){
		    	p.getXML(doc, e);
			}
		    doc.appendChild(e);
		
            parentProgram=null;
            kind_analysis_sensitive=null;
            kind_analysis_insensitive=null;
            kind_analysis_preorder=null;
            	
	        return doc;
	        }catch (Exception e){
	           throw new RuntimeException(e);
	        }
	}
  /**
   * @ast method 
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:126
   */
  public String XMLtoString(Document doc) {
    	try {
	    	TransformerFactory transfac = TransformerFactory.newInstance();
	    	transfac.setAttribute("indent-number", new Integer(4));
	        Transformer trans = transfac.newTransformer();
	        trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
	        trans.setOutputProperty(OutputKeys.INDENT, "yes");
			//trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			
	        StringWriter sw = new StringWriter();
	        StreamResult result = new StreamResult(sw);
	        DOMSource source = new DOMSource(doc);
	        trans.transform(source, result);
	        
	        return sw.toString();
	        
            }catch (Exception e){
               throw new RuntimeException(e);
            }
        
    }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:299
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseCompilationUnits(this);
    }
  /**
   * @ast method 
   * @aspect NameResolutionAdditions
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jadd:25
   */
  
    
    protected String name="";
  /**
   * @ast method 
   * @aspect NameResolutionAdditions
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jadd:26
   */
  public String getName(){return name;}
  /**
   * @ast method 
   * @aspect NameResolutionAdditions
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jadd:27
   */
  public void setName(String name){this.name=name;}
  /**
   * @ast method 
   * @aspect NameResolutionAdditions
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jadd:29
   */
  
    
    protected GenericFile rootFolder=null;
  /**
   * @ast method 
   * @aspect NameResolutionAdditions
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jadd:30
   */
  public GenericFile getRootFolder(){
	return rootFolder;
    }
  /**
   * @ast method 
   * @aspect NameResolutionAdditions
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jadd:33
   */
  public void setRootFolder(GenericFile root){
	rootFolder = root;
    }
  /**
   * @ast method 
   * 
   */
  public CompilationUnits() {
    super();

    setChild(new List(), 0);
    is$Final(true);

  }
  /**
   * @ast method 
   * 
   */
  public CompilationUnits(List<Program> p0) {
    setChild(p0, 0);
    is$Final(true);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Program list.
   * @param list The new list node to be used as the Program list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setProgramList(List<Program> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Program list.
   * @return Number of children in the Program list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumProgram() {
    return getProgramList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Program list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Program list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumProgramNoTransform() {
    return getProgramListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Program list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Program list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Program getProgram(int i) {
    return (Program)getProgramList().getChild(i);
  }
  /**
   * Append an element to the Program list.
   * @param node The element to append to the Program list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addProgram(Program node) {
    List<Program> list = (parent == null || state == null) ? getProgramListNoTransform() : getProgramList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addProgramNoTransform(Program node) {
    List<Program> list = getProgramListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Program list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setProgram(Program node, int i) {
    List<Program> list = getProgramList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Program list.
   * @return The node representing the Program list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Program> getPrograms() {
    return getProgramList();
  }
  /**
   * Retrieves the Program list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Program list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Program> getProgramsNoTransform() {
    return getProgramListNoTransform();
  }
  /**
   * Retrieves the Program list.
   * @return The node representing the Program list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Program> getProgramList() {
    List<Program> list = (List<Program>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Program list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Program list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Program> getProgramListNoTransform() {
    return (List<Program>)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  protected int makeUId_visited = -1;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:55
   */
  @SuppressWarnings({"unchecked", "cast"})
  public long makeUId() {
      ASTNode$State state = state();
    if(makeUId_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: makeUId in class: ");
    makeUId_visited = state().boundariesCrossed;
    long makeUId_value = makeUId_compute();
    makeUId_visited = -1;
    return makeUId_value;
  }
  /**
   * @apilevel internal
   */
  private long makeUId_compute() {  return 0;  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited;
  /**
   * @attribute syn
   * @aspect AnalysisPrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AnalysisPrettyPrint.jrag:56
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getAnalysisPrettyPrintedLessComments(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
    java.util.List _parameters = new java.util.ArrayList(3);
    _parameters.add(analysis);
    _parameters.add(Boolean.valueOf(compact));
    _parameters.add(Boolean.valueOf(hideComments));
    if(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited == null) getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getAnalysisPrettyPrintedLessComments in class: ");
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value = getAnalysisPrettyPrintedLessComments_compute(analysis, compact, hideComments);
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.remove(_parameters);
    return getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value;
  }
  /**
   * @apilevel internal
   */
  private String getAnalysisPrettyPrintedLessComments_compute(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
        StringBuffer buf = new StringBuffer();

        boolean first = true;
        for( Program p : getPrograms() ){
            if( first )
                first = false;
            else
                buf.append("\n\n");
            
            buf.append( p.getAnalysisPrettyPrinted(analysis, compact, hideComments) );
        }
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:88
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {
        StringBuffer buf = new StringBuffer();

        boolean first = true;
        for( Program p : getPrograms() ){
            if( first )
                first = false;
            else
                buf.append("\n==========================\n\n");
            
            buf.append( p.getPrettyPrinted() );
        }
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:22
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        for(Program prog : getPrograms()){
            buf.append(prog.getStructureString());
            buf.append('\n');
        }
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
