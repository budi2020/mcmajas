/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production RangeExpr : {@link Expr} ::= <span class="component">Lower:{@link Expr}</span> <span class="component">[Incr:{@link Expr}]</span> <span class="component">Upper:{@link Expr}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:79
 */
public class RangeExpr extends Expr implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getPrettyPrintedLessComments_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public RangeExpr clone() throws CloneNotSupportedException {
    RangeExpr node = (RangeExpr)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public RangeExpr copy() {
      try {
        RangeExpr node = (RangeExpr)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public RangeExpr fullCopy() {
    try {
      RangeExpr tree = (RangeExpr) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:109
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseRangeExpr(this);
    }
  /**
   * @ast method 
   * 
   */
  public RangeExpr() {
    super();

    setChild(new Opt(), 1);

  }
  /**
   * @ast method 
   * 
   */
  public RangeExpr(Expr p0, Opt<Expr> p1, Expr p2) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 3;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Lower child.
   * @param node The new node to replace the Lower child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setLower(Expr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Lower child.
   * @return The current node used as the Lower child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Expr getLower() {
    return (Expr)getChild(0);
  }
  /**
   * Retrieves the Lower child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Lower child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Expr getLowerNoTransform() {
    return (Expr)getChildNoTransform(0);
  }
  /**
   * Replaces the optional node for the Incr child. This is the {@code Opt} node containing the child Incr, not the actual child!
   * @param opt The new node to be used as the optional node for the Incr child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setIncrOpt(Opt<Expr> opt) {
    setChild(opt, 1);
  }
  /**
   * Check whether the optional Incr child exists.
   * @return {@code true} if the optional Incr child exists, {@code false} if it does not.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean hasIncr() {
    return getIncrOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) Incr child.
   * @return The Incr child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Expr getIncr() {
    return (Expr)getIncrOpt().getChild(0);
  }
  /**
   * Replaces the (optional) Incr child.
   * @param node The new node to be used as the Incr child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setIncr(Expr node) {
    getIncrOpt().setChild(node, 0);
  }
  /**
   * Retrieves the optional node for the Incr child. This is the {@code Opt} node containing the child Incr, not the actual child!
   * @return The optional node for child the Incr child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<Expr> getIncrOpt() {
    return (Opt<Expr>)getChild(1);
  }
  /**
   * Retrieves the optional node for child Incr. This is the {@code Opt} node containing the child Incr, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child Incr.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Opt<Expr> getIncrOptNoTransform() {
    return (Opt<Expr>)getChildNoTransform(1);
  }
  /**
   * Replaces the Upper child.
   * @param node The new node to replace the Upper child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setUpper(Expr node) {
    setChild(node, 2);
  }
  /**
   * Retrieves the Upper child.
   * @return The current node used as the Upper child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Expr getUpper() {
    return (Expr)getChild(2);
  }
  /**
   * Retrieves the Upper child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Upper child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Expr getUpperNoTransform() {
    return (Expr)getChildNoTransform(2);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:780
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
        long id = nodeCounter();
        setuID(id);
		
		Element e = doc.createElement("RangeExpr");
		e.setAttribute("id", Long.toString(id));	
        getLower().getXML(doc, e);

        if(hasIncr()) {
            getIncr().getXML(doc, e);
        }
        
       getUpper().getXML(doc, e);

        e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
parent.appendChild(e);
        return true;
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:363
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append('(');
        buf.append(getLower().getPrettyPrinted());
        buf.append(" : ");
        if(hasIncr()) {
            buf.append(getIncr().getPrettyPrinted());
            buf.append(" : ");
        }
        buf.append(getUpper().getPrettyPrinted());
        buf.append(')');
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:473
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append('(');
        buf.append(getLower().getStructureString());
        buf.append(" : ");
        if(hasIncr()) {
            buf.append(getIncr().getStructureString());
            buf.append(" : ");
        }
        buf.append(getUpper().getStructureString());
        buf.append(')');
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
