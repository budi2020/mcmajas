/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production NameExpr : {@link LValueExpr} ::= <span class="component">{@link Name}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:84
 */
public class NameExpr extends LValueExpr implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    annotateXMLWithKind_Element_String_VFFlowset_String_visited = null;
    getXML_Document_Element_visited = null;
    getSymbols_visited = -1;
    getNameExpressions_visited = -1;
    getAllNameExpressions_visited = -1;
    getPrettyPrintedLessComments_visited = -1;
    dumpString_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public NameExpr clone() throws CloneNotSupportedException {
    NameExpr node = (NameExpr)super.clone();
    node.annotateXMLWithKind_Element_String_VFFlowset_String_visited = null;
    node.getXML_Document_Element_visited = null;
    node.getSymbols_visited = -1;
    node.getNameExpressions_visited = -1;
    node.getAllNameExpressions_visited = -1;
    node.getPrettyPrintedLessComments_visited = -1;
    node.dumpString_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public NameExpr copy() {
      try {
        NameExpr node = (NameExpr)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public NameExpr fullCopy() {
    try {
      NameExpr tree = (NameExpr) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:121
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseNameExpr(this);
    }
  /**
   * @ast method 
   * @aspect Temporaries
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/Temporaries.jadd:27
   */
  
    public boolean tmpVar = false;
  /**
   * @ast method 
   * @aspect VarName
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/VarName.jadd:10
   */
  public String getVarName() {
    return getName().getVarName();
  }
  /**
   * @ast method 
   * 
   */
  public NameExpr() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public NameExpr(Name p0) {
    setChild(p0, 0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Name child.
   * @param node The new node to replace the Name child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setName(Name node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Name child.
   * @return The current node used as the Name child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Name getName() {
    return (Name)getChild(0);
  }
  /**
   * Retrieves the Name child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Name child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Name getNameNoTransform() {
    return (Name)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map annotateXMLWithKind_Element_String_VFFlowset_String_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:836
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean annotateXMLWithKind(Element e, String name, VFFlowset f, String n) {
    java.util.List _parameters = new java.util.ArrayList(4);
    _parameters.add(e);
    _parameters.add(name);
    _parameters.add(f);
    _parameters.add(n);
    if(annotateXMLWithKind_Element_String_VFFlowset_String_visited == null) annotateXMLWithKind_Element_String_VFFlowset_String_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(annotateXMLWithKind_Element_String_VFFlowset_String_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: annotateXMLWithKind in class: ");
    annotateXMLWithKind_Element_String_VFFlowset_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean annotateXMLWithKind_Element_String_VFFlowset_String_value = annotateXMLWithKind_compute(e, name, f, n);
    annotateXMLWithKind_Element_String_VFFlowset_String_visited.remove(_parameters);
    return annotateXMLWithKind_Element_String_VFFlowset_String_value;
  }
  /**
   * @apilevel internal
   */
  private boolean annotateXMLWithKind_compute(Element e, String name, VFFlowset f, String n) {
        VFDatum d = null;
        if ( f!=null )
            d = f.contains(n);


        String kind="null";
        if (d!=null){
            kind=d.toString();        
        }        
        e.setAttribute(name, kind);
        return true;
    }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:850
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
        long id = nodeCounter();
        setuID(id);
		Name t = getName();
		String nameid = getName().getID();
		Element e = doc.createElement("NameExpr");
		e.setAttribute("id", Long.toString(id));
        getName().getXML(doc, e);
        e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
        parent.appendChild(e);
        if (include_kinds_in_xml) {
            VFFlowset out_sensitive=null;
            if (parentProgram!=null)
                out_sensitive = kind_analysis_sensitive.getOutFlowSets().get( parentProgram );
            else
                out_sensitive = kind_analysis_sensitive.getOutFlowSets().get( t );


            VFFlowset out_insensitive=null;
            if (parentProgram!=null)
                out_insensitive = kind_analysis_insensitive.getFlowSets().get( parentProgram );
            else
                out_insensitive = kind_analysis_insensitive.getFlowSets().get( t );

            VFFlowset out_preorder = kind_analysis_preorder.getFlowSets().get(t );  
            annotateXMLWithKind(e, "kind", out_preorder, nameid);
            annotateXMLWithKind(e, "kind_insensitive", out_insensitive, nameid);
            annotateXMLWithKind(e, "kind_sensitive", out_sensitive, nameid);
        }
        return true;
    }
  /**
   * @apilevel internal
   */
  protected int getSymbols_visited = -1;
  /**
   * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:134
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<String> getSymbols() {
      ASTNode$State state = state();
    if(getSymbols_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getSymbols in class: ");
    getSymbols_visited = state().boundariesCrossed;
    java.util.Set<String> getSymbols_value = getSymbols_compute();
    getSymbols_visited = -1;
    return getSymbols_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<String> getSymbols_compute() {  return getName().getSymbols();  }
  /**
   * @apilevel internal
   */
  protected int getNameExpressions_visited = -1;
  /**
   * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:165
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<NameExpr> getNameExpressions() {
      ASTNode$State state = state();
    if(getNameExpressions_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getNameExpressions in class: ");
    getNameExpressions_visited = state().boundariesCrossed;
    java.util.Set<NameExpr> getNameExpressions_value = getNameExpressions_compute();
    getNameExpressions_visited = -1;
    return getNameExpressions_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<NameExpr> getNameExpressions_compute() {	
        java.util.LinkedHashSet<NameExpr> names = new java.util.LinkedHashSet<NameExpr>();
        names.add( this );
        return names;
    }
  /**
   * @apilevel internal
   */
  protected int getAllNameExpressions_visited = -1;
  /**
     * returns all children that are names
     * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:196
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<NameExpr> getAllNameExpressions() {
      ASTNode$State state = state();
    if(getAllNameExpressions_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getAllNameExpressions in class: ");
    getAllNameExpressions_visited = state().boundariesCrossed;
    java.util.Set<NameExpr> getAllNameExpressions_value = getAllNameExpressions_compute();
    getAllNameExpressions_visited = -1;
    return getAllNameExpressions_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<NameExpr> getAllNameExpressions_compute() {   
        java.util.LinkedHashSet<NameExpr> names = new java.util.LinkedHashSet<NameExpr>();
        names.add( this );
        return names;
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:383
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {  return getName().getPrettyPrinted();  }
  /**
   * @apilevel internal
   */
  protected int dumpString_visited = -1;
  /**
   * @attribute syn
   * @aspect ASTPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/ASTPrint.jadd:57
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String dumpString() {
      ASTNode$State state = state();
    if(dumpString_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: dumpString in class: ");
    dumpString_visited = state().boundariesCrossed;
    String dumpString_value = dumpString_compute();
    dumpString_visited = -1;
    return dumpString_value;
  }
  /**
   * @apilevel internal
   */
  private String dumpString_compute() {  return getClass().getName() + " [" + getName().getID() + "]";  }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:492
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {  return getName().getStructureString();  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
