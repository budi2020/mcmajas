/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production Expr : {@link ASTNode};
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:78
 */
public abstract class Expr extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getSymbols_visited = -1;
    getNameExpressions_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Expr clone() throws CloneNotSupportedException {
    Expr node = (Expr)super.clone();
    node.getSymbols_visited = -1;
    node.getNameExpressions_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:395
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseExpr(this);
    }
  /**
   * @ast method 
   * 
   */
  public Expr() {
    super();


  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * @apilevel internal
   */
  protected int getSymbols_visited = -1;
  /**
   * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:118
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<String> getSymbols() {
      ASTNode$State state = state();
    if(getSymbols_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getSymbols in class: ");
    getSymbols_visited = state().boundariesCrossed;
    java.util.Set<String> getSymbols_value = getSymbols_compute();
    getSymbols_visited = -1;
    return getSymbols_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<String> getSymbols_compute() {
        java.util.Set<String> symbols = new java.util.LinkedHashSet<String>();
        for( int i = 0; i<getNumChild(); i++ )
            symbols.addAll( getChild(i).getSymbols() );
        return symbols;
    }
  /**
   * @apilevel internal
   */
  protected int getNameExpressions_visited = -1;
  /**
   * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:156
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<NameExpr> getNameExpressions() {
      ASTNode$State state = state();
    if(getNameExpressions_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getNameExpressions in class: ");
    getNameExpressions_visited = state().boundariesCrossed;
    java.util.Set<NameExpr> getNameExpressions_value = getNameExpressions_compute();
    getNameExpressions_visited = -1;
    return getNameExpressions_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<NameExpr> getNameExpressions_compute() {
        java.util.Set<NameExpr> symbols = new java.util.LinkedHashSet<NameExpr>();
        for( int i = 0; i<getNumChild(); i++ )
            symbols.addAll( getChild(i).getNameExpressions() );
        return symbols;
    }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
