/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production Stmt : {@link FunctionOrSignatureOrPropertyAccessOrStmt};
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:52
 */
public abstract class Stmt extends FunctionOrSignatureOrPropertyAccessOrStmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    getLValues_visited = -1;
    getLValues_computed = false;
    getLValues_value = null;
    getPrettyPrinted_visited = -1;
    getIndent_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Stmt clone() throws CloneNotSupportedException {
    Stmt node = (Stmt)super.clone();
    node.getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    node.getLValues_visited = -1;
    node.getLValues_computed = false;
    node.getLValues_value = null;
    node.getPrettyPrinted_visited = -1;
    node.getIndent_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:35
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseStmt(this);
    }
  /**
   * @ast method 
   * @aspect OutputSuppression
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/OutputSuppression.jadd:26
   */
  
    private boolean outputSuppressed = defaultOutputSuppression;
  /**
   * @ast method 
   * @aspect OutputSuppression
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/OutputSuppression.jadd:27
   */
  
    private static boolean defaultOutputSuppression = false;
  /**
     * Sets the default output suppression for Statements. When Stmts get created,
     * this will be the value assigned to them
     * - in order to parse properly, the value has to be set to false, which is the initial value
     * - for rewrites etc., it makes more sense to always suppress otuput for created nodes
     * 
     * @ast method 
   * @aspect OutputSuppression
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/OutputSuppression.jadd:36
   */
  public static void setDefaultOutputSuppression(boolean suppressOutput){
        defaultOutputSuppression = suppressOutput;
    }
  /**
   * @ast method 
   * @aspect OutputSuppression
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/OutputSuppression.jadd:39
   */
  public static boolean getDefaultOutputSuppression(){ return defaultOutputSuppression; }
  /**
   * @ast method 
   * @aspect OutputSuppression
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/OutputSuppression.jadd:41
   */
  public boolean isOutputSuppressed() { return outputSuppressed; }
  /**
   * @ast method 
   * @aspect OutputSuppression
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/OutputSuppression.jadd:42
   */
  public void setOutputSuppressed(boolean outputSuppressed) { 
        this.outputSuppressed = outputSuppressed; 
    }
  /**
   * @ast method 
   * 
   */
  public Stmt() {
    super();


  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited;
  /**
   * @attribute syn
   * @aspect AnalysisPrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AnalysisPrettyPrint.jrag:159
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getAnalysisPrettyPrintedLessComments(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
    java.util.List _parameters = new java.util.ArrayList(3);
    _parameters.add(analysis);
    _parameters.add(Boolean.valueOf(compact));
    _parameters.add(Boolean.valueOf(hideComments));
    if(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited == null) getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getAnalysisPrettyPrintedLessComments in class: ");
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value = getAnalysisPrettyPrintedLessComments_compute(analysis, compact, hideComments);
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.remove(_parameters);
    return getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value;
  }
  /**
   * @apilevel internal
   */
  private String getAnalysisPrettyPrintedLessComments_compute(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
        StringBuffer buf = new StringBuffer();
        String indent = getIndent();
        buf.append( getPrettyPrintedLessComments() );
        Object in, out;
        in = analysis.getInFlowSets().get( this );
        out = analysis.getOutFlowSets().get( this );
        printFlowData(this,in,out,buf,compact, hideComments); //last '\n' was originally not printed
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getLValues_visited = -1;
  /**
   * @apilevel internal
   */
  protected boolean getLValues_computed = false;
  /**
   * @apilevel internal
   */
  protected java.util.Set<String> getLValues_value;
  /**
   * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:37
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<String> getLValues() {
    if(getLValues_computed) {
      return getLValues_value;
    }
      ASTNode$State state = state();
    if(getLValues_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getLValues in class: ");
    getLValues_visited = state().boundariesCrossed;
  int num = state.boundariesCrossed;
  boolean isFinal = this.is$Final();
    getLValues_value = getLValues_compute();
if(isFinal && num == state().boundariesCrossed) getLValues_computed = true;
    getLValues_visited = -1;
    return getLValues_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<String> getLValues_compute() {  return new java.util.LinkedHashSet<String>();  }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrinted_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:36
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrinted() {
      ASTNode$State state = state();
    if(getPrettyPrinted_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrinted in class: ");
    getPrettyPrinted_visited = state().boundariesCrossed;
    String getPrettyPrinted_value = getPrettyPrinted_compute();
    getPrettyPrinted_visited = -1;
    return getPrettyPrinted_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrinted_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append(getPrettyPrintedLessComments());
        if (this instanceof EmptyStmt) buf.append(getIndent());        
        //first comment - spaces between stmt and comment - EmptyStmt get printed without extra spaces
        String space = (this instanceof EmptyStmt)?"":"  ";
        for(beaver.Symbol comment : getComments()) {
            buf.append(space + comment.value);
            space = "\n"+getIndent(); //later comments -- full indent
        }
        return buf.toString();            
    }
  /**
   * @apilevel internal
   */
  protected int getIndent_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:720
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getIndent() {
      ASTNode$State state = state();
    if(getIndent_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getIndent in class: ");
    getIndent_visited = state().boundariesCrossed;
    String getIndent_value = getIndent_compute();
    getIndent_visited = -1;
    return getIndent_value;
  }
  /**
   * @apilevel internal
   */
  private String getIndent_compute() {
    	ASTNode parent = getParent();
    	if ( parent != null ){
        	ASTNode gparent = parent.getParent();
    	    return (gparent instanceof Program) ? "" : parent.getIndent() + ASTNode.INDENT_TAB;
        }
        return "" ;
    }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
