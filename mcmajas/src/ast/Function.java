/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production Function : {@link FunctionOrSignatureOrPropertyAccessOrStmt} ::= <span class="component">OutputParam:{@link Name}*</span> <span class="component">&lt;Name:String&gt;</span> <span class="component">InputParam:{@link Name}*</span> <span class="component">{@link HelpComment}*</span> <span class="component">{@link Stmt}*</span> <span class="component">NestedFunction:{@link Function}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:44
 */
public class Function extends FunctionOrSignatureOrPropertyAccessOrStmt implements Cloneable, LocalFunctionLookupInterface {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    getLValues_visited = -1;
    getLValues_computed = false;
    getLValues_value = null;
    getNested_visited = -1;
    getNested_computed = false;
    getNested_value = null;
    lookupFunction_String_visited = null;
    getVisible_visited = -1;
    getOutParamSet_visited = -1;
    getOutParamSet_computed = false;
    getOutParamSet_value = null;
    getInParamSet_visited = -1;
    getInParamSet_computed = false;
    getInParamSet_value = null;
    getParamSet_visited = -1;
    getParamSet_computed = false;
    getParamSet_value = null;
    getPrettyPrinted_visited = -1;
    getPrettyPrintedLessComments_visited = -1;
    getIndent_visited = -1;
    getStructureStringLessComments_visited = -1;
    getSiblings_visited = -1;
    getParentFunction_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Function clone() throws CloneNotSupportedException {
    Function node = (Function)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    node.getLValues_visited = -1;
    node.getLValues_computed = false;
    node.getLValues_value = null;
    node.getNested_visited = -1;
    node.getNested_computed = false;
    node.getNested_value = null;
    node.lookupFunction_String_visited = null;
    node.getVisible_visited = -1;
    node.getOutParamSet_visited = -1;
    node.getOutParamSet_computed = false;
    node.getOutParamSet_value = null;
    node.getInParamSet_visited = -1;
    node.getInParamSet_computed = false;
    node.getInParamSet_value = null;
    node.getParamSet_visited = -1;
    node.getParamSet_computed = false;
    node.getParamSet_value = null;
    node.getPrettyPrinted_visited = -1;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getIndent_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.getSiblings_visited = -1;
    node.getParentFunction_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Function copy() {
      try {
        Function node = (Function)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Function fullCopy() {
    try {
      Function tree = (Function) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:274
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseFunction(this);
    }
  /**
   * @ast method 
   * 
   */
  public Function() {
    super();

    setChild(new List(), 0);
    setChild(new List(), 1);
    setChild(new List(), 2);
    setChild(new List(), 3);
    setChild(new List(), 4);

  }
  /**
   * @ast method 
   * 
   */
  public Function(List<Name> p0, String p1, List<Name> p2, List<HelpComment> p3, List<Stmt> p4, List<Function> p5) {
    setChild(p0, 0);
    setName(p1);
    setChild(p2, 1);
    setChild(p3, 2);
    setChild(p4, 3);
    setChild(p5, 4);
  }
  /**
   * @ast method 
   * 
   */
  public Function(List<Name> p0, beaver.Symbol p1, List<Name> p2, List<HelpComment> p3, List<Stmt> p4, List<Function> p5) {
    setChild(p0, 0);
    setName(p1);
    setChild(p2, 1);
    setChild(p3, 2);
    setChild(p4, 3);
    setChild(p5, 4);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 5;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the OutputParam list.
   * @param list The new list node to be used as the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setOutputParamList(List<Name> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the OutputParam list.
   * @return Number of children in the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumOutputParam() {
    return getOutputParamList().getNumChild();
  }
  /**
   * Retrieves the number of children in the OutputParam list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the OutputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumOutputParamNoTransform() {
    return getOutputParamListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the OutputParam list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Name getOutputParam(int i) {
    return (Name)getOutputParamList().getChild(i);
  }
  /**
   * Append an element to the OutputParam list.
   * @param node The element to append to the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addOutputParam(Name node) {
    List<Name> list = (parent == null || state == null) ? getOutputParamListNoTransform() : getOutputParamList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addOutputParamNoTransform(Name node) {
    List<Name> list = getOutputParamListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the OutputParam list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setOutputParam(Name node, int i) {
    List<Name> list = getOutputParamList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the OutputParam list.
   * @return The node representing the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Name> getOutputParams() {
    return getOutputParamList();
  }
  /**
   * Retrieves the OutputParam list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the OutputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Name> getOutputParamsNoTransform() {
    return getOutputParamListNoTransform();
  }
  /**
   * Retrieves the OutputParam list.
   * @return The node representing the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Name> getOutputParamList() {
    List<Name> list = (List<Name>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the OutputParam list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the OutputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Name> getOutputParamListNoTransform() {
    return (List<Name>)getChildNoTransform(0);
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setName(String value) {
    tokenString_Name = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Name;
  /**
   * @ast method 
   * 
   */
  
  public int Namestart;
  /**
   * @ast method 
   * 
   */
  
  public int Nameend;
  /**
   * JastAdd-internal setter for lexeme Name using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setName(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setName is only valid for String lexemes");
    tokenString_Name = (String)symbol.value;
    Namestart = symbol.getStart();
    Nameend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
  /**
   * Replaces the InputParam list.
   * @param list The new list node to be used as the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setInputParamList(List<Name> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the InputParam list.
   * @return Number of children in the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumInputParam() {
    return getInputParamList().getNumChild();
  }
  /**
   * Retrieves the number of children in the InputParam list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the InputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumInputParamNoTransform() {
    return getInputParamListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the InputParam list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Name getInputParam(int i) {
    return (Name)getInputParamList().getChild(i);
  }
  /**
   * Append an element to the InputParam list.
   * @param node The element to append to the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addInputParam(Name node) {
    List<Name> list = (parent == null || state == null) ? getInputParamListNoTransform() : getInputParamList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addInputParamNoTransform(Name node) {
    List<Name> list = getInputParamListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the InputParam list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setInputParam(Name node, int i) {
    List<Name> list = getInputParamList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the InputParam list.
   * @return The node representing the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Name> getInputParams() {
    return getInputParamList();
  }
  /**
   * Retrieves the InputParam list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Name> getInputParamsNoTransform() {
    return getInputParamListNoTransform();
  }
  /**
   * Retrieves the InputParam list.
   * @return The node representing the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Name> getInputParamList() {
    List<Name> list = (List<Name>)getChild(1);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the InputParam list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Name> getInputParamListNoTransform() {
    return (List<Name>)getChildNoTransform(1);
  }
  /**
   * Replaces the HelpComment list.
   * @param list The new list node to be used as the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setHelpCommentList(List<HelpComment> list) {
    setChild(list, 2);
  }
  /**
   * Retrieves the number of children in the HelpComment list.
   * @return Number of children in the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumHelpComment() {
    return getHelpCommentList().getNumChild();
  }
  /**
   * Retrieves the number of children in the HelpComment list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the HelpComment list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumHelpCommentNoTransform() {
    return getHelpCommentListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the HelpComment list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public HelpComment getHelpComment(int i) {
    return (HelpComment)getHelpCommentList().getChild(i);
  }
  /**
   * Append an element to the HelpComment list.
   * @param node The element to append to the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addHelpComment(HelpComment node) {
    List<HelpComment> list = (parent == null || state == null) ? getHelpCommentListNoTransform() : getHelpCommentList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addHelpCommentNoTransform(HelpComment node) {
    List<HelpComment> list = getHelpCommentListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the HelpComment list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setHelpComment(HelpComment node, int i) {
    List<HelpComment> list = getHelpCommentList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the HelpComment list.
   * @return The node representing the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<HelpComment> getHelpComments() {
    return getHelpCommentList();
  }
  /**
   * Retrieves the HelpComment list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the HelpComment list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<HelpComment> getHelpCommentsNoTransform() {
    return getHelpCommentListNoTransform();
  }
  /**
   * Retrieves the HelpComment list.
   * @return The node representing the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<HelpComment> getHelpCommentList() {
    List<HelpComment> list = (List<HelpComment>)getChild(2);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the HelpComment list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the HelpComment list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<HelpComment> getHelpCommentListNoTransform() {
    return (List<HelpComment>)getChildNoTransform(2);
  }
  /**
   * Replaces the Stmt list.
   * @param list The new list node to be used as the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setStmtList(List<Stmt> list) {
    setChild(list, 3);
  }
  /**
   * Retrieves the number of children in the Stmt list.
   * @return Number of children in the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumStmt() {
    return getStmtList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Stmt list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Stmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumStmtNoTransform() {
    return getStmtListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Stmt list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Stmt getStmt(int i) {
    return (Stmt)getStmtList().getChild(i);
  }
  /**
   * Append an element to the Stmt list.
   * @param node The element to append to the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addStmt(Stmt node) {
    List<Stmt> list = (parent == null || state == null) ? getStmtListNoTransform() : getStmtList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addStmtNoTransform(Stmt node) {
    List<Stmt> list = getStmtListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Stmt list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setStmt(Stmt node, int i) {
    List<Stmt> list = getStmtList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Stmt list.
   * @return The node representing the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Stmt> getStmts() {
    return getStmtList();
  }
  /**
   * Retrieves the Stmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Stmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Stmt> getStmtsNoTransform() {
    return getStmtListNoTransform();
  }
  /**
   * Retrieves the Stmt list.
   * @return The node representing the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Stmt> getStmtList() {
    List<Stmt> list = (List<Stmt>)getChild(3);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Stmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Stmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Stmt> getStmtListNoTransform() {
    return (List<Stmt>)getChildNoTransform(3);
  }
  /**
   * Replaces the NestedFunction list.
   * @param list The new list node to be used as the NestedFunction list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setNestedFunctionList(List<Function> list) {
    setChild(list, 4);
  }
  /**
   * Retrieves the number of children in the NestedFunction list.
   * @return Number of children in the NestedFunction list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumNestedFunction() {
    return getNestedFunctionList().getNumChild();
  }
  /**
   * Retrieves the number of children in the NestedFunction list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the NestedFunction list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumNestedFunctionNoTransform() {
    return getNestedFunctionListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the NestedFunction list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the NestedFunction list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Function getNestedFunction(int i) {
    return (Function)getNestedFunctionList().getChild(i);
  }
  /**
   * Append an element to the NestedFunction list.
   * @param node The element to append to the NestedFunction list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addNestedFunction(Function node) {
    List<Function> list = (parent == null || state == null) ? getNestedFunctionListNoTransform() : getNestedFunctionList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addNestedFunctionNoTransform(Function node) {
    List<Function> list = getNestedFunctionListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the NestedFunction list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setNestedFunction(Function node, int i) {
    List<Function> list = getNestedFunctionList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the NestedFunction list.
   * @return The node representing the NestedFunction list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Function> getNestedFunctions() {
    return getNestedFunctionList();
  }
  /**
   * Retrieves the NestedFunction list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the NestedFunction list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Function> getNestedFunctionsNoTransform() {
    return getNestedFunctionListNoTransform();
  }
  /**
   * Retrieves the NestedFunction list.
   * @return The node representing the NestedFunction list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Function> getNestedFunctionList() {
    List<Function> list = (List<Function>)getChild(4);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the NestedFunction list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the NestedFunction list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Function> getNestedFunctionListNoTransform() {
    return (List<Function>)getChildNoTransform(4);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:198
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
        ASTNode previous = parentProgram;
        parentProgram=this;   
        long id = nodeCounter();
        setuID(id);
		
		Element e = doc.createElement("Function");
		e.setAttribute("id", Long.toString(id));
		e.setAttribute("name", getName());

		Element e1 = doc.createElement("InputParamList");
        	for(Name param : getInputParams()) {
            	param.getXML(doc, e1);
        	}
		e.appendChild(e1);

		Element e2 = doc.createElement("OutputParamList");
        	for(Name param : getOutputParams()) {
            	param.getXML(doc, e2);
        	}
		e.appendChild(e2);
        
        for(HelpComment comment : getHelpComments()) {
            comment.getXML(doc, e);
        }
        
        Element e3 = doc.createElement("StmtList");
        for(Stmt stmt : getStmts()) {
            stmt.getXML(doc, e3);
        }
        e.appendChild(e3);
        
        for(Function func : getNestedFunctions()) {
            func.getXML(doc, e);
        }
        e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));       
	    parent.appendChild(e);      
	    parentProgram=previous;
        return true;
    }
  /**
   * @apilevel internal
   */
  protected java.util.Map getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited;
  /**
   * @attribute syn
   * @aspect AnalysisPrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AnalysisPrettyPrint.jrag:111
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getAnalysisPrettyPrintedLessComments(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
    java.util.List _parameters = new java.util.ArrayList(3);
    _parameters.add(analysis);
    _parameters.add(Boolean.valueOf(compact));
    _parameters.add(Boolean.valueOf(hideComments));
    if(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited == null) getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getAnalysisPrettyPrintedLessComments in class: ");
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value = getAnalysisPrettyPrintedLessComments_compute(analysis, compact, hideComments);
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.remove(_parameters);
    return getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value;
  }
  /**
   * @apilevel internal
   */
  private String getAnalysisPrettyPrintedLessComments_compute(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
        StringBuffer buf = new StringBuffer();
        String indent = getIndent();
        buf.append(indent + "function ");
        buf.append(" [");
        boolean first = true;
        for(Name param : getOutputParams()) {
            if(!first) {
                buf.append(", ");
            }
            buf.append(param.getAnalysisPrettyPrinted(analysis, compact, hideComments));
            first = false;
        }
        buf.append("] = ");
        buf.append(getName());
        buf.append("(");
        first = true;
        for(Name param : getInputParams()) {
            if(!first) {
                buf.append(", ");
            }
            buf.append(param.getAnalysisPrettyPrinted(analysis, compact, hideComments));
            first = false;
        }
        buf.append(")");
        buf.append('\n');
        for(HelpComment comment : getHelpComments()) {
            buf.append(comment.getAnalysisPrettyPrinted(analysis, compact, hideComments));
            buf.append('\n');
        }
        for(Stmt stmt : getStmts()) {
            String stmtStr = stmt.getAnalysisPrettyPrinted(analysis, compact, hideComments);         
            buf.append(stmtStr.equals("") ? "" : stmtStr + "\n" );
        }
        for(Function func : getNestedFunctions()) {
            buf.append(func.getAnalysisPrettyPrinted(analysis, compact, hideComments));
            buf.append('\n');
        }
        buf.append(indent + "end");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getLValues_visited = -1;
  /**
   * @apilevel internal
   */
  protected boolean getLValues_computed = false;
  /**
   * @apilevel internal
   */
  protected java.util.Set<String> getLValues_value;
  /**
   * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:29
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<String> getLValues() {
    if(getLValues_computed) {
      return getLValues_value;
    }
      ASTNode$State state = state();
    if(getLValues_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getLValues in class: ");
    getLValues_visited = state().boundariesCrossed;
  int num = state.boundariesCrossed;
  boolean isFinal = this.is$Final();
    getLValues_value = getLValues_compute();
if(isFinal && num == state().boundariesCrossed) getLValues_computed = true;
    getLValues_visited = -1;
    return getLValues_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<String> getLValues_compute() {
        java.util.LinkedHashSet<String> names = new java.util.LinkedHashSet<String>();
        for( int i=0; i<getNumStmt(); i++ )
            names.addAll( getStmt(i).getLValues() );
        return names;
    }
  /**
   * @apilevel internal
   */
  protected int getNested_visited = -1;
  /**
   * @apilevel internal
   */
  protected boolean getNested_computed = false;
  /**
   * @apilevel internal
   */
  protected Map<String,Function> getNested_value;
  /**
   * @attribute syn
   * @aspect NameResolution
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:41
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Map<String,Function> getNested() {
    if(getNested_computed) {
      return getNested_value;
    }
      ASTNode$State state = state();
    if(getNested_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getNested in class: ");
    getNested_visited = state().boundariesCrossed;
  int num = state.boundariesCrossed;
  boolean isFinal = this.is$Final();
    getNested_value = getNested_compute();
if(isFinal && num == state().boundariesCrossed) getNested_computed = true;
    getNested_visited = -1;
    return getNested_value;
  }
  /**
   * @apilevel internal
   */
  private Map<String,Function> getNested_compute() {
        HashMap<String, Function> table = new HashMap<String,Function>();
        for( int i=0; i<getNumNestedFunction(); i++ ){
            table.put( getNestedFunction(i).getName(), getNestedFunction(i) );
        }
        return table;
    }
  /**
   * @apilevel internal
   */
  protected java.util.Map lookupFunction_String_visited;
  /**
   * @attribute syn
   * @aspect NameResolution
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:59
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Function lookupFunction(String name) {
    Object _parameters = name;
    if(lookupFunction_String_visited == null) lookupFunction_String_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(lookupFunction_String_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: lookupFunction in class: ");
    lookupFunction_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    Function lookupFunction_String_value = lookupFunction_compute(name);
    lookupFunction_String_visited.remove(_parameters);
    return lookupFunction_String_value;
  }
  /**
   * @apilevel internal
   */
  private Function lookupFunction_compute(String name) {
        Function f = getNested().get(name);
        if( f != null )
            return f;
        if (getParent() == null || getParentFunction() == null) return null;
        return getParentFunction().lookupFunction( name );
    }
  /**
   * @apilevel internal
   */
  protected int getVisible_visited = -1;
  /**
   * @attribute syn
   * @aspect NameResolution
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:81
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Map<String,Function> getVisible() {
      ASTNode$State state = state();
    if(getVisible_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getVisible in class: ");
    getVisible_visited = state().boundariesCrossed;
    Map<String,Function> getVisible_value = getVisible_compute();
    getVisible_visited = -1;
    return getVisible_value;
  }
  /**
   * @apilevel internal
   */
  private Map<String,Function> getVisible_compute() {
        Map<String,Function> table = getParentFunction().getVisible();
        table.putAll(getNested());
        return table;
    }
  /**
   * @apilevel internal
   */
  protected int getOutParamSet_visited = -1;
  /**
   * @apilevel internal
   */
  protected boolean getOutParamSet_computed = false;
  /**
   * @apilevel internal
   */
  protected Set<String> getOutParamSet_value;
  /**
   * @attribute syn
   * @aspect NameResolution
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:89
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Set<String> getOutParamSet() {
    if(getOutParamSet_computed) {
      return getOutParamSet_value;
    }
      ASTNode$State state = state();
    if(getOutParamSet_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getOutParamSet in class: ");
    getOutParamSet_visited = state().boundariesCrossed;
  int num = state.boundariesCrossed;
  boolean isFinal = this.is$Final();
    getOutParamSet_value = getOutParamSet_compute();
if(isFinal && num == state().boundariesCrossed) getOutParamSet_computed = true;
    getOutParamSet_visited = -1;
    return getOutParamSet_value;
  }
  /**
   * @apilevel internal
   */
  private Set<String> getOutParamSet_compute() {
        LinkedHashSet<String> names = new LinkedHashSet<String>();
        for( int i=0; i<getNumOutputParam(); i++ ){
            if( !names.add( getOutputParam(i).getID() ) )
                //TODO-JD:add error msg
                System.err.println("duplicate output parameters");
        }
        return names;
    }
  /**
   * @apilevel internal
   */
  protected int getInParamSet_visited = -1;
  /**
   * @apilevel internal
   */
  protected boolean getInParamSet_computed = false;
  /**
   * @apilevel internal
   */
  protected Set<String> getInParamSet_value;
  /**
   * @attribute syn
   * @aspect NameResolution
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:99
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Set<String> getInParamSet() {
    if(getInParamSet_computed) {
      return getInParamSet_value;
    }
      ASTNode$State state = state();
    if(getInParamSet_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getInParamSet in class: ");
    getInParamSet_visited = state().boundariesCrossed;
  int num = state.boundariesCrossed;
  boolean isFinal = this.is$Final();
    getInParamSet_value = getInParamSet_compute();
if(isFinal && num == state().boundariesCrossed) getInParamSet_computed = true;
    getInParamSet_visited = -1;
    return getInParamSet_value;
  }
  /**
   * @apilevel internal
   */
  private Set<String> getInParamSet_compute() {
        LinkedHashSet<String> names = new LinkedHashSet<String>();
        for( int i=0; i<getNumInputParam(); i++ ){
            if( !names.add( getInputParam(i).getID() ) )
                //TODO-JD:add error msg
                System.err.println("duplicate input parameters");
        }
        return names;
    }
  /**
   * @apilevel internal
   */
  protected int getParamSet_visited = -1;
  /**
   * @apilevel internal
   */
  protected boolean getParamSet_computed = false;
  /**
   * @apilevel internal
   */
  protected Set<String> getParamSet_value;
  /**
   * @attribute syn
   * @aspect NameResolution
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:110
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Set<String> getParamSet() {
    if(getParamSet_computed) {
      return getParamSet_value;
    }
      ASTNode$State state = state();
    if(getParamSet_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getParamSet in class: ");
    getParamSet_visited = state().boundariesCrossed;
  int num = state.boundariesCrossed;
  boolean isFinal = this.is$Final();
    getParamSet_value = getParamSet_compute();
if(isFinal && num == state().boundariesCrossed) getParamSet_computed = true;
    getParamSet_visited = -1;
    return getParamSet_value;
  }
  /**
   * @apilevel internal
   */
  private Set<String> getParamSet_compute() {
        LinkedHashSet<String> names = new LinkedHashSet<String>();
        names.addAll(getInParamSet());
        names.addAll(getOutParamSet());
        return names;
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrinted_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:50
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrinted() {
      ASTNode$State state = state();
    if(getPrettyPrinted_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrinted in class: ");
    getPrettyPrinted_visited = state().boundariesCrossed;
    String getPrettyPrinted_value = getPrettyPrinted_compute();
    getPrettyPrinted_visited = -1;
    return getPrettyPrinted_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrinted_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append( getPrettyPrintedLessComments() );

        StringBuffer cbuf = new StringBuffer();
        for( beaver.Symbol comment : getComments() ){
            cbuf.append(comment.value);
            cbuf.append("\n");
        }
        if( cbuf.length() > 0 ){
            buf.insert( buf.length() - getIndent().length() - 3, cbuf.toString() );
        }
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:141
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        String indent = getIndent();
        buf.append(indent + "function ");
        buf.append(" [");
        boolean first = true;
        for(Name param : getOutputParams()) {
            if(!first) {
                buf.append(", ");
            }
            buf.append(param.getPrettyPrinted());
            first = false;
        }
        buf.append("] = ");
        buf.append(getName());
        buf.append("(");
        first = true;
        for(Name param : getInputParams()) {
            if(!first) {
                buf.append(", ");
            }
            buf.append(param.getPrettyPrinted());
            first = false;
        }
        buf.append(")");
        buf.append('\n');
        for(HelpComment comment : getHelpComments()) {
            buf.append(comment.getPrettyPrinted());
            buf.append('\n');
        }
        for(Stmt stmt : getStmts()) {
            String stmtStr = stmt.getPrettyPrinted();         
            buf.append(stmtStr.equals("") ? "" : stmtStr + "\n" );
        }
        for(Function func : getNestedFunctions()) {
            buf.append(func.getPrettyPrinted());
            buf.append('\n');
        }
        buf.append(indent + "end");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getIndent_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:743
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getIndent() {
      ASTNode$State state = state();
    if(getIndent_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getIndent in class: ");
    getIndent_visited = state().boundariesCrossed;
    String getIndent_value = getIndent_compute();
    getIndent_visited = -1;
    return getIndent_value;
  }
  /**
   * @apilevel internal
   */
  private String getIndent_compute() {
    	ASTNode parent = getParent();
    	if ( parent != null ){
    	     ASTNode gparent = parent.getParent();
    	     return (gparent instanceof Program) ? "" : parent.getIndent() + ASTNode.INDENT_TAB;
        }
        return "" ;
    }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:52
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append("function ");
        buf.append("[");
        boolean first = true;
        for(Name param : getOutputParams()) {
            if(!first) {
                buf.append(", ");
            }
            buf.append(param.getStructureString());
            first = false;
        }
        buf.append("] = ");
        buf.append(getName());
        buf.append("(");
        first = true;
        for(Name param : getInputParams()) {
            if(!first) {
                buf.append(", ");
            }
            buf.append(param.getStructureString());
            first = false;
        }
        buf.append(")");
        buf.append('\n');
        for(HelpComment comment : getHelpComments()) {
            buf.append(comment.getStructureString());
            buf.append('\n');
        }
        for(Stmt stmt : getStmts()) {
            buf.append(stmt.getStructureString());
            buf.append('\n');
        }
        for(Function func : getNestedFunctions()) {
            buf.append(func.getStructureString());
            buf.append('\n');
        }
        buf.append("end");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getSiblings_visited = -1;
  /**
   * @attribute inh
   * @aspect NameResolution
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:49
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Map<String, Function> getSiblings() {
      ASTNode$State state = state();
    if(getSiblings_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getSiblings in class: ");
    getSiblings_visited = state().boundariesCrossed;
    Map<String, Function> getSiblings_value = getParent().Define_Map_String__Function__getSiblings(this, null);
    getSiblings_visited = -1;
    return getSiblings_value;
  }
  /**
   * @apilevel internal
   */
  protected int getParentFunction_visited = -1;
  /**
   * @attribute inh
   * @aspect NameResolution
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:54
   */
  @SuppressWarnings({"unchecked", "cast"})
  public LocalFunctionLookupInterface getParentFunction() {
      ASTNode$State state = state();
    if(getParentFunction_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getParentFunction in class: ");
    getParentFunction_visited = state().boundariesCrossed;
    LocalFunctionLookupInterface getParentFunction_value = getParent().Define_LocalFunctionLookupInterface_getParentFunction(this, null);
    getParentFunction_visited = -1;
    return getParentFunction_value;
  }
  /**
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:51
   * @apilevel internal
   */
  public Map<String, Function> Define_Map_String__Function__getSiblings(ASTNode caller, ASTNode child) {
    if(caller == getNestedFunctionListNoTransform()) {
      int i = caller.getIndexOfChild(child);
      return getNested();
    }
    else {      return getParent().Define_Map_String__Function__getSiblings(this, caller);
    }
  }
  /**
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:56
   * @apilevel internal
   */
  public LocalFunctionLookupInterface Define_LocalFunctionLookupInterface_getParentFunction(ASTNode caller, ASTNode child) {
    if(caller == getNestedFunctionListNoTransform()) {
      int i = caller.getIndexOfChild(child);
      return this;
    }
    else {      return getParent().Define_LocalFunctionLookupInterface_getParentFunction(this, caller);
    }
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
