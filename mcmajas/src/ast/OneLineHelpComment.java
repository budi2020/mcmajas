/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production OneLineHelpComment : {@link HelpComment};
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:47
 */
public class OneLineHelpComment extends HelpComment implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public OneLineHelpComment clone() throws CloneNotSupportedException {
    OneLineHelpComment node = (OneLineHelpComment)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public OneLineHelpComment copy() {
      try {
        OneLineHelpComment node = (OneLineHelpComment)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public OneLineHelpComment fullCopy() {
    try {
      OneLineHelpComment tree = (OneLineHelpComment) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:355
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseOneLineHelpComment(this);
    }
  /**
   * @ast method 
   * 
   */
  public OneLineHelpComment() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public OneLineHelpComment(String p0) {
    setText(p0);
  }
  /**
   * @ast method 
   * 
   */
  public OneLineHelpComment(beaver.Symbol p0) {
    setText(p0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the lexeme Text.
   * @param value The new value for the lexeme Text.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setText(String value) {
    tokenString_Text = value;
  }
  /**
   * JastAdd-internal setter for lexeme Text using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setText(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setText is only valid for String lexemes");
    tokenString_Text = (String)symbol.value;
    Textstart = symbol.getStart();
    Textend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Text.
   * @return The value for the lexeme Text.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getText() {
    return tokenString_Text != null ? tokenString_Text : "";
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
