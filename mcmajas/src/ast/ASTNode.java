/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production ASTNode;
 * @ast node
 * 
 */
public class ASTNode<T extends ASTNode> extends beaver.Symbol  implements Cloneable, Iterable<T> {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    getuID_visited = -1;
    getuID_computed = false;
    getXML_Document_Element_visited = null;
    getAnalysisPrettyPrinted_analysis_StructuralAnalysis_visited = null;
    getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_visited = null;
    getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    getSymbols_visited = -1;
    getNameExpressions_visited = -1;
    getAllNameExpressions_visited = -1;
    getPrettyPrinted_visited = -1;
    getIndent_visited = -1;
    getPrettyPrintedLessComments_visited = -1;
    dumpString_visited = -1;
    getStructureString_visited = -1;
    getStructureStringLessComments_visited = -1;
    makeUId_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ASTNode<T> clone() throws CloneNotSupportedException {
    ASTNode node = (ASTNode)super.clone();
    node.getuID_visited = -1;
    node.getuID_computed = false;
    node.getXML_Document_Element_visited = null;
    node.getAnalysisPrettyPrinted_analysis_StructuralAnalysis_visited = null;
    node.getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_visited = null;
    node.getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    node.getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    node.getSymbols_visited = -1;
    node.getNameExpressions_visited = -1;
    node.getAllNameExpressions_visited = -1;
    node.getPrettyPrinted_visited = -1;
    node.getIndent_visited = -1;
    node.getPrettyPrintedLessComments_visited = -1;
    node.dumpString_visited = -1;
    node.getStructureString_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.makeUId_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ASTNode<T> copy() {
      try {
        ASTNode node = (ASTNode)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ASTNode<T> fullCopy() {
    try {
      ASTNode tree = (ASTNode) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:31
   */
  
    
	private long uID = -1;
  /**
   * @ast method 
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:32
   */
  public void setuID(long id) {
            uID = id;
	}
  /**
   * @ast method 
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:56
   */
  
        protected static boolean include_kinds_in_xml = true;
  /**
   * @ast method 
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:57
   */
  
        protected static VFFlowInsensitiveAnalysis kind_analysis_insensitive = null;
  /**
   * @ast method 
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:58
   */
  
        protected static VFFlowSensitiveAnalysis kind_analysis_sensitive = null;
  /**
   * @ast method 
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:59
   */
  
        protected static VFPreorderAnalysis kind_analysis_preorder = null;
  /**
   * @ast method 
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:60
   */
  
        protected static ASTNode parentProgram = null;
  /**
   * @ast method 
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:64
   */
  public long nodeCounter() {
	    long counter = 0;
	    ASTNode node = this;
		while(node != null && !(node instanceof CompilationUnits))
	    		node = node.getParent();
	  
	    if( node instanceof CompilationUnits ){
			counter =  ((CompilationUnits)node).nodeCounter++;
	    }
	    return counter;
        }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:25
   */
  public void analyze(nodecases.NodeCaseHandler visitor){
        visitor.caseASTNode(this);
    }
  /**
     * prints the flow data information, and puts it int he supplied Stringbuffer.
     * If the last flag is set, will print only the outset without any extra info
     * @ast method 
   * @aspect AnalysisPrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AnalysisPrettyPrint.jrag:335
   */
  static protected void printFlowData(ASTNode node,Object in,Object out,StringBuffer buf,boolean compact, boolean hideComments){
        if( in != null || out != null ){
            String indent = node.getIndent();
            if (!compact){
                buf.append( "\n" + indent + "% Data flow of " + node.getClass() + "\n" + indent + "%\n" );
                if( in != null ){
                    buf.append( indent + "% In set\n" );
                    Scanner inScanner = new Scanner( in.toString() );
                    while( inScanner.hasNextLine() )
                        buf.append( indent + "% " + inScanner.nextLine() + "\n" );
                    buf.append( indent + "% \n" );
                }
                if( out != null ){
                    buf.append( indent + "% Out set\n" );
                    Scanner outScanner = new Scanner( out.toString() );
                    while( outScanner.hasNextLine() )
                        buf.append( indent + "% " + outScanner.nextLine() + "\n" );
                    buf.append( indent + "% \n" );
                }
                buf.append( "\n" );
            } else { //compact printing
                if( out != null ){
                    Scanner outScanner = new Scanner( out.toString() );
                    while( outScanner.hasNextLine() )
                        buf.append( "\n" + indent + "% " + outScanner.nextLine());
                }
            }
        }        
    }
  /**
   * @ast method 
   * @aspect Comments
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/Comments.jadd:25
   */
  
    private java.util.List<beaver.Symbol> comments = new java.util.ArrayList<beaver.Symbol>();
  /**
     * sets the comments for the given ast node by copying the the comments
     * from the given list of comments.
     * If the argument is null, sets an empty list as comments.
     * @ast method 
   * @aspect Comments
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/Comments.jadd:32
   */
  public void setComments(java.util.List<beaver.Symbol> comments) {
        if (comments != null){
           this.comments = new java.util.ArrayList<beaver.Symbol>(comments);
        } else {
           this.comments =  new java.util.ArrayList<beaver.Symbol>();
        }
    }
  /**
   * @ast method 
   * @aspect Comments
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/Comments.jadd:40
   */
  public java.util.List<beaver.Symbol> getComments() {
        return java.util.Collections.unmodifiableList(comments);
    }
  /**
   * @ast method 
   * @aspect Comments
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/Comments.jadd:44
   */
  public boolean hasComments() {
        return !comments.isEmpty();
    }
  /**
     * adds the given comment at the end of the list of comments
     * @ast method 
   * @aspect Comments
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/Comments.jadd:51
   */
  public void addComment(beaver.Symbol comment){
        comments.add(comment);
    }
  /**
   * @ast method 
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:24
   */
  

    protected static final String INDENT_TAB = "  ";
  /**
   * @ast method 
   * @aspect RewriteControl
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/RewriteControl.jadd:23
   */
  
    public boolean rawAST = false;
  /**
   * @ast method 
   * @aspect RewriteControl
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/RewriteControl.jadd:24
   */
  public boolean useRawAST() {
        if (getParent() == null) {
            return rawAST;
        } else {
            return getParent().useRawAST();
        }
    }
  /**
   * @ast method 
   * @aspect VarName
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/VarName.jadd:2
   */
  public String getVarName() {
    return "";
  }
  /**
   * @ast method 
   * @aspect ASTPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/ASTPrint.jadd:13
   */
  public String getNodeString() {
    StringBuffer s = new StringBuffer();
    s.append(getStructureString());
    return s.toString().trim();
  }
  /**
   * @ast method 
   * @aspect ASTPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/ASTPrint.jadd:28
   */
  public void getNodeString(StringBuffer s) {
    //  throw new Error("ERROR: getNodeString(StringBuffer s) not implemented for " + getClass().getName());
    // no need to throw exception when building tree
    s.append("WARNING: getNodeString(StringBuffer s) not implemented for " + getClass().getName()+"\n");
  }
  /**
   * @ast method 
   * @aspect ASTPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/ASTPrint.jadd:36
   */
  public String dumpTree() {
    StringBuffer s = new StringBuffer();
    dumpTree(s, 0);
    return s.toString();
  }
  /**
   * @ast method 
   * @aspect ASTPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/ASTPrint.jadd:42
   */
  public void dumpTree(StringBuffer s, int j) {
    for(int i = 0; i < j; i++) {
      s.append("  ");
    }
    s.append(dumpString() + "\n");
    for(int i = 0; i < getNumChild(); i++) {
      if (getChild(i)!=null)
      	getChild(i).dumpTree(s, j + 1);
    }
  }
  /**
   * @ast method 
   * 
   */
  public ASTNode() {
    super();


  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  

  /**
   * @apilevel internal
   */
  public static final boolean generatedWithCircularEnabled = true;
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  public static final boolean generatedWithCacheCycle = true;
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  public static final boolean generatedWithComponentCheck = false;
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected static ASTNode$State state = new ASTNode$State();
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public final ASTNode$State state() { return state; }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  public boolean in$Circle = false;
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean in$Circle() { return in$Circle; }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public void in$Circle(boolean b) { in$Circle = b; }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  public boolean is$Final = false;
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean is$Final() { return is$Final; }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public void is$Final(boolean b) { is$Final = b; }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings("cast") public T getChild(int i) {
    return (T)ASTNode.getChild(this, i);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public static ASTNode refined__ASTNode_getChild(ASTNode that, int i) {
    ASTNode node = that.getChildNoTransform(i);
    if(node.is$Final()) return node;
    if(!node.mayHaveRewrite()) {
      node.is$Final(that.is$Final());
      return node;
    }
    if(!node.in$Circle()) {
      int rewriteState;
      int num = that.state().boundariesCrossed;
      do {
        that.state().push(ASTNode$State.REWRITE_CHANGE);
        ASTNode oldNode = node;
        oldNode.in$Circle(true);
        node = node.rewriteTo();
        if(node != oldNode)
          that.setChild(node, i);
        oldNode.in$Circle(false);
        rewriteState = that.state().pop();
      } while(rewriteState == ASTNode$State.REWRITE_CHANGE);
      if(rewriteState == ASTNode$State.REWRITE_NOCHANGE && that.is$Final()) {
        node.is$Final(true);
        that.state().boundariesCrossed = num;
      }
    }
    else if(that.is$Final() != node.is$Final()) that.state().boundariesCrossed++;
    return node;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  private int childIndex;
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getIndexOfChild(ASTNode node) {
    if(node != null && node.childIndex < getNumChildNoTransform() && node == getChildNoTransform(node.childIndex))
      return node.childIndex;
    for(int i = 0; i < getNumChildNoTransform(); i++)
      if(getChildNoTransform(i) == node) {
        node.childIndex = i;
        return i;
      }
    return -1;
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addChild(T node) {
    setChild(node, getNumChildNoTransform());
  }
  /**
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings("cast")
  public final T getChildNoTransform(int i) {
    return (T) (children != null ? children[i] : null);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  
  /**
   * @apilevel low-level
   */
  protected int numChildren;
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return numChildren;
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumChild() {
    return numChildren();
  }
  /**
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @apilevel low-level
   * @ast method 
   * 
   */
  public final int getNumChildNoTransform() {
    return numChildren();
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setChild(ASTNode node, int i) {
    if(children == null) {
      children = new ASTNode[i + 1];
    } else if (i >= children.length) {
      ASTNode c[] = new ASTNode[i << 1];
      System.arraycopy(children, 0, c, 0, children.length);
      children = c;
    }
    children[i] = node;
    if(i >= numChildren) numChildren = i+1;
    if(node != null) { node.setParent(this); node.childIndex = i; }
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void insertChild(ASTNode node, int i) {
    if(children == null) {
      children = new ASTNode[i + 1];
      children[i] = node;
    } else {
      ASTNode c[] = new ASTNode[children.length + 1];
      System.arraycopy(children, 0, c, 0, i);
      c[i] = node;
      if(i < children.length) {
        System.arraycopy(children, i, c, i+1, children.length-i);
        for(int j = i+1; j < c.length; ++j) {
          if(c[j] != null)
            c[j].childIndex = j;
        }
      }
      children = c;
    }
    numChildren++;
    if(node != null) { node.setParent(this); node.childIndex = i; }
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void removeChild(int i) {
    if(children != null) {
      ASTNode child = (ASTNode)children[i];
      if(child != null) {
        child.setParent(null);
        child.childIndex = -1;
      }
      System.arraycopy(children, i+1, children, i, children.length-i-1);
      numChildren--;
      for(int j = i; j < numChildren; ++j) {
        if(children[j] != null) {
          child = (ASTNode) children[j];
          child.childIndex = j;
        }
      }
    }
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public ASTNode getParent() {
    if(parent != null && ((ASTNode)parent).is$Final() != is$Final()) {
      state().boundariesCrossed++;
    }
    return (ASTNode)parent;
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void setParent(ASTNode node) {
    parent = node;
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  
  /**
   * @apilevel low-level
   */
  protected ASTNode parent;
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  
  /**
   * @apilevel low-level
   */
  protected ASTNode[] children;
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public java.util.Iterator<T> iterator() {
    return new java.util.Iterator<T>() {
      private int counter = 0;
      public boolean hasNext() {
        return counter < getNumChild();
      }
      @SuppressWarnings("unchecked") public T next() {
        if(hasNext())
          return (T)getChild(counter++);
        else
          return null;
      }
      public void remove() {
        throw new UnsupportedOperationException();
      }
    };
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * @ast method 
   * @aspect RewriteControl
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/RewriteControl.jadd:32
   */
   public static ASTNode getChild(ASTNode that, int i) {
        if (that.useRawAST()) {
            return that.getChildNoTransform(i);
        } else{
            return refined__ASTNode_getChild(that, i);
        }
    }
  /**
   * @apilevel internal
   */
  protected int getuID_visited = -1;
  /**
   * @apilevel internal
   */
  protected boolean getuID_computed = false;
  /**
   * @apilevel internal
   */
  protected long getuID_value;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:54
   */
  @SuppressWarnings({"unchecked", "cast"})
  public long getuID() {
    if(getuID_computed) {
      return getuID_value;
    }
      ASTNode$State state = state();
    if(getuID_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getuID in class: ");
    getuID_visited = state().boundariesCrossed;
  int num = state.boundariesCrossed;
  boolean isFinal = this.is$Final();
    getuID_value = getuID_compute();
if(isFinal && num == state().boundariesCrossed) getuID_computed = true;
    getuID_visited = -1;
    return getuID_value;
  }
  /**
   * @apilevel internal
   */
  private long getuID_compute() {  return makeUId();  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:149
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) { throw new UnsupportedOperationException(getClass().getName() + ".getXML()"); }
  /**
   * @apilevel internal
   */
  protected java.util.Map getAnalysisPrettyPrinted_analysis_StructuralAnalysis_visited;
  /**
   * @attribute syn
   * @aspect AnalysisPrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AnalysisPrettyPrint.jrag:23
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getAnalysisPrettyPrinted(analysis.StructuralAnalysis analysis) {
    Object _parameters = analysis;
    if(getAnalysisPrettyPrinted_analysis_StructuralAnalysis_visited == null) getAnalysisPrettyPrinted_analysis_StructuralAnalysis_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getAnalysisPrettyPrinted_analysis_StructuralAnalysis_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getAnalysisPrettyPrinted in class: ");
    getAnalysisPrettyPrinted_analysis_StructuralAnalysis_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String getAnalysisPrettyPrinted_analysis_StructuralAnalysis_value = getAnalysisPrettyPrinted_compute(analysis);
    getAnalysisPrettyPrinted_analysis_StructuralAnalysis_visited.remove(_parameters);
    return getAnalysisPrettyPrinted_analysis_StructuralAnalysis_value;
  }
  /**
   * @apilevel internal
   */
  private String getAnalysisPrettyPrinted_compute(analysis.StructuralAnalysis analysis) {
        return getAnalysisPrettyPrinted(analysis,false);
    }
  /**
   * @apilevel internal
   */
  protected java.util.Map getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_visited;
  /**
   * @attribute syn
   * @aspect AnalysisPrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AnalysisPrettyPrint.jrag:28
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getAnalysisPrettyPrinted(analysis.StructuralAnalysis analysis, boolean compact) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(analysis);
    _parameters.add(Boolean.valueOf(compact));
    if(getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_visited == null) getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getAnalysisPrettyPrinted in class: ");
    getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_value = getAnalysisPrettyPrinted_compute(analysis, compact);
    getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_visited.remove(_parameters);
    return getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_value;
  }
  /**
   * @apilevel internal
   */
  private String getAnalysisPrettyPrinted_compute(analysis.StructuralAnalysis analysis, boolean compact) {
        return getAnalysisPrettyPrinted(analysis,false,false);
    }
  /**
   * @apilevel internal
   */
  protected java.util.Map getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_visited;
  /**
     * prints the ast node, together with the flowset(s) that is associated with the nodes, by the
     * given analysis. The second optional parameter results in a more compact print
     * @attribute syn
   * @aspect AnalysisPrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AnalysisPrettyPrint.jrag:37
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getAnalysisPrettyPrinted(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
    java.util.List _parameters = new java.util.ArrayList(3);
    _parameters.add(analysis);
    _parameters.add(Boolean.valueOf(compact));
    _parameters.add(Boolean.valueOf(hideComments));
    if(getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_visited == null) getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getAnalysisPrettyPrinted in class: ");
    getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_value = getAnalysisPrettyPrinted_compute(analysis, compact, hideComments);
    getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_visited.remove(_parameters);
    return getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_value;
  }
  /**
   * @apilevel internal
   */
  private String getAnalysisPrettyPrinted_compute(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
        StringBuffer buf = new StringBuffer();
        if (!hideComments){
            for(beaver.Symbol comment : getComments()) {
                buf.append(comment.value);
                buf.append("\n");
            }   
        }
        buf.append( getAnalysisPrettyPrintedLessComments( analysis, compact, hideComments) );
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected java.util.Map getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited;
  /**
   * @attribute syn
   * @aspect AnalysisPrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AnalysisPrettyPrint.jrag:50
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getAnalysisPrettyPrintedLessComments(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
    java.util.List _parameters = new java.util.ArrayList(3);
    _parameters.add(analysis);
    _parameters.add(Boolean.valueOf(compact));
    _parameters.add(Boolean.valueOf(hideComments));
    if(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited == null) getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getAnalysisPrettyPrintedLessComments in class: ");
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value = getAnalysisPrettyPrintedLessComments_compute(analysis, compact, hideComments);
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.remove(_parameters);
    return getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value;
  }
  /**
   * @apilevel internal
   */
  private String getAnalysisPrettyPrintedLessComments_compute(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
        //throw new UnsupportedOperationException( getClass().getName() + ".getAnalysisPrettyPrintedLessComments( analysis, compact, hideComments)");
        return getPrettyPrintedLessComments();
    }
  /**
   * @apilevel internal
   */
  protected int getSymbols_visited = -1;
  /**
   * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:111
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<String> getSymbols() {
      ASTNode$State state = state();
    if(getSymbols_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getSymbols in class: ");
    getSymbols_visited = state().boundariesCrossed;
    java.util.Set<String> getSymbols_value = getSymbols_compute();
    getSymbols_visited = -1;
    return getSymbols_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<String> getSymbols_compute() {
        java.util.Set<String> symbols = new java.util.LinkedHashSet<String>();
        for( int i = 0; i<getNumChild(); i++ )
            symbols.addAll( getChild(i).getSymbols() );
        return symbols;
    }
  /**
   * @apilevel internal
   */
  protected int getNameExpressions_visited = -1;
  /**
   * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:149
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<NameExpr> getNameExpressions() {
      ASTNode$State state = state();
    if(getNameExpressions_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getNameExpressions in class: ");
    getNameExpressions_visited = state().boundariesCrossed;
    java.util.Set<NameExpr> getNameExpressions_value = getNameExpressions_compute();
    getNameExpressions_visited = -1;
    return getNameExpressions_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<NameExpr> getNameExpressions_compute() {
        java.util.Set<NameExpr> symbols = new java.util.LinkedHashSet<NameExpr>();
        for( int i = 0; i<getNumChild(); i++ )
            symbols.addAll( getChild(i).getNameExpressions() );
        return symbols;
    }
  /**
   * @apilevel internal
   */
  protected int getAllNameExpressions_visited = -1;
  /**
     * returns all children that are names
     * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:189
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<NameExpr> getAllNameExpressions() {
      ASTNode$State state = state();
    if(getAllNameExpressions_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getAllNameExpressions in class: ");
    getAllNameExpressions_visited = state().boundariesCrossed;
    java.util.Set<NameExpr> getAllNameExpressions_value = getAllNameExpressions_compute();
    getAllNameExpressions_visited = -1;
    return getAllNameExpressions_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<NameExpr> getAllNameExpressions_compute() {
        java.util.Set<NameExpr> names = new java.util.LinkedHashSet<NameExpr>();
        for( int i = 0; i<getNumChild(); i++ )
            names.addAll( getChild(i).getAllNameExpressions() );
        return names;
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrinted_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:26
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrinted() {
      ASTNode$State state = state();
    if(getPrettyPrinted_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrinted in class: ");
    getPrettyPrinted_visited = state().boundariesCrossed;
    String getPrettyPrinted_value = getPrettyPrinted_compute();
    getPrettyPrinted_visited = -1;
    return getPrettyPrinted_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrinted_compute() {
        StringBuffer buf = new StringBuffer();
        for(beaver.Symbol comment : getComments()) {
            buf.append(getIndent() + comment.value + "\n");
        }
        buf.append(getPrettyPrintedLessComments());
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getIndent_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:67
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getIndent() {
      ASTNode$State state = state();
    if(getIndent_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getIndent in class: ");
    getIndent_visited = state().boundariesCrossed;
    String getIndent_value = getIndent_compute();
    getIndent_visited = -1;
    return getIndent_value;
  }
  /**
   * @apilevel internal
   */
  private String getIndent_compute() {
        ASTNode parent = getParent();
    	if ( parent != null ){
        	return parent.getIndent();
        }
        return "";
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:77
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() { throw new UnsupportedOperationException(getClass().getName() + ".getPrettyPrintedLessComments()"); }
  /**
   * @apilevel internal
   */
  protected int dumpString_visited = -1;
  /**
   * @attribute syn
   * @aspect ASTPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/ASTPrint.jadd:53
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String dumpString() {
      ASTNode$State state = state();
    if(dumpString_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: dumpString in class: ");
    dumpString_visited = state().boundariesCrossed;
    String dumpString_value = dumpString_compute();
    dumpString_visited = -1;
    return dumpString_value;
  }
  /**
   * @apilevel internal
   */
  private String dumpString_compute() {  return getClass().getName() + " [" + toString() + "]";  }
  /**
   * @apilevel internal
   */
  protected int getStructureString_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:5
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureString() {
      ASTNode$State state = state();
    if(getStructureString_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureString in class: ");
    getStructureString_visited = state().boundariesCrossed;
    String getStructureString_value = getStructureString_compute();
    getStructureString_visited = -1;
    return getStructureString_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureString_compute() {
        StringBuffer buf = new StringBuffer();
        for(beaver.Symbol comment : getComments()) {
            buf.append(comment.value);
            buf.append('\n');
        }
        buf.append(getStructureStringLessComments());
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:16
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() { throw new UnsupportedOperationException(getClass().getName() + ".getStructureStringLessComments()"); }
  /**
   * @apilevel internal
   */
  protected int makeUId_visited = -1;
  /*
	public long ASTNode.getuID() {
		return uID;
        }
        * @attribute inh
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:40
   */
  @SuppressWarnings({"unchecked", "cast"})
  public long makeUId() {
      ASTNode$State state = state();
    if(makeUId_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: makeUId in class: ");
    makeUId_visited = state().boundariesCrossed;
    long makeUId_value = getParent().Define_long_makeUId(this, null);
    makeUId_visited = -1;
    return makeUId_value;
  }
  /**
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:42
   * @apilevel internal
   */
  public long Define_long_makeUId(ASTNode caller, ASTNode child) {
     { 
   int i = this.getIndexOfChild(caller);
{
            long m = getuID();
            
            if( i > 0 ){
                long uid = getChild( i-1 ).getuID();
                m = uid > m ? uid : m;
            }
            uID = m+1;
            return m+1;
        }
    }
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    if(state().peek() == ASTNode$State.REWRITE_CHANGE) {
      state().pop();
      state().push(ASTNode$State.REWRITE_NOCHANGE);
    }
    return this;
  }
  /**
   * @apilevel internal
   */
  public Map<String, Function> Define_Map_String__Function__getSiblings(ASTNode caller, ASTNode child) {
    return getParent().Define_Map_String__Function__getSiblings(this, caller);
  }
  /**
   * @apilevel internal
   */
  public LocalFunctionLookupInterface Define_LocalFunctionLookupInterface_getParentFunction(ASTNode caller, ASTNode child) {
    return getParent().Define_LocalFunctionLookupInterface_getParentFunction(this, caller);
  }
}
