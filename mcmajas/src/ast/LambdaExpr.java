/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production LambdaExpr : {@link Expr} ::= <span class="component">InputParam:{@link Name}*</span> <span class="component">Body:{@link Expr}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:135
 */
public class LambdaExpr extends Expr implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getPrettyPrintedLessComments_visited = -1;
    dumpString_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public LambdaExpr clone() throws CloneNotSupportedException {
    LambdaExpr node = (LambdaExpr)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getPrettyPrintedLessComments_visited = -1;
    node.dumpString_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public LambdaExpr copy() {
      try {
        LambdaExpr node = (LambdaExpr)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public LambdaExpr fullCopy() {
    try {
      LambdaExpr tree = (LambdaExpr) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:269
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseLambdaExpr(this);
    }
  /**
   * @ast method 
   * 
   */
  public LambdaExpr() {
    super();

    setChild(new List(), 0);

  }
  /**
   * @ast method 
   * 
   */
  public LambdaExpr(List<Name> p0, Expr p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the InputParam list.
   * @param list The new list node to be used as the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setInputParamList(List<Name> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the InputParam list.
   * @return Number of children in the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumInputParam() {
    return getInputParamList().getNumChild();
  }
  /**
   * Retrieves the number of children in the InputParam list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the InputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumInputParamNoTransform() {
    return getInputParamListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the InputParam list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Name getInputParam(int i) {
    return (Name)getInputParamList().getChild(i);
  }
  /**
   * Append an element to the InputParam list.
   * @param node The element to append to the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addInputParam(Name node) {
    List<Name> list = (parent == null || state == null) ? getInputParamListNoTransform() : getInputParamList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addInputParamNoTransform(Name node) {
    List<Name> list = getInputParamListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the InputParam list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setInputParam(Name node, int i) {
    List<Name> list = getInputParamList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the InputParam list.
   * @return The node representing the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Name> getInputParams() {
    return getInputParamList();
  }
  /**
   * Retrieves the InputParam list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Name> getInputParamsNoTransform() {
    return getInputParamListNoTransform();
  }
  /**
   * Retrieves the InputParam list.
   * @return The node representing the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Name> getInputParamList() {
    List<Name> list = (List<Name>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the InputParam list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Name> getInputParamListNoTransform() {
    return (List<Name>)getChildNoTransform(0);
  }
  /**
   * Replaces the Body child.
   * @param node The new node to replace the Body child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setBody(Expr node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the Body child.
   * @return The current node used as the Body child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Expr getBody() {
    return (Expr)getChild(1);
  }
  /**
   * Retrieves the Body child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Body child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Expr getBodyNoTransform() {
    return (Expr)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:995
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
        /*
        long id = nodeCounter();
        setuID(id);
		
		Element e = doc.createElement("LambdaExpr");
		e.setAttribute("id", Long.toString(id));

        for(Name param : getInputParams()) {
            param.getXML(doc, e);
        }

        getBody().getXML(doc, e);

        e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
        parent.appendChild(e);*/
        return true;
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:460
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append("(@(");
        boolean first = true;
        for(Name param : getInputParams()) {
            if(!first) {
                buf.append(", ");
            }
            buf.append(param.getPrettyPrinted());
            first = false;
        }
        buf.append(") ");
        buf.append(getBody().getPrettyPrinted());
        buf.append(")");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int dumpString_visited = -1;
  /**
   * @attribute syn
   * @aspect ASTPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/ASTPrint.jadd:61
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String dumpString() {
      ASTNode$State state = state();
    if(dumpString_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: dumpString in class: ");
    dumpString_visited = state().boundariesCrossed;
    String dumpString_value = dumpString_compute();
    dumpString_visited = -1;
    return dumpString_value;
  }
  /**
   * @apilevel internal
   */
  private String dumpString_compute() {  return getClass().getName() + " [" + getStructureStringLessComments() + "]";  }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:567
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append("(@(");
        boolean first = true;
        for(Name param : getInputParams()) {
            if(!first) {
                buf.append(", ");
            }
            buf.append(param.getStructureString());
            first = false;
        }
        buf.append(") ");
        buf.append(getBody().getStructureString());
        buf.append(")");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
