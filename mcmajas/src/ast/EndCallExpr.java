/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production EndCallExpr : {@link Expr} ::= <span class="component">Array:{@link Expr}</span> <span class="component">&lt;NumDim:int&gt;</span> <span class="component">&lt;WhatDim:int&gt;</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/IR.ast:24
 */
public class EndCallExpr extends Expr implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getPrettyPrintedLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public EndCallExpr clone() throws CloneNotSupportedException {
    EndCallExpr node = (EndCallExpr)super.clone();
    node.getPrettyPrintedLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public EndCallExpr copy() {
      try {
        EndCallExpr node = (EndCallExpr)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public EndCallExpr fullCopy() {
    try {
      EndCallExpr tree = (EndCallExpr) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:289
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseEndCallExpr(this);
    }
  /**
   * @ast method 
   * 
   */
  public EndCallExpr() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public EndCallExpr(Expr p0, int p1, int p2) {
    setChild(p0, 0);
    setNumDim(p1);
    setWhatDim(p2);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Array child.
   * @param node The new node to replace the Array child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setArray(Expr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Array child.
   * @return The current node used as the Array child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Expr getArray() {
    return (Expr)getChild(0);
  }
  /**
   * Retrieves the Array child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Array child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Expr getArrayNoTransform() {
    return (Expr)getChildNoTransform(0);
  }
  /**
   * Replaces the lexeme NumDim.
   * @param value The new value for the lexeme NumDim.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setNumDim(int value) {
    tokenint_NumDim = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected int tokenint_NumDim;
  /**
   * Retrieves the value for the lexeme NumDim.
   * @return The value for the lexeme NumDim.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumDim() {
    return tokenint_NumDim;
  }
  /**
   * Replaces the lexeme WhatDim.
   * @param value The new value for the lexeme WhatDim.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setWhatDim(int value) {
    tokenint_WhatDim = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected int tokenint_WhatDim;
  /**
   * Retrieves the value for the lexeme WhatDim.
   * @return The value for the lexeme WhatDim.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getWhatDim() {
    return tokenint_WhatDim;
  }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:82
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {  return "end("+getArray().getPrettyPrinted() + ", " + getNumDim() + ", " + getWhatDim() + ")";  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
