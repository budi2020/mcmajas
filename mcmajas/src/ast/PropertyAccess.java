/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production PropertyAccess : {@link FunctionOrSignatureOrPropertyAccessOrStmt} ::= <span class="component">OutputParam:{@link Name}*</span> <span class="component">&lt;Access:String&gt;</span> <span class="component">&lt;Name:String&gt;</span> <span class="component">InputParam:{@link Name}*</span> <span class="component">{@link HelpComment}*</span> <span class="component">{@link Stmt}*</span> <span class="component">NestedFunction:{@link Function}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:40
 */
public class PropertyAccess extends FunctionOrSignatureOrPropertyAccessOrStmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getPrettyPrintedLessComments_visited = -1;
    getIndent_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public PropertyAccess clone() throws CloneNotSupportedException {
    PropertyAccess node = (PropertyAccess)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getIndent_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public PropertyAccess copy() {
      try {
        PropertyAccess node = (PropertyAccess)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public PropertyAccess fullCopy() {
    try {
      PropertyAccess tree = (PropertyAccess) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:339
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.casePropertyAccess(this);
    }
  /**
   * @ast method 
   * 
   */
  public PropertyAccess() {
    super();

    setChild(new List(), 0);
    setChild(new List(), 1);
    setChild(new List(), 2);
    setChild(new List(), 3);
    setChild(new List(), 4);

  }
  /**
   * @ast method 
   * 
   */
  public PropertyAccess(List<Name> p0, String p1, String p2, List<Name> p3, List<HelpComment> p4, List<Stmt> p5, List<Function> p6) {
    setChild(p0, 0);
    setAccess(p1);
    setName(p2);
    setChild(p3, 1);
    setChild(p4, 2);
    setChild(p5, 3);
    setChild(p6, 4);
  }
  /**
   * @ast method 
   * 
   */
  public PropertyAccess(List<Name> p0, beaver.Symbol p1, beaver.Symbol p2, List<Name> p3, List<HelpComment> p4, List<Stmt> p5, List<Function> p6) {
    setChild(p0, 0);
    setAccess(p1);
    setName(p2);
    setChild(p3, 1);
    setChild(p4, 2);
    setChild(p5, 3);
    setChild(p6, 4);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 5;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the OutputParam list.
   * @param list The new list node to be used as the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setOutputParamList(List<Name> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the OutputParam list.
   * @return Number of children in the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumOutputParam() {
    return getOutputParamList().getNumChild();
  }
  /**
   * Retrieves the number of children in the OutputParam list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the OutputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumOutputParamNoTransform() {
    return getOutputParamListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the OutputParam list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Name getOutputParam(int i) {
    return (Name)getOutputParamList().getChild(i);
  }
  /**
   * Append an element to the OutputParam list.
   * @param node The element to append to the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addOutputParam(Name node) {
    List<Name> list = (parent == null || state == null) ? getOutputParamListNoTransform() : getOutputParamList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addOutputParamNoTransform(Name node) {
    List<Name> list = getOutputParamListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the OutputParam list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setOutputParam(Name node, int i) {
    List<Name> list = getOutputParamList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the OutputParam list.
   * @return The node representing the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Name> getOutputParams() {
    return getOutputParamList();
  }
  /**
   * Retrieves the OutputParam list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the OutputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Name> getOutputParamsNoTransform() {
    return getOutputParamListNoTransform();
  }
  /**
   * Retrieves the OutputParam list.
   * @return The node representing the OutputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Name> getOutputParamList() {
    List<Name> list = (List<Name>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the OutputParam list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the OutputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Name> getOutputParamListNoTransform() {
    return (List<Name>)getChildNoTransform(0);
  }
  /**
   * Replaces the lexeme Access.
   * @param value The new value for the lexeme Access.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setAccess(String value) {
    tokenString_Access = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Access;
  /**
   * @ast method 
   * 
   */
  
  public int Accessstart;
  /**
   * @ast method 
   * 
   */
  
  public int Accessend;
  /**
   * JastAdd-internal setter for lexeme Access using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setAccess(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setAccess is only valid for String lexemes");
    tokenString_Access = (String)symbol.value;
    Accessstart = symbol.getStart();
    Accessend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Access.
   * @return The value for the lexeme Access.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getAccess() {
    return tokenString_Access != null ? tokenString_Access : "";
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setName(String value) {
    tokenString_Name = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_Name;
  /**
   * @ast method 
   * 
   */
  
  public int Namestart;
  /**
   * @ast method 
   * 
   */
  
  public int Nameend;
  /**
   * JastAdd-internal setter for lexeme Name using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setName(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setName is only valid for String lexemes");
    tokenString_Name = (String)symbol.value;
    Namestart = symbol.getStart();
    Nameend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
  /**
   * Replaces the InputParam list.
   * @param list The new list node to be used as the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setInputParamList(List<Name> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the InputParam list.
   * @return Number of children in the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumInputParam() {
    return getInputParamList().getNumChild();
  }
  /**
   * Retrieves the number of children in the InputParam list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the InputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumInputParamNoTransform() {
    return getInputParamListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the InputParam list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Name getInputParam(int i) {
    return (Name)getInputParamList().getChild(i);
  }
  /**
   * Append an element to the InputParam list.
   * @param node The element to append to the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addInputParam(Name node) {
    List<Name> list = (parent == null || state == null) ? getInputParamListNoTransform() : getInputParamList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addInputParamNoTransform(Name node) {
    List<Name> list = getInputParamListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the InputParam list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setInputParam(Name node, int i) {
    List<Name> list = getInputParamList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the InputParam list.
   * @return The node representing the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Name> getInputParams() {
    return getInputParamList();
  }
  /**
   * Retrieves the InputParam list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Name> getInputParamsNoTransform() {
    return getInputParamListNoTransform();
  }
  /**
   * Retrieves the InputParam list.
   * @return The node representing the InputParam list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Name> getInputParamList() {
    List<Name> list = (List<Name>)getChild(1);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the InputParam list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the InputParam list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Name> getInputParamListNoTransform() {
    return (List<Name>)getChildNoTransform(1);
  }
  /**
   * Replaces the HelpComment list.
   * @param list The new list node to be used as the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setHelpCommentList(List<HelpComment> list) {
    setChild(list, 2);
  }
  /**
   * Retrieves the number of children in the HelpComment list.
   * @return Number of children in the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumHelpComment() {
    return getHelpCommentList().getNumChild();
  }
  /**
   * Retrieves the number of children in the HelpComment list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the HelpComment list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumHelpCommentNoTransform() {
    return getHelpCommentListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the HelpComment list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public HelpComment getHelpComment(int i) {
    return (HelpComment)getHelpCommentList().getChild(i);
  }
  /**
   * Append an element to the HelpComment list.
   * @param node The element to append to the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addHelpComment(HelpComment node) {
    List<HelpComment> list = (parent == null || state == null) ? getHelpCommentListNoTransform() : getHelpCommentList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addHelpCommentNoTransform(HelpComment node) {
    List<HelpComment> list = getHelpCommentListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the HelpComment list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setHelpComment(HelpComment node, int i) {
    List<HelpComment> list = getHelpCommentList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the HelpComment list.
   * @return The node representing the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<HelpComment> getHelpComments() {
    return getHelpCommentList();
  }
  /**
   * Retrieves the HelpComment list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the HelpComment list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<HelpComment> getHelpCommentsNoTransform() {
    return getHelpCommentListNoTransform();
  }
  /**
   * Retrieves the HelpComment list.
   * @return The node representing the HelpComment list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<HelpComment> getHelpCommentList() {
    List<HelpComment> list = (List<HelpComment>)getChild(2);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the HelpComment list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the HelpComment list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<HelpComment> getHelpCommentListNoTransform() {
    return (List<HelpComment>)getChildNoTransform(2);
  }
  /**
   * Replaces the Stmt list.
   * @param list The new list node to be used as the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setStmtList(List<Stmt> list) {
    setChild(list, 3);
  }
  /**
   * Retrieves the number of children in the Stmt list.
   * @return Number of children in the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumStmt() {
    return getStmtList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Stmt list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Stmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumStmtNoTransform() {
    return getStmtListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Stmt list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Stmt getStmt(int i) {
    return (Stmt)getStmtList().getChild(i);
  }
  /**
   * Append an element to the Stmt list.
   * @param node The element to append to the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addStmt(Stmt node) {
    List<Stmt> list = (parent == null || state == null) ? getStmtListNoTransform() : getStmtList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addStmtNoTransform(Stmt node) {
    List<Stmt> list = getStmtListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Stmt list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setStmt(Stmt node, int i) {
    List<Stmt> list = getStmtList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Stmt list.
   * @return The node representing the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Stmt> getStmts() {
    return getStmtList();
  }
  /**
   * Retrieves the Stmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Stmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Stmt> getStmtsNoTransform() {
    return getStmtListNoTransform();
  }
  /**
   * Retrieves the Stmt list.
   * @return The node representing the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Stmt> getStmtList() {
    List<Stmt> list = (List<Stmt>)getChild(3);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Stmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Stmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Stmt> getStmtListNoTransform() {
    return (List<Stmt>)getChildNoTransform(3);
  }
  /**
   * Replaces the NestedFunction list.
   * @param list The new list node to be used as the NestedFunction list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setNestedFunctionList(List<Function> list) {
    setChild(list, 4);
  }
  /**
   * Retrieves the number of children in the NestedFunction list.
   * @return Number of children in the NestedFunction list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumNestedFunction() {
    return getNestedFunctionList().getNumChild();
  }
  /**
   * Retrieves the number of children in the NestedFunction list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the NestedFunction list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumNestedFunctionNoTransform() {
    return getNestedFunctionListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the NestedFunction list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the NestedFunction list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Function getNestedFunction(int i) {
    return (Function)getNestedFunctionList().getChild(i);
  }
  /**
   * Append an element to the NestedFunction list.
   * @param node The element to append to the NestedFunction list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addNestedFunction(Function node) {
    List<Function> list = (parent == null || state == null) ? getNestedFunctionListNoTransform() : getNestedFunctionList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addNestedFunctionNoTransform(Function node) {
    List<Function> list = getNestedFunctionListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the NestedFunction list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setNestedFunction(Function node, int i) {
    List<Function> list = getNestedFunctionList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the NestedFunction list.
   * @return The node representing the NestedFunction list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Function> getNestedFunctions() {
    return getNestedFunctionList();
  }
  /**
   * Retrieves the NestedFunction list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the NestedFunction list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Function> getNestedFunctionsNoTransform() {
    return getNestedFunctionListNoTransform();
  }
  /**
   * Retrieves the NestedFunction list.
   * @return The node representing the NestedFunction list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Function> getNestedFunctionList() {
    List<Function> list = (List<Function>)getChild(4);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the NestedFunction list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the NestedFunction list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Function> getNestedFunctionListNoTransform() {
    return (List<Function>)getChildNoTransform(4);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:340
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
        long id = nodeCounter();
        setuID(id);
               
		Element e = doc.createElement("PropertyAccess");
		e.setAttribute("id", Long.toString(id));
		e.setAttribute("name", getName());
		e.setAttribute("access", getAccess());
				
		Element e1 = doc.createElement("InputParamList");
        	for(Name param : getInputParams()) {
            	param.getXML(doc, e1);
        	}
		e.appendChild(e1);

		Element e2 = doc.createElement("OutputParamList");
        	for(Name param : getOutputParams()) {
            	param.getXML(doc, e2);
        	}
		e.appendChild(e2);

        for(HelpComment comment : getHelpComments()) {
            comment.getXML(doc, e);
        }
        
        Element e3 = doc.createElement("StmtList");
        for(Stmt stmt : getStmts()) {
            stmt.getXML(doc, e3);
        }
        e.appendChild(e3);
        
        for(Function func : getNestedFunctions()) {
            func.getXML(doc, e);
        }
           
		e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
parent.appendChild(e);   
        return true;
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:641
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append(getIndent() + "function ");
        buf.append("[");
        boolean first = true;
        for(Name param : getOutputParams()) {
            if(!first) {
                buf.append(", ");
            }
            buf.append(param.getPrettyPrinted());
            first = false;
        }
        buf.append("] = ");
        buf.append(getAccess());
        buf.append(".");
        buf.append(getName());
        buf.append("(");
        first = true;
        for(Name param : getInputParams()) {
            if(!first) {
                buf.append(", ");
            }
            buf.append(param.getPrettyPrinted());
            first = false;
        }
        buf.append(")");
        buf.append('\n');
        for(HelpComment comment : getHelpComments()) {
            buf.append(comment.getPrettyPrinted());
            buf.append('\n');
        }
        for(Stmt stmt : getStmts()) {
            buf.append(stmt.getPrettyPrinted());
            buf.append('\n');
        }
        for(Function func : getNestedFunctions()) {
            buf.append(func.getPrettyPrinted());
            buf.append('\n');
        }
        buf.append(getIndent() + "end");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getIndent_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:776
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getIndent() {
      ASTNode$State state = state();
    if(getIndent_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getIndent in class: ");
    getIndent_visited = state().boundariesCrossed;
    String getIndent_value = getIndent_compute();
    getIndent_visited = -1;
    return getIndent_value;
  }
  /**
   * @apilevel internal
   */
  private String getIndent_compute() {
    	ASTNode parent = getParent();
    	if ( parent != null ){
        	return parent.getIndent() + ASTNode.INDENT_TAB;
        }
        return "" ;
    }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:220
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append("function ");
        buf.append("[");
        boolean first = true;
        for(Name param : getOutputParams()) {
            if(!first) {
                buf.append(", ");
            }
            buf.append(param.getStructureString());
            first = false;
        }
        buf.append("] = ");
        buf.append(getAccess());
        buf.append(".");
        buf.append(getName());
        buf.append("(");
        first = true;
        for(Name param : getInputParams()) {
            if(!first) {
                buf.append(", ");
            }
            buf.append(param.getStructureString());
            first = false;
        }
        buf.append(")");
        buf.append('\n');
        for(HelpComment comment : getHelpComments()) {
            buf.append(comment.getStructureString());
            buf.append('\n');
        }
        for(Stmt stmt : getStmts()) {
            buf.append(stmt.getStructureString());
            buf.append('\n');
        }
        for(Function func : getNestedFunctions()) {
            buf.append(func.getStructureString());
            buf.append('\n');
        }
        buf.append("end");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
