/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production Name : {@link ASTNode} ::= <span class="component">&lt;ID:String&gt;</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:50
 */
public class Name extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getSymbols_visited = -1;
    getPrettyPrintedLessComments_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Name clone() throws CloneNotSupportedException {
    Name node = (Name)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getSymbols_visited = -1;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Name copy() {
      try {
        Name node = (Name)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Name fullCopy() {
    try {
      Name tree = (Name) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:125
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseName(this);
    }
  /**
   * @ast method 
   * @aspect Temporaries
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/Temporaries.jadd:26
   */
  

    public boolean tmpVar = false;
  /**
   * @ast method 
   * @aspect VarName
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/VarName.jadd:14
   */
  public String getVarName() {
    return getID();
  }
  /**
   * @ast method 
   * 
   */
  public Name() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public Name(String p0) {
    setID(p0);
  }
  /**
   * @ast method 
   * 
   */
  public Name(beaver.Symbol p0) {
    setID(p0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setID(String value) {
    tokenString_ID = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected String tokenString_ID;
  /**
   * @ast method 
   * 
   */
  
  public int IDstart;
  /**
   * @ast method 
   * 
   */
  
  public int IDend;
  /**
   * JastAdd-internal setter for lexeme ID using the Beaver parser.
   * @apilevel internal
   * @ast method 
   * 
   */
  public void setID(beaver.Symbol symbol) {
    if(symbol.value != null && !(symbol.value instanceof String))
      throw new UnsupportedOperationException("setID is only valid for String lexemes");
    tokenString_ID = (String)symbol.value;
    IDstart = symbol.getStart();
    IDend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:823
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
    	long id = nodeCounter();
        setuID(id);

		Element e = doc.createElement("Name");
		e.setAttribute("id", Long.toString(id));	
		e.setAttribute("nameId", getID());	

        e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
parent.appendChild(e);
        return true;
    }
  /**
   * @apilevel internal
   */
  protected int getSymbols_visited = -1;
  /**
   * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:126
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<String> getSymbols() {
      ASTNode$State state = state();
    if(getSymbols_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getSymbols in class: ");
    getSymbols_visited = state().boundariesCrossed;
    java.util.Set<String> getSymbols_value = getSymbols_compute();
    getSymbols_visited = -1;
    return getSymbols_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<String> getSymbols_compute() {
        java.util.LinkedHashSet<String> names = new java.util.LinkedHashSet<String>();
        names.add( getID() );
        return names;
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:380
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {  return getID();  }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:489
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {  return getID();  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
