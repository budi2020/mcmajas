/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production Int32 : {@link IntType} ::= <span class="component">&lt;Signed:boolean&gt;</span> <span class="component">&lt;Complex:boolean&gt;</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/types.ast:44
 */
public class Int32 extends IntType implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Int32 clone() throws CloneNotSupportedException {
    Int32 node = (Int32)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Int32 copy() {
      try {
        Int32 node = (Int32)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Int32 fullCopy() {
    try {
      Int32 tree = (Int32) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:506
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseInt32(this);
    }
  /**
   * @ast method 
   * 
   */
  public Int32() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public Int32(boolean p0, boolean p1) {
    setSigned(p0);
    setComplex(p1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the lexeme Signed.
   * @param value The new value for the lexeme Signed.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setSigned(boolean value) {
    tokenboolean_Signed = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected boolean tokenboolean_Signed;
  /**
   * Retrieves the value for the lexeme Signed.
   * @return The value for the lexeme Signed.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean getSigned() {
    return tokenboolean_Signed;
  }
  /**
   * Replaces the lexeme Complex.
   * @param value The new value for the lexeme Complex.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setComplex(boolean value) {
    tokenboolean_Complex = value;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  
  /**
   * @apilevel internal
   */
  protected boolean tokenboolean_Complex;
  /**
   * Retrieves the value for the lexeme Complex.
   * @return The value for the lexeme Complex.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public boolean getComplex() {
    return tokenboolean_Complex;
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
