/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production TryStmt : {@link Stmt} ::= <span class="component">TryStmt:{@link Stmt}*</span> <span class="component">CatchStmt:{@link Stmt}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:68
 */
public class TryStmt extends Stmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    getLValues_visited = -1;
    getLValues_computed = false;
    getLValues_value = null;
    getPrettyPrintedLessComments_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public TryStmt clone() throws CloneNotSupportedException {
    TryStmt node = (TryStmt)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    node.getLValues_visited = -1;
    node.getLValues_computed = false;
    node.getLValues_value = null;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public TryStmt copy() {
      try {
        TryStmt node = (TryStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public TryStmt fullCopy() {
    try {
      TryStmt tree = (TryStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:68
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseTryStmt(this);
    }
  /**
   * @ast method 
   * 
   */
  public TryStmt() {
    super();

    setChild(new List(), 0);
    setChild(new List(), 1);

  }
  /**
   * @ast method 
   * 
   */
  public TryStmt(List<Stmt> p0, List<Stmt> p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the TryStmt list.
   * @param list The new list node to be used as the TryStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setTryStmtList(List<Stmt> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the TryStmt list.
   * @return Number of children in the TryStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumTryStmt() {
    return getTryStmtList().getNumChild();
  }
  /**
   * Retrieves the number of children in the TryStmt list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the TryStmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumTryStmtNoTransform() {
    return getTryStmtListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the TryStmt list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the TryStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Stmt getTryStmt(int i) {
    return (Stmt)getTryStmtList().getChild(i);
  }
  /**
   * Append an element to the TryStmt list.
   * @param node The element to append to the TryStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addTryStmt(Stmt node) {
    List<Stmt> list = (parent == null || state == null) ? getTryStmtListNoTransform() : getTryStmtList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addTryStmtNoTransform(Stmt node) {
    List<Stmt> list = getTryStmtListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the TryStmt list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setTryStmt(Stmt node, int i) {
    List<Stmt> list = getTryStmtList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the TryStmt list.
   * @return The node representing the TryStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Stmt> getTryStmts() {
    return getTryStmtList();
  }
  /**
   * Retrieves the TryStmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the TryStmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Stmt> getTryStmtsNoTransform() {
    return getTryStmtListNoTransform();
  }
  /**
   * Retrieves the TryStmt list.
   * @return The node representing the TryStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Stmt> getTryStmtList() {
    List<Stmt> list = (List<Stmt>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the TryStmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the TryStmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Stmt> getTryStmtListNoTransform() {
    return (List<Stmt>)getChildNoTransform(0);
  }
  /**
   * Replaces the CatchStmt list.
   * @param list The new list node to be used as the CatchStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setCatchStmtList(List<Stmt> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the CatchStmt list.
   * @return Number of children in the CatchStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumCatchStmt() {
    return getCatchStmtList().getNumChild();
  }
  /**
   * Retrieves the number of children in the CatchStmt list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the CatchStmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumCatchStmtNoTransform() {
    return getCatchStmtListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the CatchStmt list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the CatchStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Stmt getCatchStmt(int i) {
    return (Stmt)getCatchStmtList().getChild(i);
  }
  /**
   * Append an element to the CatchStmt list.
   * @param node The element to append to the CatchStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addCatchStmt(Stmt node) {
    List<Stmt> list = (parent == null || state == null) ? getCatchStmtListNoTransform() : getCatchStmtList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addCatchStmtNoTransform(Stmt node) {
    List<Stmt> list = getCatchStmtListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the CatchStmt list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setCatchStmt(Stmt node, int i) {
    List<Stmt> list = getCatchStmtList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the CatchStmt list.
   * @return The node representing the CatchStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Stmt> getCatchStmts() {
    return getCatchStmtList();
  }
  /**
   * Retrieves the CatchStmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the CatchStmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Stmt> getCatchStmtsNoTransform() {
    return getCatchStmtListNoTransform();
  }
  /**
   * Retrieves the CatchStmt list.
   * @return The node representing the CatchStmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Stmt> getCatchStmtList() {
    List<Stmt> list = (List<Stmt>)getChild(1);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the CatchStmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the CatchStmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Stmt> getCatchStmtListNoTransform() {
    return (List<Stmt>)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:754
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
        long id = nodeCounter();
        setuID(id);

		Element e = doc.createElement("TryStmt");
		e.setAttribute("id", Long.toString(id));	

		Element e1 = doc.createElement("StmtList");
        for(Stmt stmt : getTryStmts()) {
            stmt.getXML(doc, e1);
        }
        e.appendChild(e1);
       
        List<Stmt> catchList = getCatchStmts();
        if (catchList.getNumChild()>0) {      
	        Element e2 = doc.createElement("StmtList");
	        for(Stmt stmt : catchList) {
	            stmt.getXML(doc, e2);
	        }
	        e.appendChild(e2);
	    }
        e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
        parent.appendChild(e);
        return true;
    }
  /**
   * @apilevel internal
   */
  protected java.util.Map getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited;
  /**
   * @attribute syn
   * @aspect AnalysisPrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AnalysisPrettyPrint.jrag:304
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getAnalysisPrettyPrintedLessComments(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
    java.util.List _parameters = new java.util.ArrayList(3);
    _parameters.add(analysis);
    _parameters.add(Boolean.valueOf(compact));
    _parameters.add(Boolean.valueOf(hideComments));
    if(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited == null) getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getAnalysisPrettyPrintedLessComments in class: ");
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value = getAnalysisPrettyPrintedLessComments_compute(analysis, compact, hideComments);
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.remove(_parameters);
    return getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value;
  }
  /**
   * @apilevel internal
   */
  private String getAnalysisPrettyPrintedLessComments_compute(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
        StringBuffer buf = new StringBuffer();
        String indent = getIndent();
        buf.append( indent + "try ");
        buf.append('\n');
        for(Stmt stmt : getTryStmts()) {
            String stmtStr = stmt.getAnalysisPrettyPrinted(analysis, compact, hideComments);         
            buf.append(stmtStr.equals("") ? "" : stmtStr + "\n" );
        }
        List<Stmt> catchList = getCatchStmts();
        if (catchList.getNumChild()>0) {
            buf.append( indent + "catch ");
            buf.append('\n');        
            for(Stmt stmt : catchList) {
                String stmtStr = stmt.getAnalysisPrettyPrinted(analysis, compact, hideComments);         
            	buf.append(stmtStr.equals("") ? "" : stmtStr + "\n" );
            }
        }
        buf.append( indent + "end" );
        Object in, out;
        in = analysis.getInFlowSets().get( this );
        out = analysis.getOutFlowSets().get( this );
        printFlowData(this,in,out,buf,compact, hideComments);
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getLValues_visited = -1;
  /**
   * @apilevel internal
   */
  protected boolean getLValues_computed = false;
  /**
   * @apilevel internal
   */
  protected java.util.Set<String> getLValues_value;
  /**
   * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:86
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<String> getLValues() {
    if(getLValues_computed) {
      return getLValues_value;
    }
      ASTNode$State state = state();
    if(getLValues_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getLValues in class: ");
    getLValues_visited = state().boundariesCrossed;
  int num = state.boundariesCrossed;
  boolean isFinal = this.is$Final();
    getLValues_value = getLValues_compute();
if(isFinal && num == state().boundariesCrossed) getLValues_computed = true;
    getLValues_visited = -1;
    return getLValues_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<String> getLValues_compute() {
        java.util.LinkedHashSet<String> names = new java.util.LinkedHashSet<String>();
        for( int i=0; i<getNumTryStmt(); i++ )
            names.addAll( getTryStmt(i).getLValues() );
        for( int i=0; i<getNumCatchStmt(); i++ )
            names.addAll( getCatchStmt(i).getLValues() );
        return names;
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:339
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        String indent = getIndent();
        buf.append( indent + "try ");
        buf.append('\n');
        for(Stmt stmt : getTryStmts()) {
            String stmtStr = stmt.getPrettyPrinted();         
            buf.append(stmtStr.equals("") ? "" : stmtStr + "\n" );
        }
        List<Stmt> catchList = getCatchStmts();
        if (catchList.getNumChild()>0) {
            buf.append( indent + "catch ");
            buf.append('\n');        
            for(Stmt stmt : catchList) {
                String stmtStr = stmt.getPrettyPrinted();         
            	buf.append(stmtStr.equals("") ? "" : stmtStr + "\n" );
            }
        }
        buf.append( indent + "end" );
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:448
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append("try ");
        buf.append('\n');
        for(Stmt stmt : getTryStmts()) {
            buf.append('\t');
            buf.append(stmt.getStructureString());
            buf.append('\n');
        }
        List<Stmt> catchList = getCatchStmts();
        if (catchList.getNumChild()>0) {
	        buf.append("catch ");
	        buf.append('\n');        
	        for(Stmt stmt : catchList) {
	            buf.append('\t');
	            buf.append(stmt.getStructureString());
	            buf.append('\n');
	        }
	    }
        buf.append("end");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
