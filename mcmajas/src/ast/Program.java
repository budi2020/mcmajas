/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production Program : {@link ASTNode};
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:24
 */
public abstract class Program extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Program clone() throws CloneNotSupportedException {
    Program node = (Program)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:31
   */
  public void analyze(nodecases.NodeCaseHandler visitor){
        visitor.caseProgram(this);
    }
  /**
   * @ast method 
   * @aspect NameResolutionAdditions
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jadd:37
   */
     	    

    protected String fullpath="";
  /**
   * @ast method 
   * @aspect NameResolutionAdditions
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jadd:38
   */
  
    protected String name="";
  /**
   * @ast method 
   * @aspect NameResolutionAdditions
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jadd:39
   */
    
    protected GenericFile file=null;
  /**
   * @ast method 
   * @aspect NameResolutionAdditions
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jadd:40
   */
  public void setName(String name){this.name=name;}
  /**
   * @ast method 
   * @aspect NameResolutionAdditions
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jadd:42
   */
  public void setFullPath(String file){

    }
  /**
   * @ast method 
   * @aspect NameResolutionAdditions
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jadd:45
   */
  public String getName(){return name;}
  /**
   * @ast method 
   * @aspect NameResolutionAdditions
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jadd:46
   */
  public String getFullPath(){return this.fullpath;}
  /**
   * @ast method 
   * @aspect NameResolutionAdditions
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jadd:48
   */
  public void setFile(GenericFile f){
        this.fullpath=f.getPath();
        String extension = f.getExtensionComplete();
        this.name=f.getName().substring(0, f.getName().length()-extension.length());
        this.file=f;
    }
  /**
   * @ast method 
   * @aspect NameResolutionAdditions
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jadd:55
   */
  public GenericFile getFile(){
        return file;
    }
  /**
   * @ast method 
   * 
   */
  public Program() {
    super();


  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
