/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production DotExpr : {@link LValueExpr} ::= <span class="component">Target:{@link Expr}</span> <span class="component">Field:{@link Name}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:87
 */
public class DotExpr extends LValueExpr implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getSymbols_visited = -1;
    getNameExpressions_visited = -1;
    getPrettyPrintedLessComments_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public DotExpr clone() throws CloneNotSupportedException {
    DotExpr node = (DotExpr)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getSymbols_visited = -1;
    node.getNameExpressions_visited = -1;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public DotExpr copy() {
      try {
        DotExpr node = (DotExpr)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public DotExpr fullCopy() {
    try {
      DotExpr tree = (DotExpr) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:137
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseDotExpr(this);
    }
  /**
   * @ast method 
   * 
   */
  public DotExpr() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public DotExpr(Expr p0, Name p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Target child.
   * @param node The new node to replace the Target child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setTarget(Expr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Target child.
   * @return The current node used as the Target child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Expr getTarget() {
    return (Expr)getChild(0);
  }
  /**
   * Retrieves the Target child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Target child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Expr getTargetNoTransform() {
    return (Expr)getChildNoTransform(0);
  }
  /**
   * Replaces the Field child.
   * @param node The new node to replace the Field child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setField(Name node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the Field child.
   * @return The current node used as the Field child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Name getField() {
    return (Name)getChild(1);
  }
  /**
   * Retrieves the Field child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Field child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Name getFieldNoTransform() {
    return (Name)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:882
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
        long id = nodeCounter();
        setuID(id);
		
		Element e = doc.createElement("DotExpr");
		e.setAttribute("id", Long.toString(id));

      getTarget().getXML(doc, e);
	getField().getXML(doc, e);

        e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
parent.appendChild(e);
        return true;
    }
  /**
   * @apilevel internal
   */
  protected int getSymbols_visited = -1;
  /**
   * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:137
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<String> getSymbols() {
      ASTNode$State state = state();
    if(getSymbols_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getSymbols in class: ");
    getSymbols_visited = state().boundariesCrossed;
    java.util.Set<String> getSymbols_value = getSymbols_compute();
    getSymbols_visited = -1;
    return getSymbols_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<String> getSymbols_compute() {  return getTarget().getSymbols();  }
  /**
   * @apilevel internal
   */
  protected int getNameExpressions_visited = -1;
  /**
   * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:173
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<NameExpr> getNameExpressions() {
      ASTNode$State state = state();
    if(getNameExpressions_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getNameExpressions in class: ");
    getNameExpressions_visited = state().boundariesCrossed;
    java.util.Set<NameExpr> getNameExpressions_value = getNameExpressions_compute();
    getNameExpressions_visited = -1;
    return getNameExpressions_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<NameExpr> getNameExpressions_compute() {  return getTarget().getNameExpressions();  }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:384
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {  return getTarget().getPrettyPrinted() + "." + getField().getPrettyPrinted();  }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:493
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {  return getTarget().getStructureString() + "." + getField().getStructureString();  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
