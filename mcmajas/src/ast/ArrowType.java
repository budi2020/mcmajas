/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production ArrowType : {@link Type} ::= <span class="component">ArgType:{@link Type}</span> <span class="component">ResultType:{@link Type}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/types.ast:25
 */
public class ArrowType extends Type implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArrowType clone() throws CloneNotSupportedException {
    ArrowType node = (ArrowType)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArrowType copy() {
      try {
        ArrowType node = (ArrowType)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArrowType fullCopy() {
    try {
      ArrowType tree = (ArrowType) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:454
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseArrowType(this);
    }
  /**
   * @ast method 
   * 
   */
  public ArrowType() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public ArrowType(Type p0, Type p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the ArgType child.
   * @param node The new node to replace the ArgType child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setArgType(Type node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the ArgType child.
   * @return The current node used as the ArgType child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Type getArgType() {
    return (Type)getChild(0);
  }
  /**
   * Retrieves the ArgType child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the ArgType child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Type getArgTypeNoTransform() {
    return (Type)getChildNoTransform(0);
  }
  /**
   * Replaces the ResultType child.
   * @param node The new node to replace the ResultType child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setResultType(Type node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the ResultType child.
   * @return The current node used as the ResultType child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Type getResultType() {
    return (Type)getChild(1);
  }
  /**
   * Retrieves the ResultType child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the ResultType child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Type getResultTypeNoTransform() {
    return (Type)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
