/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production ArgTupleType : {@link Type} ::= <span class="component">StaticArgType:{@link Type}*</span> <span class="component">{@link VarArgType}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/types.ast:26
 */
public class ArgTupleType extends Type implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArgTupleType clone() throws CloneNotSupportedException {
    ArgTupleType node = (ArgTupleType)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArgTupleType copy() {
      try {
        ArgTupleType node = (ArgTupleType)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ArgTupleType fullCopy() {
    try {
      ArgTupleType tree = (ArgTupleType) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:458
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseArgTupleType(this);
    }
  /**
   * @ast method 
   * 
   */
  public ArgTupleType() {
    super();

    setChild(new List(), 0);

  }
  /**
   * @ast method 
   * 
   */
  public ArgTupleType(List<Type> p0, VarArgType p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the StaticArgType list.
   * @param list The new list node to be used as the StaticArgType list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setStaticArgTypeList(List<Type> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the StaticArgType list.
   * @return Number of children in the StaticArgType list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumStaticArgType() {
    return getStaticArgTypeList().getNumChild();
  }
  /**
   * Retrieves the number of children in the StaticArgType list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the StaticArgType list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumStaticArgTypeNoTransform() {
    return getStaticArgTypeListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the StaticArgType list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the StaticArgType list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Type getStaticArgType(int i) {
    return (Type)getStaticArgTypeList().getChild(i);
  }
  /**
   * Append an element to the StaticArgType list.
   * @param node The element to append to the StaticArgType list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addStaticArgType(Type node) {
    List<Type> list = (parent == null || state == null) ? getStaticArgTypeListNoTransform() : getStaticArgTypeList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addStaticArgTypeNoTransform(Type node) {
    List<Type> list = getStaticArgTypeListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the StaticArgType list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setStaticArgType(Type node, int i) {
    List<Type> list = getStaticArgTypeList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the StaticArgType list.
   * @return The node representing the StaticArgType list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Type> getStaticArgTypes() {
    return getStaticArgTypeList();
  }
  /**
   * Retrieves the StaticArgType list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the StaticArgType list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Type> getStaticArgTypesNoTransform() {
    return getStaticArgTypeListNoTransform();
  }
  /**
   * Retrieves the StaticArgType list.
   * @return The node representing the StaticArgType list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Type> getStaticArgTypeList() {
    List<Type> list = (List<Type>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the StaticArgType list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the StaticArgType list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Type> getStaticArgTypeListNoTransform() {
    return (List<Type>)getChildNoTransform(0);
  }
  /**
   * Replaces the VarArgType child.
   * @param node The new node to replace the VarArgType child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setVarArgType(VarArgType node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the VarArgType child.
   * @return The current node used as the VarArgType child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public VarArgType getVarArgType() {
    return (VarArgType)getChild(1);
  }
  /**
   * Retrieves the VarArgType child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the VarArgType child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public VarArgType getVarArgTypeNoTransform() {
    return (VarArgType)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
