/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production HandleType : {@link BaseType} ::= <span class="component">{@link ArrowType}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/types.ast:36
 */
public class HandleType extends BaseType implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public HandleType clone() throws CloneNotSupportedException {
    HandleType node = (HandleType)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public HandleType copy() {
      try {
        HandleType node = (HandleType)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public HandleType fullCopy() {
    try {
      HandleType tree = (HandleType) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:486
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseHandleType(this);
    }
  /**
   * @ast method 
   * 
   */
  public HandleType() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public HandleType(ArrowType p0) {
    setChild(p0, 0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the ArrowType child.
   * @param node The new node to replace the ArrowType child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setArrowType(ArrowType node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the ArrowType child.
   * @return The current node used as the ArrowType child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public ArrowType getArrowType() {
    return (ArrowType)getChild(0);
  }
  /**
   * Retrieves the ArrowType child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the ArrowType child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public ArrowType getArrowTypeNoTransform() {
    return (ArrowType)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
