/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production FunctionList : {@link Program} ::= <span class="component">{@link Function}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:26
 */
public class FunctionList extends Program implements Cloneable, LocalFunctionLookupInterface {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    getMainFunction_visited = -1;
    getSiblings_visited = -1;
    getSiblings_computed = false;
    getSiblings_value = null;
    getNested_visited = -1;
    getNested_computed = false;
    getNested_value = null;
    getParentFunction_visited = -1;
    lookupFunction_String_visited = null;
    getVisible_visited = -1;
    getPrettyPrintedLessComments_visited = -1;
    getPrettyPrinted_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public FunctionList clone() throws CloneNotSupportedException {
    FunctionList node = (FunctionList)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    node.getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    node.getMainFunction_visited = -1;
    node.getSiblings_visited = -1;
    node.getSiblings_computed = false;
    node.getSiblings_value = null;
    node.getNested_visited = -1;
    node.getNested_computed = false;
    node.getNested_value = null;
    node.getParentFunction_visited = -1;
    node.lookupFunction_String_visited = null;
    node.getVisible_visited = -1;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getPrettyPrinted_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public FunctionList copy() {
      try {
        FunctionList node = (FunctionList)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public FunctionList fullCopy() {
    try {
      FunctionList tree = (FunctionList) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:85
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseFunctionList(this);
    }
  /**
   * @ast method 
   * 
   */
  public FunctionList() {
    super();

    setChild(new List(), 0);

  }
  /**
   * @ast method 
   * 
   */
  public FunctionList(List<Function> p0) {
    setChild(p0, 0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Function list.
   * @param list The new list node to be used as the Function list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setFunctionList(List<Function> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Function list.
   * @return Number of children in the Function list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumFunction() {
    return getFunctionList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Function list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Function list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumFunctionNoTransform() {
    return getFunctionListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Function list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Function list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Function getFunction(int i) {
    return (Function)getFunctionList().getChild(i);
  }
  /**
   * Append an element to the Function list.
   * @param node The element to append to the Function list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addFunction(Function node) {
    List<Function> list = (parent == null || state == null) ? getFunctionListNoTransform() : getFunctionList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addFunctionNoTransform(Function node) {
    List<Function> list = getFunctionListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Function list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setFunction(Function node, int i) {
    List<Function> list = getFunctionList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Function list.
   * @return The node representing the Function list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Function> getFunctions() {
    return getFunctionList();
  }
  /**
   * Retrieves the Function list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Function list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Function> getFunctionsNoTransform() {
    return getFunctionListNoTransform();
  }
  /**
   * Retrieves the Function list.
   * @return The node representing the Function list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Function> getFunctionList() {
    List<Function> list = (List<Function>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Function list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Function list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Function> getFunctionListNoTransform() {
    return (List<Function>)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:182
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
        long id = nodeCounter();
        setuID(id);

		Element e = doc.createElement("FunctionList");
		e.setAttribute("id", Long.toString(id));
		e.setAttribute("fullpath", fullpath);
       
        for(Function func : getFunctions()) {
            func.getXML(doc, e);
        }
        e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
        parent.appendChild(e);
        return true;
    }
  /**
   * @apilevel internal
   */
  protected java.util.Map getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited;
  /**
   * @attribute syn
   * @aspect AnalysisPrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AnalysisPrettyPrint.jrag:92
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getAnalysisPrettyPrintedLessComments(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
    java.util.List _parameters = new java.util.ArrayList(3);
    _parameters.add(analysis);
    _parameters.add(Boolean.valueOf(compact));
    _parameters.add(Boolean.valueOf(hideComments));
    if(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited == null) getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getAnalysisPrettyPrintedLessComments in class: ");
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value = getAnalysisPrettyPrintedLessComments_compute(analysis, compact, hideComments);
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.remove(_parameters);
    return getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value;
  }
  /**
   * @apilevel internal
   */
  private String getAnalysisPrettyPrintedLessComments_compute(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
        StringBuffer buf = new StringBuffer();
        for(Function func : getFunctions()) {
            buf.append(func.getAnalysisPrettyPrinted(analysis, compact, hideComments));
            buf.append('\n');
        }
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected java.util.Map getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_visited;
  /**
     * prints the ast node, together with the flowset(s) that is associated with the nodes, by the
     * given analysis. The second optional parameter results in a more compact print
     * @attribute syn
   * @aspect AnalysisPrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AnalysisPrettyPrint.jrag:101
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getAnalysisPrettyPrinted(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
    java.util.List _parameters = new java.util.ArrayList(3);
    _parameters.add(analysis);
    _parameters.add(Boolean.valueOf(compact));
    _parameters.add(Boolean.valueOf(hideComments));
    if(getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_visited == null) getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getAnalysisPrettyPrinted in class: ");
    getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_value = getAnalysisPrettyPrinted_compute(analysis, compact, hideComments);
    getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_visited.remove(_parameters);
    return getAnalysisPrettyPrinted_analysis_StructuralAnalysis_boolean_boolean_value;
  }
  /**
   * @apilevel internal
   */
  private String getAnalysisPrettyPrinted_compute(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
        StringBuffer buf = new StringBuffer();
        buf.append(getAnalysisPrettyPrintedLessComments( analysis, compact, hideComments));
        for(beaver.Symbol comment : getComments()) {
            buf.append(comment.value);
            buf.append("\n");
        }
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getMainFunction_visited = -1;
  /**
   * @attribute syn
   * @aspect NameResolution
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:27
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Function getMainFunction() {
      ASTNode$State state = state();
    if(getMainFunction_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getMainFunction in class: ");
    getMainFunction_visited = state().boundariesCrossed;
    Function getMainFunction_value = getMainFunction_compute();
    getMainFunction_visited = -1;
    return getMainFunction_value;
  }
  /**
   * @apilevel internal
   */
  private Function getMainFunction_compute() {  return getFunction(0);  }
  /**
   * @apilevel internal
   */
  protected int getSiblings_visited = -1;
  /**
   * @apilevel internal
   */
  protected boolean getSiblings_computed = false;
  /**
   * @apilevel internal
   */
  protected Map<String,Function> getSiblings_value;
  /**
   * @attribute syn
   * @aspect NameResolution
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:29
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Map<String,Function> getSiblings() {
    if(getSiblings_computed) {
      return getSiblings_value;
    }
      ASTNode$State state = state();
    if(getSiblings_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getSiblings in class: ");
    getSiblings_visited = state().boundariesCrossed;
  int num = state.boundariesCrossed;
  boolean isFinal = this.is$Final();
    getSiblings_value = getSiblings_compute();
if(isFinal && num == state().boundariesCrossed) getSiblings_computed = true;
    getSiblings_visited = -1;
    return getSiblings_value;
  }
  /**
   * @apilevel internal
   */
  private Map<String,Function> getSiblings_compute() {  return new HashMap<String,Function>();  }
  /**
   * @apilevel internal
   */
  protected int getNested_visited = -1;
  /**
   * @apilevel internal
   */
  protected boolean getNested_computed = false;
  /**
   * @apilevel internal
   */
  protected Map<String,Function> getNested_value;
  /**
   * @attribute syn
   * @aspect NameResolution
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:30
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Map<String,Function> getNested() {
    if(getNested_computed) {
      return getNested_value;
    }
      ASTNode$State state = state();
    if(getNested_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getNested in class: ");
    getNested_visited = state().boundariesCrossed;
  int num = state.boundariesCrossed;
  boolean isFinal = this.is$Final();
    getNested_value = getNested_compute();
if(isFinal && num == state().boundariesCrossed) getNested_computed = true;
    getNested_visited = -1;
    return getNested_value;
  }
  /**
   * @apilevel internal
   */
  private Map<String,Function> getNested_compute() {
        HashMap<String,Function> table = new HashMap<String,Function>();
        for( int i=0; i<getNumFunction(); i++ ){
            table.put( getFunction(i).getName(), getFunction(i) );
        }
        //TODO-JD: add alias for main function with file name
        return table;
    }
  /**
   * @apilevel internal
   */
  protected int getParentFunction_visited = -1;
  /**
   * @attribute syn
   * @aspect NameResolution
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:39
   */
  @SuppressWarnings({"unchecked", "cast"})
  public LocalFunctionLookupInterface getParentFunction() {
      ASTNode$State state = state();
    if(getParentFunction_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getParentFunction in class: ");
    getParentFunction_visited = state().boundariesCrossed;
    LocalFunctionLookupInterface getParentFunction_value = getParentFunction_compute();
    getParentFunction_visited = -1;
    return getParentFunction_value;
  }
  /**
   * @apilevel internal
   */
  private LocalFunctionLookupInterface getParentFunction_compute() {  return null;  }
  /**
   * @apilevel internal
   */
  protected java.util.Map lookupFunction_String_visited;
  /**
   * @attribute syn
   * @aspect NameResolution
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:67
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Function lookupFunction(String name) {
    Object _parameters = name;
    if(lookupFunction_String_visited == null) lookupFunction_String_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(lookupFunction_String_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: lookupFunction in class: ");
    lookupFunction_String_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    Function lookupFunction_String_value = lookupFunction_compute(name);
    lookupFunction_String_visited.remove(_parameters);
    return lookupFunction_String_value;
  }
  /**
   * @apilevel internal
   */
  private Function lookupFunction_compute(String name) {
        return getNested().get(name);
    }
  /**
   * @apilevel internal
   */
  protected int getVisible_visited = -1;
  /**
   * @attribute syn
   * @aspect NameResolution
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:74
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Map<String,Function> getVisible() {
      ASTNode$State state = state();
    if(getVisible_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getVisible in class: ");
    getVisible_visited = state().boundariesCrossed;
    Map<String,Function> getVisible_value = getVisible_compute();
    getVisible_visited = -1;
    return getVisible_value;
  }
  /**
   * @apilevel internal
   */
  private Map<String,Function> getVisible_compute() {
        /*HashMap<String,Function> table = new HashMap<String,Function>();
        table.put(getMainFunction().getName(), getMainFunction());
        return table;*/
        return getNested();
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:122
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        for(Function func : getFunctions()) {
            buf.append(func.getPrettyPrinted());
            buf.append('\n');
        }
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrinted_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:131
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrinted() {
      ASTNode$State state = state();
    if(getPrettyPrinted_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrinted in class: ");
    getPrettyPrinted_visited = state().boundariesCrossed;
    String getPrettyPrinted_value = getPrettyPrinted_compute();
    getPrettyPrinted_visited = -1;
    return getPrettyPrinted_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrinted_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append(getPrettyPrintedLessComments());
        for(beaver.Symbol comment : getComments()) {
            buf.append(comment.value);
            buf.append("\n");
        }
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:43
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        for(Function func : getFunctions()) {
            buf.append(func.getStructureString());
            buf.append('\n');
        }
        return buf.toString();
    }
  /**
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:50
   * @apilevel internal
   */
  public Map<String, Function> Define_Map_String__Function__getSiblings(ASTNode caller, ASTNode child) {
    if(caller == getFunctionListNoTransform()) {
      int i = caller.getIndexOfChild(child);
      return getNested();
    }
    else {      return getParent().Define_Map_String__Function__getSiblings(this, caller);
    }
  }
  /**
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/NameResolution.jrag:55
   * @apilevel internal
   */
  public LocalFunctionLookupInterface Define_LocalFunctionLookupInterface_getParentFunction(ASTNode caller, ASTNode child) {
    if(caller == getFunctionListNoTransform()) {
      int i = caller.getIndexOfChild(child);
      return this;
    }
    else {      return getParent().Define_LocalFunctionLookupInterface_getParentFunction(this, caller);
    }
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
