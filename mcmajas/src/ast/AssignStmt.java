/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production AssignStmt : {@link Stmt} ::= <span class="component">LHS:{@link Expr}</span> <span class="component">RHS:{@link Expr}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:54
 */
public class AssignStmt extends Stmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getLValues_visited = -1;
    getLValues_computed = false;
    getLValues_value = null;
    getPrettyPrintedLessComments_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public AssignStmt clone() throws CloneNotSupportedException {
    AssignStmt node = (AssignStmt)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getLValues_visited = -1;
    node.getLValues_computed = false;
    node.getLValues_value = null;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public AssignStmt copy() {
      try {
        AssignStmt node = (AssignStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public AssignStmt fullCopy() {
    try {
      AssignStmt tree = (AssignStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:93
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseAssignStmt(this);
    }
  /**
   * @ast method 
   * @aspect VarName
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/VarName.jadd:6
   */
  public String getVarName() {
    return getLHS().getVarName();
  }
  /**
   * @ast method 
   * 
   */
  public AssignStmt() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public AssignStmt(Expr p0, Expr p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the LHS child.
   * @param node The new node to replace the LHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setLHS(Expr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the LHS child.
   * @return The current node used as the LHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Expr getLHS() {
    return (Expr)getChild(0);
  }
  /**
   * Retrieves the LHS child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the LHS child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Expr getLHSNoTransform() {
    return (Expr)getChildNoTransform(0);
  }
  /**
   * Replaces the RHS child.
   * @param node The new node to replace the RHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setRHS(Expr node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the RHS child.
   * @return The current node used as the RHS child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public Expr getRHS() {
    return (Expr)getChild(1);
  }
  /**
   * Retrieves the RHS child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the RHS child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public Expr getRHSNoTransform() {
    return (Expr)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:503
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
    	long id = nodeCounter();
        setuID(id);

        	Element e = doc.createElement("AssignStmt");
		e.setAttribute("id", Long.toString(id));
        e.setAttribute("outputSuppressed", Boolean.toString(isOutputSuppressed()));
        e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
        getLHS().getXML(doc, e);
        getRHS().getXML(doc, e);

        e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
parent.appendChild(e);
        return true;
    }
  /**
   * @apilevel internal
   */
  protected int getLValues_visited = -1;
  /**
   * @apilevel internal
   */
  protected boolean getLValues_computed = false;
  /**
   * @apilevel internal
   */
  protected java.util.Set<String> getLValues_value;
  /**
   * @attribute syn
   * @aspect LValue
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/LValue.jrag:41
   */
  @SuppressWarnings({"unchecked", "cast"})
  public java.util.Set<String> getLValues() {
    if(getLValues_computed) {
      return getLValues_value;
    }
      ASTNode$State state = state();
    if(getLValues_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getLValues in class: ");
    getLValues_visited = state().boundariesCrossed;
  int num = state.boundariesCrossed;
  boolean isFinal = this.is$Final();
    getLValues_value = getLValues_compute();
if(isFinal && num == state().boundariesCrossed) getLValues_computed = true;
    getLValues_visited = -1;
    return getLValues_value;
  }
  /**
   * @apilevel internal
   */
  private java.util.Set<String> getLValues_compute() {  return getLHS().getSymbols();  }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:190
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {  return getIndent() + getLHS().getPrettyPrinted() + " = " + getRHS().getPrettyPrinted() + (isOutputSuppressed() ? ";" : "");  }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:306
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {  return getLHS().getStructureString() + " = " + getRHS().getStructureString() + (isOutputSuppressed() ? ";" : "");  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
