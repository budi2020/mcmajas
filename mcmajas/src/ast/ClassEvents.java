/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production ClassEvents : {@link ClassBody} ::= <span class="component">{@link Attribute}*</span> <span class="component">{@link Event}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:41
 */
public class ClassEvents extends ClassBody implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getPrettyPrintedLessComments_visited = -1;
    getIndent_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ClassEvents clone() throws CloneNotSupportedException {
    ClassEvents node = (ClassEvents)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getIndent_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ClassEvents copy() {
      try {
        ClassEvents node = (ClassEvents)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ClassEvents fullCopy() {
    try {
      ClassEvents tree = (ClassEvents) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:343
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseClassEvents(this);
    }
  /**
   * @ast method 
   * 
   */
  public ClassEvents() {
    super();

    setChild(new List(), 0);
    setChild(new List(), 1);

  }
  /**
   * @ast method 
   * 
   */
  public ClassEvents(List<Attribute> p0, List<Event> p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Attribute list.
   * @param list The new list node to be used as the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setAttributeList(List<Attribute> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Attribute list.
   * @return Number of children in the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumAttribute() {
    return getAttributeList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Attribute list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Attribute list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumAttributeNoTransform() {
    return getAttributeListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Attribute list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Attribute getAttribute(int i) {
    return (Attribute)getAttributeList().getChild(i);
  }
  /**
   * Append an element to the Attribute list.
   * @param node The element to append to the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addAttribute(Attribute node) {
    List<Attribute> list = (parent == null || state == null) ? getAttributeListNoTransform() : getAttributeList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addAttributeNoTransform(Attribute node) {
    List<Attribute> list = getAttributeListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Attribute list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setAttribute(Attribute node, int i) {
    List<Attribute> list = getAttributeList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Attribute list.
   * @return The node representing the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Attribute> getAttributes() {
    return getAttributeList();
  }
  /**
   * Retrieves the Attribute list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Attribute list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Attribute> getAttributesNoTransform() {
    return getAttributeListNoTransform();
  }
  /**
   * Retrieves the Attribute list.
   * @return The node representing the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Attribute> getAttributeList() {
    List<Attribute> list = (List<Attribute>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Attribute list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Attribute list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Attribute> getAttributeListNoTransform() {
    return (List<Attribute>)getChildNoTransform(0);
  }
  /**
   * Replaces the Event list.
   * @param list The new list node to be used as the Event list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setEventList(List<Event> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Event list.
   * @return Number of children in the Event list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumEvent() {
    return getEventList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Event list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Event list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumEventNoTransform() {
    return getEventListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Event list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Event list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Event getEvent(int i) {
    return (Event)getEventList().getChild(i);
  }
  /**
   * Append an element to the Event list.
   * @param node The element to append to the Event list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addEvent(Event node) {
    List<Event> list = (parent == null || state == null) ? getEventListNoTransform() : getEventList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addEventNoTransform(Event node) {
    List<Event> list = getEventListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Event list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setEvent(Event node, int i) {
    List<Event> list = getEventList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Event list.
   * @return The node representing the Event list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Event> getEvents() {
    return getEventList();
  }
  /**
   * Retrieves the Event list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Event list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Event> getEventsNoTransform() {
    return getEventListNoTransform();
  }
  /**
   * Retrieves the Event list.
   * @return The node representing the Event list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Event> getEventList() {
    List<Event> list = (List<Event>)getChild(1);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Event list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Event list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Event> getEventListNoTransform() {
    return (List<Event>)getChildNoTransform(1);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:380
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
        long id = nodeCounter();
        setuID(id);

		Element e = doc.createElement("ClassEvents");
		e.setAttribute("id", Long.toString(id));

        for(Attribute attr : getAttributes()) {
            attr.getXML(doc, e);
        }

        for(Event eve : getEvents()) {
            eve.getXML(doc, e);
        }

        e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
parent.appendChild(e);
        return true;
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:684
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append(getIndent() + "events ");
		boolean first = true;
        for(Attribute attr : getAttributes()) {
            if(first)
            	buf.append("(");            
            else
                buf.append(", ");
            buf.append(attr.getPrettyPrinted());
            first = false;
        }
        if(!first)
        	buf.append(") ");
        buf.append('\n');
        for(Event eve : getEvents()) {
            buf.append(eve.getPrettyPrinted());
            buf.append('\n');
        }
        buf.append(getIndent() + "end");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getIndent_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:768
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getIndent() {
      ASTNode$State state = state();
    if(getIndent_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getIndent in class: ");
    getIndent_visited = state().boundariesCrossed;
    String getIndent_value = getIndent_compute();
    getIndent_visited = -1;
    return getIndent_value;
  }
  /**
   * @apilevel internal
   */
  private String getIndent_compute() {
    	ASTNode parent = getParent();
    	if ( parent != null ){
        	return parent.getIndent() + ASTNode.INDENT_TAB;
        }
        return "" ;
    }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:263
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append("events ");
		boolean first = true;
        for(Attribute attr : getAttributes()) {
            if(first)
            	buf.append("(");            
            else
                buf.append(", ");
            buf.append(attr.getStructureString());
            first = false;
        }
        if(!first)
        	buf.append(") ");
        buf.append('\n');
        for(Event eve : getEvents()) {
            buf.append(eve.getStructureString());
            buf.append('\n');
        }
        buf.append("end");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
