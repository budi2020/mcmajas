/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production Methods : {@link ClassBody} ::= <span class="component">{@link Attribute}*</span> <span class="component">{@link Signature}*</span> <span class="component">PropAcc:{@link PropertyAccess}*</span> <span class="component">{@link Function}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:38
 */
public class Methods extends ClassBody implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getPrettyPrintedLessComments_visited = -1;
    getIndent_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Methods clone() throws CloneNotSupportedException {
    Methods node = (Methods)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getIndent_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Methods copy() {
      try {
        Methods node = (Methods)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Methods fullCopy() {
    try {
      Methods tree = (Methods) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:331
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseMethods(this);
    }
  /**
   * @ast method 
   * 
   */
  public Methods() {
    super();

    setChild(new List(), 0);
    setChild(new List(), 1);
    setChild(new List(), 2);
    setChild(new List(), 3);

  }
  /**
   * @ast method 
   * 
   */
  public Methods(List<Attribute> p0, List<Signature> p1, List<PropertyAccess> p2, List<Function> p3) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
    setChild(p3, 3);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 4;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Attribute list.
   * @param list The new list node to be used as the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setAttributeList(List<Attribute> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Attribute list.
   * @return Number of children in the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumAttribute() {
    return getAttributeList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Attribute list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Attribute list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumAttributeNoTransform() {
    return getAttributeListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Attribute list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Attribute getAttribute(int i) {
    return (Attribute)getAttributeList().getChild(i);
  }
  /**
   * Append an element to the Attribute list.
   * @param node The element to append to the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addAttribute(Attribute node) {
    List<Attribute> list = (parent == null || state == null) ? getAttributeListNoTransform() : getAttributeList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addAttributeNoTransform(Attribute node) {
    List<Attribute> list = getAttributeListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Attribute list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setAttribute(Attribute node, int i) {
    List<Attribute> list = getAttributeList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Attribute list.
   * @return The node representing the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Attribute> getAttributes() {
    return getAttributeList();
  }
  /**
   * Retrieves the Attribute list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Attribute list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Attribute> getAttributesNoTransform() {
    return getAttributeListNoTransform();
  }
  /**
   * Retrieves the Attribute list.
   * @return The node representing the Attribute list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Attribute> getAttributeList() {
    List<Attribute> list = (List<Attribute>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Attribute list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Attribute list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Attribute> getAttributeListNoTransform() {
    return (List<Attribute>)getChildNoTransform(0);
  }
  /**
   * Replaces the Signature list.
   * @param list The new list node to be used as the Signature list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setSignatureList(List<Signature> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Signature list.
   * @return Number of children in the Signature list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumSignature() {
    return getSignatureList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Signature list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Signature list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumSignatureNoTransform() {
    return getSignatureListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Signature list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Signature list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Signature getSignature(int i) {
    return (Signature)getSignatureList().getChild(i);
  }
  /**
   * Append an element to the Signature list.
   * @param node The element to append to the Signature list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addSignature(Signature node) {
    List<Signature> list = (parent == null || state == null) ? getSignatureListNoTransform() : getSignatureList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addSignatureNoTransform(Signature node) {
    List<Signature> list = getSignatureListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Signature list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setSignature(Signature node, int i) {
    List<Signature> list = getSignatureList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Signature list.
   * @return The node representing the Signature list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Signature> getSignatures() {
    return getSignatureList();
  }
  /**
   * Retrieves the Signature list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Signature list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Signature> getSignaturesNoTransform() {
    return getSignatureListNoTransform();
  }
  /**
   * Retrieves the Signature list.
   * @return The node representing the Signature list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Signature> getSignatureList() {
    List<Signature> list = (List<Signature>)getChild(1);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Signature list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Signature list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Signature> getSignatureListNoTransform() {
    return (List<Signature>)getChildNoTransform(1);
  }
  /**
   * Replaces the PropAcc list.
   * @param list The new list node to be used as the PropAcc list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setPropAccList(List<PropertyAccess> list) {
    setChild(list, 2);
  }
  /**
   * Retrieves the number of children in the PropAcc list.
   * @return Number of children in the PropAcc list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumPropAcc() {
    return getPropAccList().getNumChild();
  }
  /**
   * Retrieves the number of children in the PropAcc list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the PropAcc list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumPropAccNoTransform() {
    return getPropAccListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the PropAcc list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the PropAcc list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public PropertyAccess getPropAcc(int i) {
    return (PropertyAccess)getPropAccList().getChild(i);
  }
  /**
   * Append an element to the PropAcc list.
   * @param node The element to append to the PropAcc list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addPropAcc(PropertyAccess node) {
    List<PropertyAccess> list = (parent == null || state == null) ? getPropAccListNoTransform() : getPropAccList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addPropAccNoTransform(PropertyAccess node) {
    List<PropertyAccess> list = getPropAccListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the PropAcc list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setPropAcc(PropertyAccess node, int i) {
    List<PropertyAccess> list = getPropAccList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the PropAcc list.
   * @return The node representing the PropAcc list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<PropertyAccess> getPropAccs() {
    return getPropAccList();
  }
  /**
   * Retrieves the PropAcc list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the PropAcc list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<PropertyAccess> getPropAccsNoTransform() {
    return getPropAccListNoTransform();
  }
  /**
   * Retrieves the PropAcc list.
   * @return The node representing the PropAcc list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<PropertyAccess> getPropAccList() {
    List<PropertyAccess> list = (List<PropertyAccess>)getChild(2);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the PropAcc list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the PropAcc list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<PropertyAccess> getPropAccListNoTransform() {
    return (List<PropertyAccess>)getChildNoTransform(2);
  }
  /**
   * Replaces the Function list.
   * @param list The new list node to be used as the Function list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setFunctionList(List<Function> list) {
    setChild(list, 3);
  }
  /**
   * Retrieves the number of children in the Function list.
   * @return Number of children in the Function list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumFunction() {
    return getFunctionList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Function list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Function list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumFunctionNoTransform() {
    return getFunctionListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Function list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Function list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Function getFunction(int i) {
    return (Function)getFunctionList().getChild(i);
  }
  /**
   * Append an element to the Function list.
   * @param node The element to append to the Function list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addFunction(Function node) {
    List<Function> list = (parent == null || state == null) ? getFunctionListNoTransform() : getFunctionList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addFunctionNoTransform(Function node) {
    List<Function> list = getFunctionListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Function list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setFunction(Function node, int i) {
    List<Function> list = getFunctionList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Function list.
   * @return The node representing the Function list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Function> getFunctions() {
    return getFunctionList();
  }
  /**
   * Retrieves the Function list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Function list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Function> getFunctionsNoTransform() {
    return getFunctionListNoTransform();
  }
  /**
   * Retrieves the Function list.
   * @return The node representing the Function list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Function> getFunctionList() {
    List<Function> list = (List<Function>)getChild(3);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Function list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Function list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Function> getFunctionListNoTransform() {
    return (List<Function>)getChildNoTransform(3);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:291
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
        long id = nodeCounter();
        setuID(id);

		Element e = doc.createElement("Methods");
		e.setAttribute("id", Long.toString(id));

        for(Attribute attr : getAttributes()) {
            attr.getXML(doc, e);
        }
        for(Signature sign : getSignatures()) {
            sign.getXML(doc, e);
        }
        for(PropertyAccess prop : getPropAccs()) {
            prop.getXML(doc, e);
        }
        for(Function func : getFunctions()) {
            func.getXML(doc, e);
        }
       e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
parent.appendChild(e);
        return true;
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:584
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append(getIndent() + "methods ");
		boolean first = true;
        for(Attribute attr : getAttributes()) {
            if(first)
            	buf.append("(");            
            else
                buf.append(", ");
            buf.append(attr.getPrettyPrinted());
            first = false;
        }
        if(!first)
        	buf.append(") ");
        buf.append('\n');
        for(Signature sign : getSignatures()) {
            buf.append(sign.getPrettyPrinted());
            buf.append('\n');
        }
        for(PropertyAccess prop : getPropAccs()) {
            buf.append(prop.getPrettyPrinted());
            buf.append('\n');
        }
        for(Function func : getFunctions()) {
            buf.append(func.getPrettyPrinted());
            buf.append('\n');
        }
        buf.append(getIndent() + "end");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getIndent_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:760
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getIndent() {
      ASTNode$State state = state();
    if(getIndent_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getIndent in class: ");
    getIndent_visited = state().boundariesCrossed;
    String getIndent_value = getIndent_compute();
    getIndent_visited = -1;
    return getIndent_value;
  }
  /**
   * @apilevel internal
   */
  private String getIndent_compute() {
    	ASTNode parent = getParent();
    	if ( parent != null ){
        	return parent.getIndent() + ASTNode.INDENT_TAB;
        }
        return "" ;
    }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:163
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append("methods ");
		boolean first = true;
        for(Attribute attr : getAttributes()) {
            if(first)
            	buf.append("(");            
            else
                buf.append(", ");
            buf.append(attr.getStructureString());
            first = false;
        }
        if(!first)
        	buf.append(") ");
        buf.append('\n');
        for(Signature sign : getSignatures()) {
            buf.append(sign.getStructureString());
            buf.append('\n');
        }
        for(PropertyAccess prop : getPropAccs()) {
            buf.append(prop.getStructureString());
            buf.append('\n');
        }
        for(Function func : getFunctions()) {
            buf.append(func.getStructureString());
            buf.append('\n');
        }
        buf.append("end");
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
