/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production CheckScalarStmt : {@link Stmt} ::= <span class="component">{@link NameExpr}</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/IR.ast:25
 */
public class CheckScalarStmt extends Stmt implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getPrettyPrintedLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public CheckScalarStmt clone() throws CloneNotSupportedException {
    CheckScalarStmt node = (CheckScalarStmt)super.clone();
    node.getPrettyPrintedLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public CheckScalarStmt copy() {
      try {
        CheckScalarStmt node = (CheckScalarStmt)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public CheckScalarStmt fullCopy() {
    try {
      CheckScalarStmt tree = (CheckScalarStmt) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:293
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseCheckScalarStmt(this);
    }
  /**
   * @ast method 
   * 
   */
  public CheckScalarStmt() {
    super();


  }
  /**
   * @ast method 
   * 
   */
  public CheckScalarStmt(NameExpr p0) {
    setChild(p0, 0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the NameExpr child.
   * @param node The new node to replace the NameExpr child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setNameExpr(NameExpr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the NameExpr child.
   * @return The current node used as the NameExpr child.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public NameExpr getNameExpr() {
    return (NameExpr)getChild(0);
  }
  /**
   * Retrieves the NameExpr child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the NameExpr child.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public NameExpr getNameExprNoTransform() {
    return (NameExpr)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:84
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {  return getIndent() + "check_scalar("+getNameExpr().getPrettyPrinted() + ")"+ (isOutputSuppressed() ? ";" : "");  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
