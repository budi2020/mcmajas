/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production ElseBlock : {@link ASTNode} ::= <span class="component">{@link Stmt}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/natlab.ast:76
 */
public class ElseBlock extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
    getXML_Document_Element_visited = null;
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    getPrettyPrintedLessComments_visited = -1;
    getStructureStringLessComments_visited = -1;
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ElseBlock clone() throws CloneNotSupportedException {
    ElseBlock node = (ElseBlock)super.clone();
    node.getXML_Document_Element_visited = null;
    node.getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = null;
    node.getPrettyPrintedLessComments_visited = -1;
    node.getStructureStringLessComments_visited = -1;
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ElseBlock copy() {
      try {
        ElseBlock node = (ElseBlock)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public ElseBlock fullCopy() {
    try {
      ElseBlock tree = (ElseBlock) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:375
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseElseBlock(this);
    }
  /**
   * @ast method 
   * 
   */
  public ElseBlock() {
    super();

    setChild(new List(), 0);

  }
  /**
   * @ast method 
   * 
   */
  public ElseBlock(List<Stmt> p0) {
    setChild(p0, 0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Stmt list.
   * @param list The new list node to be used as the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setStmtList(List<Stmt> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Stmt list.
   * @return Number of children in the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumStmt() {
    return getStmtList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Stmt list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Stmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumStmtNoTransform() {
    return getStmtListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Stmt list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Stmt getStmt(int i) {
    return (Stmt)getStmtList().getChild(i);
  }
  /**
   * Append an element to the Stmt list.
   * @param node The element to append to the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addStmt(Stmt node) {
    List<Stmt> list = (parent == null || state == null) ? getStmtListNoTransform() : getStmtList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addStmtNoTransform(Stmt node) {
    List<Stmt> list = getStmtListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Stmt list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setStmt(Stmt node, int i) {
    List<Stmt> list = getStmtList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Stmt list.
   * @return The node representing the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Stmt> getStmts() {
    return getStmtList();
  }
  /**
   * Retrieves the Stmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Stmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Stmt> getStmtsNoTransform() {
    return getStmtListNoTransform();
  }
  /**
   * Retrieves the Stmt list.
   * @return The node representing the Stmt list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Stmt> getStmtList() {
    List<Stmt> list = (List<Stmt>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Stmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Stmt list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Stmt> getStmtListNoTransform() {
    return (List<Stmt>)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  protected java.util.Map getXML_Document_Element_visited;
  /**
   * @attribute syn
   * @aspect ASTtoXML
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AST2XML.jrag:677
   */
  @SuppressWarnings({"unchecked", "cast"})
  public boolean getXML(Document doc, Element parent) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(doc);
    _parameters.add(parent);
    if(getXML_Document_Element_visited == null) getXML_Document_Element_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getXML_Document_Element_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getXML in class: ");
    getXML_Document_Element_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    boolean getXML_Document_Element_value = getXML_compute(doc, parent);
    getXML_Document_Element_visited.remove(_parameters);
    return getXML_Document_Element_value;
  }
  /**
   * @apilevel internal
   */
  private boolean getXML_compute(Document doc, Element parent) {
        long id = nodeCounter();
        setuID(id);
		
		Element e = doc.createElement("ElseBlock");
		e.setAttribute("id", Long.toString(id));

        Element e1 = doc.createElement("StmtList");
        for(Stmt stmt : getStmts()) {
            stmt.getXML(doc, e1);
        }
        e.appendChild(e1);
       	
        e.setAttribute("line", ""+beaver.Symbol.getLine(getStart()));
parent.appendChild(e); 
        return true;
    }
  /**
   * @apilevel internal
   */
  protected java.util.Map getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited;
  /**
   * @attribute syn
   * @aspect AnalysisPrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/AnalysisPrettyPrint.jrag:247
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getAnalysisPrettyPrintedLessComments(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
    java.util.List _parameters = new java.util.ArrayList(3);
    _parameters.add(analysis);
    _parameters.add(Boolean.valueOf(compact));
    _parameters.add(Boolean.valueOf(hideComments));
    if(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited == null) getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited = new java.util.HashMap(4);
      ASTNode$State state = state();
    if(Integer.valueOf(state().boundariesCrossed).equals(getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.get(_parameters)))
      throw new RuntimeException("Circular definition of attr: getAnalysisPrettyPrintedLessComments in class: ");
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.put(_parameters, Integer.valueOf(state().boundariesCrossed));
    String getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value = getAnalysisPrettyPrintedLessComments_compute(analysis, compact, hideComments);
    getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_visited.remove(_parameters);
    return getAnalysisPrettyPrintedLessComments_analysis_StructuralAnalysis_boolean_boolean_value;
  }
  /**
   * @apilevel internal
   */
  private String getAnalysisPrettyPrintedLessComments_compute(analysis.StructuralAnalysis analysis, boolean compact, boolean hideComments) {
        StringBuffer buf = new StringBuffer();
        String indent = getIndent();
        buf.append(indent + "else ");
        buf.append('\n');
        for(Stmt stmt : getStmts()) {
            String stmtStr = stmt.getAnalysisPrettyPrinted(analysis, compact, hideComments);         
            buf.append(stmtStr.equals("") ? "" : stmtStr + "\n" );
        }
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getPrettyPrintedLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/PrettyPrint.jrag:286
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getPrettyPrintedLessComments() {
      ASTNode$State state = state();
    if(getPrettyPrintedLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getPrettyPrintedLessComments in class: ");
    getPrettyPrintedLessComments_visited = state().boundariesCrossed;
    String getPrettyPrintedLessComments_value = getPrettyPrintedLessComments_compute();
    getPrettyPrintedLessComments_visited = -1;
    return getPrettyPrintedLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getPrettyPrintedLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        String indent = getIndent();
        buf.append(indent + "else ");
        buf.append('\n');
        for(Stmt stmt : getStmts()) {
            String stmtStr = stmt.getPrettyPrinted();         
            buf.append(stmtStr.equals("") ? "" : stmtStr + "\n" );
        }
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  protected int getStructureStringLessComments_visited = -1;
  /**
   * @attribute syn
   * @aspect StructureString
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/test/natlab/StructureString.jrag:396
   */
  @SuppressWarnings({"unchecked", "cast"})
  public String getStructureStringLessComments() {
      ASTNode$State state = state();
    if(getStructureStringLessComments_visited == state().boundariesCrossed)
      throw new RuntimeException("Circular definition of attr: getStructureStringLessComments in class: ");
    getStructureStringLessComments_visited = state().boundariesCrossed;
    String getStructureStringLessComments_value = getStructureStringLessComments_compute();
    getStructureStringLessComments_visited = -1;
    return getStructureStringLessComments_value;
  }
  /**
   * @apilevel internal
   */
  private String getStructureStringLessComments_compute() {
        StringBuffer buf = new StringBuffer();
        buf.append("else ");
        buf.append('\n');
        for(Stmt stmt : getStmts()) {
            buf.append('\t');
            buf.append(stmt.getStructureString());
            buf.append('\n');
        }
        return buf.toString();
    }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
