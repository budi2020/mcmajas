/* This file was generated with JastAdd2 (http://jastadd.org) version R20121112 (r872) */
package ast;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import beaver.Symbol;
import natlab.toolkits.analysis.varorfun.*;
import natlab.LocalFunctionLookupInterface;
import natlab.toolkits.filehandling.genericFile.*;
import java.util.*;
/**
 * @production IntersectionType : {@link Type} ::= <span class="component">{@link Type}*</span>;
 * @ast node
 * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/types.ast:24
 */
public class IntersectionType extends Type implements Cloneable {
  /**
   * @apilevel low-level
   */
  public void flushCache() {
    super.flushCache();
  }
  /**
   * @apilevel internal
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IntersectionType clone() throws CloneNotSupportedException {
    IntersectionType node = (IntersectionType)super.clone();
    node.in$Circle(false);
    node.is$Final(false);
    return node;
  }
  /**
   * @apilevel internal
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IntersectionType copy() {
      try {
        IntersectionType node = (IntersectionType)clone();
        if(children != null) node.children = (ASTNode[])children.clone();
        return node;
      } catch (CloneNotSupportedException e) {
      }
      System.err.println("Error: Could not clone node of type " + getClass().getName() + "!");
      return null;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   */
  @SuppressWarnings({"unchecked", "cast"})
  public IntersectionType fullCopy() {
    try {
      IntersectionType tree = (IntersectionType) clone();
      tree.setParent(null);// make dangling
      if (children != null) {
        tree.children = new ASTNode[children.length];
        for (int i = 0; i < children.length; ++i) {
          if (children[i] == null) {
            tree.children[i] = null;
          } else {
            tree.children[i] = ((ASTNode) children[i]).fullCopy();
            ((ASTNode) tree.children[i]).setParent(tree);
          }
        }
      }
      return tree;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " +
        getClass().getName());
    }
  }
  /**
   * @ast method 
   * @aspect ASTAnalyze
   * @declaredat /home/isbadawi/workspace/mclab/languages/Natlab/src/ASTAnalyze.jadd:450
   */
  public void analyze(nodecases.NodeCaseHandler visitor)
    {
        visitor.caseIntersectionType(this);
    }
  /**
   * @ast method 
   * 
   */
  public IntersectionType() {
    super();

    setChild(new List(), 0);

  }
  /**
   * @ast method 
   * 
   */
  public IntersectionType(List<Type> p0) {
    setChild(p0, 0);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @ast method 
   * 
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /**
   * Replaces the Type list.
   * @param list The new list node to be used as the Type list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setTypeList(List<Type> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Type list.
   * @return Number of children in the Type list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public int getNumType() {
    return getTypeList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Type list.
   * Calling this method will not trigger rewrites..
   * @return Number of children in the Type list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public int getNumTypeNoTransform() {
    return getTypeListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Type list..
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Type list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public Type getType(int i) {
    return (Type)getTypeList().getChild(i);
  }
  /**
   * Append an element to the Type list.
   * @param node The element to append to the Type list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void addType(Type node) {
    List<Type> list = (parent == null || state == null) ? getTypeListNoTransform() : getTypeList();
    list.addChild(node);
  }
  /**
   * @apilevel low-level
   * @ast method 
   * 
   */
  public void addTypeNoTransform(Type node) {
    List<Type> list = getTypeListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Type list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public void setType(Type node, int i) {
    List<Type> list = getTypeList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Type list.
   * @return The node representing the Type list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  public List<Type> getTypes() {
    return getTypeList();
  }
  /**
   * Retrieves the Type list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Type list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  public List<Type> getTypesNoTransform() {
    return getTypeListNoTransform();
  }
  /**
   * Retrieves the Type list.
   * @return The node representing the Type list.
   * @apilevel high-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Type> getTypeList() {
    List<Type> list = (List<Type>)getChild(0);
    list.getNumChild();
    return list;
  }
  /**
   * Retrieves the Type list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Type list.
   * @apilevel low-level
   * @ast method 
   * 
   */
  @SuppressWarnings({"unchecked", "cast"})
  public List<Type> getTypeListNoTransform() {
    return (List<Type>)getChildNoTransform(0);
  }
  /**
   * @apilevel internal
   */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}
