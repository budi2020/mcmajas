package javascriptcodegen;

import java.util.HashMap;
import java.util.Map;

public class Util {
	static Map<String, String> binaryOperatorMap = new HashMap<String, String>(); 
	static Map<String, String> directBuiltinMap = new HashMap<String, String>();
	
	static {
		binaryOperatorMap.put("plus", "+");
		binaryOperatorMap.put("minus", "-");
		binaryOperatorMap.put("mtimes", "*");
		binaryOperatorMap.put("mrdivide", "/");
		binaryOperatorMap.put("mldivide", "\\");
		binaryOperatorMap.put("mpower", "**");
		binaryOperatorMap.put("times", "*");
		binaryOperatorMap.put("rdivide", "/");
		binaryOperatorMap.put("ldivide", "\\");
		binaryOperatorMap.put("power", "**");
		binaryOperatorMap.put("and", "&&");
		binaryOperatorMap.put("or", "||");
		binaryOperatorMap.put("lt", "<");
		binaryOperatorMap.put("gt", ">");
		binaryOperatorMap.put("le", "<=");
		binaryOperatorMap.put("ge", ">=");
		binaryOperatorMap.put("eq", "==");
		binaryOperatorMap.put("ne", "!=");
		binaryOperatorMap.put("not", "!");
		
		directBuiltinMap.put("sqrt", "sqrt");	
		directBuiltinMap.put("sin", "sin");	
		directBuiltinMap.put("cos", "cos");
		directBuiltinMap.put("sum", "sum");
		directBuiltinMap.put("size", "size");
		directBuiltinMap.put("exp", "exp");
		directBuiltinMap.put("transpose", "transpose");
		directBuiltinMap.put("ceil", "ceiling");
		
		
	}
	
	public static String getBinaryOperator(String symbol) {
		return binaryOperatorMap.get(symbol);
	}
	
	private void makeFortranUnOperatorMap(){
//		FortranUnOperatorMap.put("uminus", "-");
//		FortranUnOperatorMap.put("uplus", "+");
	}
	
	private void makeFortranDirectBuiltinMap(){
		//TODO create a categorical map here 
	}
	
	private void makeFortranNoDirectBuiltinSet(){
//		FortranNoDirectBuiltinSet.add("horzcat");
//		FortranNoDirectBuiltinSet.add("vertcat");
//		FortranNoDirectBuiltinSet.add("ones");
//		FortranNoDirectBuiltinSet.add("zeros");
//		FortranNoDirectBuiltinSet.add("colon");
//		FortranNoDirectBuiltinSet.add("randperm");
	}
	
	private void makeFortranBuiltinConstMap(){
		//TODO create a categorical map here 
//		FortranBuiltinConstMap.put("pi", "Math.PI");
//		FortranBuiltinConstMap.put("true", ".true.");
//		FortranBuiltinConstMap.put("false", ".false.");
	}
	
	private void makeFortranIOOperationMap(){
//		FortranIOOperationMap.put("disp", "print *, ");	
	}
	
	
}
