package javascriptcodegen;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import natlab.tame.BasicTamerTool;
import natlab.tame.interproceduralAnalysis.InterproceduralAnalysisNode;
import natlab.tame.valueanalysis.ValueAnalysis;
import natlab.tame.valueanalysis.ValueAnalysisPrinter;
import natlab.tame.valueanalysis.aggrvalue.AggrValue;
import natlab.tame.valueanalysis.basicmatrix.BasicMatrixValue;
import natlab.toolkits.filehandling.genericFile.GenericFile;
import natlab.toolkits.path.FileEnvironment;

public class McMajasMain {
	
	private static String fileToString(String path) {
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(new File(path));
		} catch (FileNotFoundException e) {
			System.out.println(path + " not found");
			return null;
		}
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		StringBuilder sb = new StringBuilder();
		try {
			String line = bufferedReader.readLine();
			while (line != null) {
				sb.append(line + "\n");
				line = bufferedReader.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e2) {
					
				}
			}
		}
		return sb.toString();
	}
	
	private static void generateJavaScript(String funcName, String fileIn) {
	    GenericFile gFile = GenericFile.create(fileIn);
		FileEnvironment env = new FileEnvironment(gFile); //get path environment obj
		BasicTamerTool tool = new BasicTamerTool();
		ValueAnalysis<AggrValue<BasicMatrixValue>>  analysis = tool.analyze(new String[] {}, env);
		/**
		 * generate JavaScript and then let the AST toString itself.
		 */
		StringBuilder ir = new StringBuilder();
		
		int size = analysis.getNodeList().size();
		for (int i = 0; i < size; i++) {
			ir.append(ValueAnalysisPrinter.prettyPrint(analysis
					.getNodeList().get(i).getAnalysis()));
		}
		
		StringBuilder js = new StringBuilder(4000);
		JavascriptCodeGenerator.analyze(analysis, js);
		js.append("\n");
		js.append("\n" + funcName + "();");
		js.append("\n");
		
		System.out.println(js.toString());
		
		String matlab = fileToString(fileIn);
		String html = fileToString("./template.txt");
		html = html.replace("<%=matlab%>", matlab);
		html = html.replace("<%=ir%>", ir.toString());
		html = html.replace("<%=js%>", js);
		String jsAsText = js.toString();//.replace("\n", "<br/>\n");
		html = html.replace("<%=prog%>", jsAsText);
		
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new BufferedWriter
					(new FileWriter("./output/" + funcName + ".html")));
			pw.print(html);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				pw.close();
			} catch (Exception e) {
				
			}
		}
	}
	
	public static void main(String[] args) {
		
		String args0 = "input";
		String funcName = "minus1";
		
		String path = args0;
		File inputPath = new File(path);
		if (!inputPath.exists()) {
			System.out.println("input does not exist");
			return;
		}
		if (inputPath.isDirectory()) {
			String[] inputFiles = inputPath.list();
			for (String inputFile : inputFiles) {
				if (inputFile.endsWith(".m")) {
					funcName = inputFile.substring(0, inputFile.length() - 2);
				    generateJavaScript(funcName, new File(inputPath, inputFile).getAbsolutePath());
				}
			}
		} else {
			funcName = path.substring(0, path.length() - 2);
		    String fileIn = funcName + ".m";
		    generateJavaScript(funcName, fileIn);
		}
	}
}
