package javascriptcodegen.handler;

import java.util.HashMap;
import java.util.Map;

public class HandlerFactory {
	private static Map<String, Handler> handlers = new HashMap<String, Handler>();
	private static String[] validNames = { "disp", "eq", "ge", "gt", "horzcat", "int16", 
		"le", "lt",
		"minus", "mtimes", "ne", "plot", "plus", "rdivide", "times", "uminus", "vertcat" };
	
	
	private static String capitalize(String s) {
		if (s != null && s.length() > 1) {
			return s.substring(0, 1).toUpperCase() + s.substring(1, s.length());
		} else {
			return s;
		}
	}
	public static Handler getHandler(String name) {
		Handler handler = handlers.get(name);
		if (handler == null) {
			for (String validName : validNames) {
				if (validName.equals(name)) {
					try {
						String className = "javascriptcodegen.handler.impl." + 
								capitalize(name) + "Handler";
						Class<?> klass = Class.forName(className);
						handler = (Handler) klass.newInstance();
						handlers.put(name, handler);
						break;
					} catch (Exception e) {
						//e.printStackTrace();
					}
				}
			}
		}
		return handler;
	}

}
