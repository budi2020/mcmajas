package javascriptcodegen.handler.impl;

public class MinusHandler extends PlusHandler {

	@Override
	protected String getFunctionName() {
		return "minus";
	}
}
