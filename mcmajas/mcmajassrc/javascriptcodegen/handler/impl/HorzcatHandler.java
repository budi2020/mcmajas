package javascriptcodegen.handler.impl;

import java.util.Set;

import natlab.tame.valueanalysis.ValueAnalysis;
import natlab.tame.valueanalysis.aggrvalue.AggrValue;
import natlab.tame.valueanalysis.basicmatrix.BasicMatrixValue;

import ast.ASTNode;
import ast.NameExpr;
import ast.ParameterizedExpr;
import javascriptcodegen.handler.Handler;

public class HorzcatHandler implements Handler {
	@Override
	public String handle(ValueAnalysis<AggrValue<BasicMatrixValue>> analysis, ParameterizedExpr expr) {
		Set<NameExpr> nameExprs = expr.getAllNameExpressions();
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		
		int size = nameExprs.size();
		int count = 0;
		for (NameExpr nameExpr : nameExprs) {
			if (count > 0) {
				// first expr is "horzcat", so skip this
				sb.append(nameExpr.getVarName());
			}
			if (count > 0 && count < size - 1) {
				sb.append(", ");
			}
			count++;
		}
		sb.append("];");
		return sb.toString();
	}

}
