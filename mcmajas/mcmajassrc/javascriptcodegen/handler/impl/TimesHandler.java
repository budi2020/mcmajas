package javascriptcodegen.handler.impl;

public class TimesHandler extends PlusHandler {
	@Override
	protected String getFunctionName() {
		return "times";
	}
}
