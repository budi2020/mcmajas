package javascriptcodegen.handler.impl;

import java.util.Set;

import javascriptcodegen.handler.Handler;
import natlab.tame.valueanalysis.ValueAnalysis;
import natlab.tame.valueanalysis.aggrvalue.AggrValue;
import natlab.tame.valueanalysis.basicmatrix.BasicMatrixValue;
import ast.NameExpr;
import ast.ParameterizedExpr;

public class LeHandler implements Handler {
	@Override
	public String handle(ValueAnalysis<AggrValue<BasicMatrixValue>> analysis, ParameterizedExpr expr) {
		
		
		Set<NameExpr> nameExprs = expr.getAllNameExpressions();
		String operator = null;
		String var1 = null;
		String var2 = null;
		
		for (NameExpr nameExpr : nameExprs) {
			if (operator == null) {
				operator = nameExpr.getVarName();
			} else if (var1 == null) {
				var1 = nameExpr.getVarName();
			} else if (var2 == null) {
				var2 = nameExpr.getVarName();
			}
		}

		return "(" + var1 + " <= " + var2 + ");";
	}

}
