package javascriptcodegen.handler.impl;

import java.util.List;
import java.util.Set;

import natlab.tame.valueanalysis.IntraproceduralValueAnalysis;
import natlab.tame.valueanalysis.ValueAnalysis;
import natlab.tame.valueanalysis.ValueSet;
import natlab.tame.valueanalysis.aggrvalue.AggrValue;
import natlab.tame.valueanalysis.basicmatrix.BasicMatrixValue;

import ast.NameExpr;
import ast.ParameterizedExpr;
import javascriptcodegen.handler.Handler;

public class PlusHandler implements Handler {
	
	protected String getFunctionName() {
		return "plus";
	}
	
	private boolean isInt16(ValueAnalysis<AggrValue<BasicMatrixValue>> analysis, String var) {
		IntraproceduralValueAnalysis<AggrValue<BasicMatrixValue>> intraAnalysis = 
				analysis.getNodeList().get(0).getAnalysis();
		ValueSet<AggrValue<BasicMatrixValue>> valueSet = intraAnalysis.getCurrentOutSet().get(var);
		AggrValue<BasicMatrixValue> singleton = valueSet.getSingleton();
		return singleton.toString().indexOf("int16") != -1;
	}
	
	@Override
	public String handle(ValueAnalysis<AggrValue<BasicMatrixValue>> analysis, ParameterizedExpr expr) {
		Set<NameExpr> nameExprs = expr.getAllNameExpressions();
		String operator = null;
		String var1 = null;
		String var2 = null;
		
		for (NameExpr nameExpr : nameExprs) {
			if (operator == null) {
				operator = nameExpr.getVarName();
			} else if (var1 == null) {
				var1 = nameExpr.getVarName();
			} else {
				var2 = nameExpr.getVarName();
			}
		}

		boolean involvingInt = isInt16(analysis, var1) || isInt16(analysis, var2);
		if (involvingInt) {
			return getFunctionName() + "(int16(" + var1 + "), int16(" + var2 + "));";
		} else {
			return getFunctionName() + "(" + var1 + ", " + var2 + ");";
		}
	}

}
