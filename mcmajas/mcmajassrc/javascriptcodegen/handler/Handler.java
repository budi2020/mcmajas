package javascriptcodegen.handler;

import natlab.tame.valueanalysis.ValueAnalysis;
import natlab.tame.valueanalysis.aggrvalue.AggrValue;
import natlab.tame.valueanalysis.basicmatrix.BasicMatrixValue;
import ast.ParameterizedExpr;

public interface Handler {
	String handle(ValueAnalysis<AggrValue<BasicMatrixValue>> analysis, ParameterizedExpr expr);
}
