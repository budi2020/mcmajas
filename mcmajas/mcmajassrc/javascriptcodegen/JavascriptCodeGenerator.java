package javascriptcodegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javascriptcodegen.handler.Handler;
import javascriptcodegen.handler.HandlerFactory;

import natlab.backends.Fortran.codegen.FortranMapping;
import natlab.backends.Fortran.codegen.ASTcaseHandler.HandleCaseTIRArrayGetStmt;
import natlab.backends.Fortran.codegen.ASTcaseHandler.HandleCaseTIRArraySetStmt;
import natlab.backends.Fortran.codegen.ASTcaseHandler.HandleCaseTIRCommentStmt;
import natlab.backends.Fortran.codegen.ASTcaseHandler.HandleCaseTIRFunction;
import natlab.backends.Fortran.codegen.ASTcaseHandler.HandleCaseTIRIfStmt;
import natlab.backends.Fortran.codegen.FortranAST.StatementSection;
import natlab.backends.Fortran.codegen.FortranAST.SubProgram;
import natlab.tame.tir.TIRAbstractAssignToListStmt;
import natlab.tame.tir.TIRAbstractAssignToVarStmt;
import natlab.tame.tir.TIRArrayGetStmt;
import natlab.tame.tir.TIRArraySetStmt;
import natlab.tame.tir.TIRAssignLiteralStmt;
import natlab.tame.tir.TIRBreakStmt;
import natlab.tame.tir.TIRCommaSeparatedList;
import natlab.tame.tir.TIRCommentStmt;
import natlab.tame.tir.TIRContinueStmt;
import natlab.tame.tir.TIRForStmt;
import natlab.tame.tir.TIRFunction;
import natlab.tame.tir.TIRIfStmt;
import natlab.tame.tir.TIRNode;
import natlab.tame.tir.TIRStatementList;
import natlab.tame.tir.TIRStmt;
import natlab.tame.tir.TIRWhileStmt;
import natlab.tame.tir.analysis.TIRAbstractNodeCaseHandler;
import natlab.tame.valueanalysis.IntraproceduralValueAnalysis;
import natlab.tame.valueanalysis.ValueAnalysis;
import natlab.tame.valueanalysis.ValueSet;
import natlab.tame.valueanalysis.aggrvalue.AggrValue;
import natlab.tame.valueanalysis.basicmatrix.BasicMatrixValue;
import ast.ASTNode;
import ast.Expr;
import ast.MatrixExpr;
import ast.Name;
import ast.NameExpr;
import ast.ParameterizedExpr;
import ast.Row;
import ast.Stmt;

public class JavascriptCodeGenerator extends TIRAbstractNodeCaseHandler {
	ValueAnalysis<AggrValue<BasicMatrixValue>> analysis;
	public StringBuilder sb;
	public int indent;
	
	private JavascriptCodeGenerator(ValueAnalysis<AggrValue<BasicMatrixValue>> analysis,
			StringBuilder sb) {
		this.sb = sb;
		this.analysis = analysis;
		((TIRNode) analysis.getNodeList().get(0).getAnalysis().getTree()).tirAnalyze(this);
	}

	public static void analyze(
			ValueAnalysis<AggrValue<BasicMatrixValue>> analysis, StringBuilder sb){
		new JavascriptCodeGenerator(analysis, sb);
	}
	
	public void iterateStatements(ast.List<ast.Stmt> stmts){
		System.out.println("iterateStatements");
		for(ast.Stmt stmt : stmts){
			((TIRNode)stmt).tirAnalyze(this);
		}
	}
	
	@Override
	public void caseASTNode(ASTNode node){
		System.out.println("caseASTNode:" + node.dumpString());
	}
	
	@Override
	public void caseTIRBreakStmt(TIRBreakStmt node) {
		System.out.println("caseTIRBreakStmt:" + node.dumpString());
		sb.append("\n" + getIndent() + "break;");
	}

	@Override
	public void caseTIRContinueStmt(TIRContinueStmt node) {
		System.out.println("caseTIRContinueStmt:" + node.dumpString());
		sb.append("\n" + getIndent() + "continue;");
	}

	@Override
	public void caseTIRFunction(TIRFunction node){
		sb.append("\nfunction ");
		sb.append(node.getName() + "() {");
		indent++;
		TIRStatementList stmtList = node.getStmtList();
		for (Stmt stmt : stmtList) {
			((TIRNode)stmt).tirAnalyze(this);
		}
		indent--;
		sb.append("\n}");
	}
	
	@Override
	public void caseTIRCommentStmt(TIRCommentStmt node){
		System.out.println("caseTIRCommentStmt: " + node.getNodeString());
		
		if (node.getNodeString().contains("%")){
			sb.append("\n" + getIndent() + "/*" +
					node.getNodeString().replace("%", "") + " */");
		}
	}
	
	@Override
	public void caseTIRAssignLiteralStmt(TIRAssignLiteralStmt node){
  		System.out.println("caseTIRAssignLiteralStmt");
  		sb.append("\n" + getIndent() + node.getNodeString());
	}
	
	@Override
	public void caseTIRAbstractAssignToVarStmt(TIRAbstractAssignToVarStmt node){
		System.out.println("caseTIRAbstractAssignToVarStmt");
	}

	@Override
	public void caseTIRAbstractAssignToListStmt(TIRAbstractAssignToListStmt node){
		System.out.println("caseTIRAbstractAssignToListStmt:" + node.getNodeString());
		Expr lhs = node.getLHS();
		Expr rhs = node.getRHS();
		
		System.out.println("lhs type:" + lhs.getClass());
		boolean matrixEmpty = false;
  		if (lhs instanceof MatrixExpr) {
  			String nodeString = null;
  			MatrixExpr matrixExpr = (MatrixExpr) lhs;
  			if (matrixExpr.getNumChild() == 1 && matrixExpr.getChild(0) instanceof ast.List) {
  				if (matrixExpr.getChild(0).getNumChild() == 1) {
  					ASTNode<?> listChild = matrixExpr.getChild(0).getChild(0);
  					if (listChild instanceof ast.Row) {
  						Row row = (Row) listChild;
  						if (row.getNodeString().isEmpty()) {
  							matrixEmpty = true;
  						} else {
  							nodeString = row.getNodeString();
  						}
  					}
  				}
  			}
  			
  			if (!matrixEmpty) {
  				sb.append("\n" + getIndent() + nodeString + " = ");
  			}
  		}
		System.out.println("rhs type:" + rhs.getClass());
  		if (rhs instanceof ParameterizedExpr) {
  			ParameterizedExpr parExpr = (ParameterizedExpr) rhs;

  			if (matrixEmpty) {
  				sb.append("\n" + getIndent());
  			}
  			String varName = parExpr.getVarName();
  			Handler handler = HandlerFactory.getHandler(varName);
  			if (handler != null) {
  				sb.append(handler.handle(analysis, parExpr));
  			}
  		}
	}
	
	@Override
	public void caseTIRIfStmt(TIRIfStmt node){
		System.out.println("caseTIRIfStmt");
		sb.append("\n" + getIndent() + "if (" + node.getConditionVarName().getID() + "===true) {");
		TIRStatementList stmtList = node.getIfStameents();
		for (Stmt stmt : stmtList) {
			indent++;
			((TIRNode)stmt).tirAnalyze(this);
			indent--;
		}
		
		if (node.getElseStatements().getNumChild() > 0) {
			sb.append("\n" + getIndent() + "} else {");
			stmtList = node.getElseStatements();
			indent++;
			for (Stmt stmt : stmtList) {
				((TIRNode)stmt).tirAnalyze(this);
			}
			indent--;
		}
		sb.append("\n" + getIndent() + "}");
	}
	
	@Override
	public void caseTIRWhileStmt(TIRWhileStmt node){
		System.out.println("caseTIRWhileStmt");
		
		String condition = node.getCondition().getNodeString();
		sb.append("\n" + getIndent() + "while (" + condition + ") {");
		indent++;
		
		TIRStatementList stmtList = node.getStatements();
		for (Stmt stmt : stmtList) {
			((TIRNode)stmt).tirAnalyze(this);
		}
		indent--;
		sb.append("\n" + getIndent() + "}");
	}
	
	@Override
	public void caseTIRStmt(TIRStmt node) {
		sb.append(getIndent() + node);
		node.tirAnalyze(this);
	}
	@Override
	public void caseTIRForStmt(TIRForStmt node){
		System.out.println("caseTIRForStmt");
		String varName = node.getLoopVarName().getVarName();
		sb.append("\n" + getIndent() + "for (var " + varName + "=" + node.getLowerName().getID()
				+"; " + varName + "<=" + node.getUpperName().getID() + "; " + varName + "++) {");

		indent++;
		
		TIRStatementList stmtList = node.getStatements();
		for (Stmt stmt : stmtList) {
			((TIRNode)stmt).tirAnalyze(this);
		}
		indent--;
		sb.append("\n" + getIndent() + "}");
	}
	
	@Override
	public void caseTIRArrayGetStmt(TIRArrayGetStmt node){
		System.out.println("caseTIRArrayGetStmt");
		HandleCaseTIRArrayGetStmt arrGetStmt = new HandleCaseTIRArrayGetStmt();
	}
	
	@Override
	public void caseTIRArraySetStmt(TIRArraySetStmt node){
		System.out.println("caseTIRArraySetStmt:" + node.dumpString());
		String arrayName = node.getArrayName().getNodeString();
		
		String[] indexString = node.getIndizes().toString().replace("[", "").replace("]", "").split(",");
		for(String index : indexString){
			sb.append("\n" + getIndent() + "setArrayElements(" + arrayName + ", " + index + ", " +
					node.getValueName().getNodeString() + ");");
		}
	}
	
	private String getIndent() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < indent; i++) {
			sb.append("\t");
		}
		return sb.toString();
	}
}