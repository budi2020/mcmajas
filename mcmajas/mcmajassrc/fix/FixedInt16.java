package fix;

import natlab.tame.builtin.BuiltinVisitor;
import natlab.tame.builtin.Builtin.AbstractMatrixConstructor;
import natlab.tame.builtin.classprop.ClassPropTool;
import natlab.tame.builtin.classprop.HasClassPropagationInfo;
import natlab.tame.builtin.classprop.ast.CP;
import natlab.tame.builtin.classprop.ast.CPNone;
import natlab.tame.builtin.shapeprop.HasShapePropagationInfo;
import natlab.tame.builtin.shapeprop.ShapePropTool;
import natlab.tame.builtin.shapeprop.ast.SPNode;

public class FixedInt16 extends AbstractMatrixConstructor implements HasClassPropagationInfo, HasShapePropagationInfo {
    //returns the singleton instance of this class
    private static FixedInt16 singleton = null;
    public static FixedInt16 getInstance(){
        if (singleton == null) singleton = new FixedInt16();
        return singleton;
    }
    //visit visitor
    public <Arg,Ret> Ret visit(BuiltinVisitor<Arg,Ret> visitor, Arg arg){
        return visitor.caseInt16(this,arg);
    }
    //return name of builtin
    public String getName(){
        return "int16";
    }
    
    public CP getMatlabClassPropagationInfo(){{
        return getClassPropagationInfo();
    }}

    private CP classPropInfo = null;
    public CP getClassPropagationInfo(){
        //set classPropInfo if not defined
        if (classPropInfo == null){
            classPropInfo = ClassPropTool.parse("any->int16");
            classPropInfo.setVar("parent",new CPNone());
            classPropInfo.setVar("matlab",getMatlabClassPropagationInfo());
        }
        return classPropInfo;
    }
	@Override
    public SPNode getShapePropagationInfo(){
        return ShapePropTool.parse("[m,n],k=minimum(m,n)->[k,1]");
    }
}
